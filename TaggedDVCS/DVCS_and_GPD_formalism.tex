\setlength\parskip{\baselineskip}%
\chapter{DVCS Formalism}

\section{Theory bound nucleon DVCS}\label{sec:GPDs}
In the infinite-momentum frame, where the initial and the final nucleons go at 
the speed of light along the positive z-axis, the partons have relatively small 
transverse momenta compared to their longitudinal momenta. Referring to figure 
\ref{fig:GPDpartonicInterp}, the struck parton carries a longitudinal momentum 
fraction $x+\xi$ and it goes back into the nucleon with a momentum fraction 
$x-\xi$. The GPDs are defined in the interval where $x$ and $\xi$~$\in$ [-1,1], 
which can be separated into three regions as can be seen in figure 
\ref{fig:GPDpartonicInterp}.  The regions are:
\begin{itemize}
 \item $x\in$ [$\xi$,1]: both momentum fractions $x+\xi$ and $x-\xi$ are 
positive and the process describes the emission and reabsorption of a quark.
 \item $x\in $ [-$\xi$,$\xi$]: $x+\xi$ is positive reflecting the emission of a 
quark, while $x-\xi$ is negative and is interpreted as an antiquark being 
emitted from the initial proton.
 \item $x\in$ [-1,-$\xi$]: both fractions are negative, and $x+\xi$ and $x-\xi$ 
represent the emission and reabsorption of antiquarks.
\end{itemize}

The GPDs in the first and in the third regions represent the probability 
amplitude of finding a quark or an antiquark in the nucleon, while in the 
second region they represent the probability amplitude of finding a 
quark-antiquark pair in the nucleon \cite{Diehl:2001pm}.
   
   
Following the definition of reference \cite{Ji:1998pc}, the differential DVCS 
cross section is obtained from the DVCS scattering amplitude 
($\mathcal{T}_{DVCS}$) as:
\begin{equation}\label{DVCSCrossSection_tot}
\frac{d^{5}\sigma}{dQ^{2}\, dx_{B}\, dt\, d\phi\, d\phi_{e}} = 
\frac{1}{(2\pi^{4})32}\frac{x_{B}\, 
y^{2}}{Q^{4}}\bigg(1+\frac{4M^{2}x_{B}^{2}}{Q^{2}}\bigg)^{-1/2} 
|\mathcal{T}_{DVCS}|^{2},
\end{equation}
where $\phi_{e}$ is the azimuthal angle of the scattered lepton, 
$y=\frac{E-E'}{E}$ and $Q^{2},x_{B}, t, \phi$ are the four kinematic variables 
that describe the process. The variable $\phi$ is the angle between the 
leptonic and the hadronic planes, as can be seen in figure \ref{fig:phi}.
\begin{figure}[tbp]
\centering
\includegraphics[scale=0.40]{../NuclearGPD/fig_NuclGPD/DGLAP.png}
 \caption{The parton interpretations of the GPDs in three $x$-intervals 
[-1,-$\xi$], [-$\xi$,$\xi$] and [$\xi$,1]. The red arrows indicate the initial 
and the final-state of the proton, while the blue (black) arrows represent 
helicity (momentum) of the struck quark.} \label{fig:GPDpartonicInterp}
\end{figure}


\begin{figure}[tbp]
\centering
\includegraphics[scale=0.33]{../NuclearGPD/fig_NuclGPD/plane.png}
\caption{The definition of the azimuthal angle $\phi$ between the leptonic and 
the hadronic planes.} \label{fig:phi}
\end{figure}

By neglecting the mass of the quark with respect to the energies of 
$\gamma^{*}$ and $\gamma$, the DVCS scattering amplitude can be parametrized by 
four quark helicity conserving (chiral-even) GPDs: $H$, $E$, $\widetilde{H}$ 
and $\widetilde{E}$ as:
\begin{eqnarray}
\mathcal{T}_{DVCS} =  \sum_{q}(|e|Q_{q})^{2}\epsilon_{\mu}^{\ast} 
\epsilon_{\nu} \Bigg\lbrace \nonumber 
\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\, 
\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\, 
\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\, 
\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\, 
\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\\
g_{\perp}^{\mu \nu} \int_{-1}^{1} dx \,  \left[ \frac{1}{x-\xi+i\epsilon} 
+\frac{1}{x+\xi-i\epsilon} \right] \times \frac{1}{2} \bar{u}(p') \left[ H^{q} 
\gamma^{+} +  E^{q} i \sigma^{+ \alpha} \frac{\Delta_{\alpha}}{2m_{N}} \right] 
u(p)   \nonumber \\
  + i\epsilon^{\mu \nu + -} \int_{-1}^{1} dx \, \left[ 
\frac{1}{x+\xi-i\epsilon} -\frac{1}{x-\xi+i\epsilon} \right] \times \frac{1}{2} 
\bar{u}(p') \left[ \tilde{H}^{q} \gamma^{+}\gamma_{5} + \tilde{E}^{q} 
\gamma_{5} \frac{\Delta^{+}}{2m_{N}} \right] u(p) \Bigg\rbrace , \nonumber \\
 & &
\label{DVCS_Amplitude_4}
\end{eqnarray}
where $\bar{u}(p')$ and $u(p)$ are the spinors of the nucleon.
 
The GPDs $H$, $E$, $\widetilde{H}$ and $\widetilde{E}$ are defined for each 
quark flavor (q = u, d, s, ... ). Analogous GPDs exist for the gluons, see 
references \cite{Ji:1998pc,Radyushkin:1997ki,Goeke:2001tz} for details. In this 
work, we are mostly concerned by the valence quark region, in which the sea 
quarks and the gluons contributions do not dominate the DVCS scattering 
amplitude.

The GPDs $H$, $E$, $\widetilde{H}$ and $\widetilde{E}$ are called chiral-even 
GPDs because they conserve the helicity of the struck quark. The GPDs $H$ and 
$\widetilde{H}$ conserve the spin of the nucleon, while $E$ and $\widetilde{E}$ 
flip it. The $H$ and $E$ GPDs are called the unpolarized GPDs as they represent 
the sum over the different configurations of the quarks' helicities, whereas 
$\widetilde{H}$ and $\widetilde{E}$ are called the polarized GPDs because they 
are made up of the difference between the orientations of the quarks' 
helicities.

If one keeps the quark mass, another set of GPDs gives contribution to the DVCS 
amplitude. They are called chiral-odd GPDs. They give information about the 
quarks helicity-flip transitions. At leading twist, there are four chiral-odd 
GPDs that parametrize the helicity-flip structure of the partons in a nucleon: 
$H_{T}$, $E_{T}$, $\widetilde{H}_{T}$ and $\widetilde{E}_{T}$ 
\cite{PhysRevD.58.054006}.  Analogous set of chiral-odd GPDs exist for the 
gluon sector (see \cite{PhysRevD.58.054006, Diehl:2003ny}). The chiral-even 
GPDs contribute mostly in the regions where $\xi<x$ and $x<-\xi$, while the 
chiral-odd GPDs have larger contribution in the $x<|\xi|$ region 
\cite{Ji:1998pc}. 

\subsubsection{Basic properties of GPDs} \label{Properties_of_GPDs}

\paragraph{Links to the ordinary FFs and PDFs}

  Links between GPDs and the FFs are constructed by integrating the GPDs over 
the momentum fraction $x$ at given momentum transfer ($t$). Because of Lorentz 
invariance, integrating over $x$ removes all the references to the particular 
light-cone frame, in which $\xi$ is defined. Therefore, the result must be 
$\xi$-independent as can be see in equation \ref{EqGPDsFFlink}:
\begin{eqnarray}
  \int_{-1}^{1}dx \, H^{q}(x,\xi,t)=F^{q}_{1}(t), 	&  & 	\int_{-1}^{1}dx 
\, E^{q}(x,\xi,t)=F^{q}_{2}(t),  \, \, \,\, \, \, \,\, \, \, \,\,  \nonumber \\
  \int_{-1}^{1}dx \, \widetilde{H}^{q}(x,\xi,t)=G^{q}_{A}(t), 	&  & 	
\int_{-1}^{1}dx \, \widetilde{E}^{q}(x,\xi,t)=G^{q}_{P}(t), \, \, \,\, \, \, 
\,\, \, \, \,\, \label{EqGPDsFFlink}
\end{eqnarray}
 where $F^{q}_{1}(t)$ and $F^{q}_{2}(t)$ are the previously introduced Dirac 
and Pauli FFs, $G^{q}_{A}(t)$ and $G^{q}_{P}(t)$ are the axial and pseudoscalar 
electroweak FFs. The latter two can be measured in electroweak interactions; 
see reference \cite{Bernard:1996cc} for more details about the electroweak FFs.
 
From the optical theorem, the DIS cross section is proportional to the 
imaginary part of the forward amplitude of the doubly virtual Compton 
scattering (production of a spacelike ($Q^2<0$) virtual photon in the final 
state instead of a real photon) \cite{Guidal:2013rya}. In the limit $\xi 
\rightarrow $0 and t $\rightarrow$0, the GPDs are reduced to the ordinary PDFs, 
such that for the quark sector:
\begin{equation}
H^{q}(x,0,0) = q(x), ~~~~~~~~~~~~~  \widetilde H^{q}(x,0,0) = \Delta q(x),
\end{equation}
where $q(x)$ is the unpolarized PDF, defined for each quark flavor. The 
polarized PDFs $\Delta q(x)$ are accessible from polarized-beam and 
polarized-target DIS experiments. There are no similar relations for the GPDs 
$E$ and $\widetilde{E}$, as in the scattering amplitude, equation 
\ref{DVCS_Amplitude_4}, they are multiplied by factors proportional to $t~(= 
\Delta^{2}$), which vanish in the forward limit. Figure \ref{fig:GPDs_FFs_PDFs} 
summarizes the physics interpretations of the GPDs, the FFs, the PDFs, and the 
links between them.
\begin{figure}[tbp]
\hspace{-0.1in}
\includegraphics[scale=0.32]{../NuclearGPD/fig_NuclGPD/GPDs_FFs_PDFs.png}
\caption{The links between the GPDs and the ordinary FFs and PDFs. From left to 
right: the FFs reflect, via a Fourier transform, the two-dimensional spatial 
distributions of the quarks in the transverse plane; the PDFs give information 
about the longitudinal momentum distributions of the partons; finally, the GPDs 
provide a three-dimensional imaging of the partons in terms of both their 
longitudinal momenta and their position in the transverse space plane. The 
figure is from \cite{928a488bce7d45fa933f425b7213a349}. } 
\label{fig:GPDs_FFs_PDFs}
\end{figure}


\paragraph{Polynomiality of GPDs}

The GPDs have a key property which is the polynomiality. This property comes 
from the Lorentz invariance of the nucleon matrix elements. It states that the 
$x^n$ moment of the GPDs must be a polynomial in $\xi$ with a maximum order of 
n+1 \cite{Ji:1998pc}.
\begin{eqnarray}
         \int_{-1}^{1}dx \, x^{n} H^{q}(x,\xi,t) &=& \sum_{(even)i=0}^{n} 
(2\xi)^{i} A_{n+1,i}^{q}(t) + mod(n,2)(2\xi)^{n+1}C_{n+1}^{q}(t), ~~~~~~~~~~ \\
         \int_{-1}^{1}dx \, x^{n} E^{q}(x,\xi,t) &=& \sum_{(even)i=0}^{n} 
(2\xi)^{i} B_{n+1,i}^{q}(t) - mod(n,2)(2\xi)^{n+1}C_{n+1}^{q}(t), ~~~~~~~~~~ \\
        \int_{-1}^{1}dx \, x^{n} \widetilde{H}^{q}(x,\xi,t) &=& 
\sum_{(even)i=0}^{n} (2\xi)^{i} \widetilde{A}_{n+1,i}^{q}(t), ~~~~~~~~~~ \\
        \int_{-1}^{1}dx \, x^{n} \widetilde{E}^{q}(x,\xi,t) &=& 
\sum_{(even)i=0}^{n} (2\xi)^{i} \widetilde{B}_{n+1,i}^{q}(t). ~~~~~~~~~~
\label{Mellinmoment}
\end{eqnarray}
where $mod(n,2)$ is 1 for odd $n$ and 0 for even $n$. Thus, the corresponding 
polynomials contain only even powers of the skewedness parameter $\xi$. This 
follows from time-reversal invariance, i.e. $GPD(x,\xi,t)$ = $GPD(x,-\xi,t)$ 
\cite{Mankiewicz:1997uy}. This implies that the highest power of $\xi$ is $n+1$ 
for odd $n$ (singlet GPDs) and of highest power $n$ in case of even $n$ 
(non-singlet GPDs).  Due to the fact that the nucleon has spin 1/2, the 
coefficients in front of the highest power of $\xi$ for the singlet functions 
$H^q$ and $E^q$ are equal and have opposite signs. This sum rule is the same 
for the gluons \cite{Diehl:2003ny}.

As a consequence of the polynomiality of the GPDs, the first moments of GPDs 
lead to the ordinary form factors, as shown previously in this section. X.~Ji 
derived a sum rule \cite{PhysRevD.55.7114} that links the second moments of the 
quark GPDs $H^q$ and $E^q$, in the forward limit ($t=0$), to the total angular 
momentum ($J_{quarks}~=~\frac{1}{2} \Delta \Sigma + L_{quarks}$), where 
$\Delta\Sigma$ is the contribution of the quark spin to the nucleon spin and 
$L_{quarks}$ is the quarks orbital angular momentum contribution, as:
\begin{equation}
J_{quarks} = \frac{1}{2}\int_{-1}^{1} dx ~ x \left[H^{q}(x, \xi, t=0) + 
E^{q}(x, \xi, t=0) \right]
\label{jis}
\end{equation}
A similar expression exists for the gluons contribution ($J_{gluons}$).

The spin of a nucleon is built from the sum of the quarks' and the gluons' 
total angular momenta, $\frac{1}{2}=J_{quarks}+J_{gluons}$. Regarding the 
experimental measurements, the EMC collaboration \cite{Ashman:1989ig} has 
measured the contribution of the spins of the quarks ($\Delta \Sigma$) to the 
nucleon spin to be around 30$\%$. Therefore, measuring the second moments of 
the GPDs $H$ and $E$ will give access to the quarks orbital momentum 
($L_{quarks}$) which will complete the sector of the quarks in understanding 
the nucleon spin.  For the gluon total angular momentum ($J_{gluons}$), it is 
still an open question how to decompose $J_{gluons}$ into orbital 
($L_{gluons}$) and spin ($\Delta g$) components and to access them 
experimentally, see reference \cite{Lorce:2012rr} for more discussions on this 
subject.  
 

\subsubsection{Compton form factors}

The GPDs are real functions of two experimentally measurable variables, $\xi$ 
and $t$, and one unmeasurable variable, $x$, in the DVCS reaction.  Therefore, 
the GPDs are not directly measurable. In DVCS what we measure are the Compton 
Form Factors (CFFs) that are linked to the GPDs. As shown in equation 
\ref{DVCS_Amplitude_4}, the DVCS scattering amplitude, at leading order in 
$\alpha_{s}$ and leading twist, contains $x$-integrals of the form, 
$\int_{-1}^{+1} dx\frac{GPD^{q}(x,\xi,t)}{x\pm\xi \mp i\epsilon}$, where 
$\frac{1}{x \pm \xi +i\epsilon}$ is the propagator of the quark between the two 
photons. The integrals can be written as:
\begin{equation}
 \int_{-1}^{+1}dx \frac{GPD^{q}(x,\xi,t)}{x \pm \xi \mp i\epsilon} = 
\mathcal{P} \int_{-1}^{1}dx \frac{GPD^{q}(x,\xi,t)}{x \pm\xi} \pm i\pi 
GPD^{q}(x = \mp \xi,\xi,t),
\end{equation}
 where $\mathcal{P}$ stands for the Cauchy principal value integral. The DVCS 
amplitude can be decomposed into four complex CFFs, such that for each GPD 
there is a corresponding CFF. For instance, for the GPD $H^{q}(x,\xi,t)$, the 
real and imaginary parts of its CFF ($\mathcal{H}(\xi, t)$) at leading order in 
$\alpha_{s}$ can be expressed as:
\begin{subequations}
\begin{align}
 \mathcal{H}(\xi, t) =  \Re e(\mathcal{H})(\xi, t) - i\pi \Im 
m(\mathcal{H})(\xi, t)~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\\
\text{with~~~~~~~~~~} \Re e(\mathcal{H})(\xi, t)  =  \mathcal{P} 
\int_{0}^{1}dx[H(x,\xi,t)-H(-x,\xi,t)] \, C^{+}(x,\xi)\\ \text{and ~~~~~~~~~~~} 
\Im m(\mathcal{H})(\xi, t)  =  
H(\xi,\xi,t)-H(-\xi,\xi,t),~~~~~~~~~~~~~~~~~~~~~~~~
\end{align}
\end{subequations}
 where the term corresponding to the real part is weighted by $C^{+}(x,\xi)$ 
($=  \frac{1}{x-\xi} + \frac{1}{x+\xi}$), which appears also in an analogous 
expression for the GPD $E^{q}(x,\xi,t)$. The real parts of the CFFs that are 
associated with the GPDs $\widetilde{H}^{q}(x,\xi,t)$ and 
$\widetilde{E}^{q}(x,\xi,t)$, are weighted by $C^{-}(x,\xi)$ ($=  
\frac{1}{x-\xi} - \frac{1}{x+\xi}$).  


\subsubsection{Bethe-Heitler}
Experimentally, the DVCS is indistinguishable from the Bethe-Heitler (BH) 
process, which is the reaction where the final photon is emitted either from 
the incoming or the outgoing leptons, as shown in figure \ref{fig:BH}. The BH 
process is not sensitive to GPDs and does not carry information about the 
partonic structure of the hadronic target. The BH cross section is calculable 
from the well-known electromagnetic FFs.
 
\begin{figure}[tbp]
\centering
\includegraphics[scale=0.30]{../NuclearGPD/fig_NuclGPD/BH.png}
\caption{Schematic for the Bethe-Heitler process. The final real photon can be  
emitted from the incoming electron (left plot) or from the scattered electron 
(right plot).} \label{fig:BH}
\end{figure}
 

The $ep\rightarrow ep\gamma$ differential cross section of a 
longitudinally-polarized electron beam on an unpolarized proton target can be 
written as \cite{PhysRevD.82.074010}:
\begin{equation}
\frac{d^{5}\sigma^{\lambda}}{dQ^{2} dx_{B} dt d\phi d\phi_{e}} = 
\frac{\alpha^{3}}{16 \pi^{2}} \frac{x_{B} \, y^{2}}{Q^{2} \sqrt{1 + (2x_{b}M_{N}/Q)^{2}}} \frac{
|\mathcal{T}_{BH}|^{2} + |{\mathcal{T}}_{DVCS}^{\lambda}|^{2} + {\mathcal{I}}_{BH*DVCS}^{\lambda}}{e^6} 
\label{sigdiff}
\end{equation}
where $\lambda$ is the beam helicity, ${\mathcal{T}}_{DVCS}$ is the pure DVCS 
scattering amplitude, ${\mathcal{T}}_{BH}$ is the pure BH amplitude and 
${\mathcal{I}}^{\lambda}_{BH*DVCS}$ represents the interference amplitude. At 
leading twist, A.~V.~Belitsky, D. Mueller and A. Kirchner have shown that these 
amplitudes can be decomposed into a finite sum of Fourier harmonics, the 
so-called BMK formalism \cite{PhysRevD.82.074010}, as:
\begin{equation}
|\mathcal{T}_{BH}|^{2} =  \frac{e^{6} (1 + \epsilon^{2})^{-2}}{x^{2}_{B} y^{2} 
t \mathcal{P}_{1}(\phi) \mathcal{P}_{2}(\phi)} \left[ c_{0}^{BH} + 
\sum_{n=1}^{2} \Bigg( c^{BH}_{n} \cos(n\phi) + s_{n}^{BH} \sin(\phi) \Bigg) 
\right] \label{TBH}
\end{equation}

\begin{equation}
|\mathcal{T}_{DVCS}|^{2} =  \frac{e^{6}}{y^{2} Q^{2}} \left[ c_{0}^{DVCS} + 
\sum_{n=1}^{2} \Bigg( c_{n}^{DVCS} \cos(n \phi) + \lambda s_{n}^{DVCS} \sin(n 
\phi)\Bigg) \right] \label{TDVCS}
\end{equation}

\begin{equation}
\mathcal{I}_{BH*DVCS} = \frac{\pm e^{6}}{x_B y^{3} t \, \mathcal{P}_{1}(\phi) 
\mathcal{P}_{2}(\phi)} \left[ c_{0}^{I} + \sum_{n=0}^{3} \Bigg( c_{n}^{I} 
\cos(n \phi) + \lambda s_{n}^{I} \sin(n \phi) \Bigg) \right] \label{Tinter} 
\end{equation}
where $\mathcal{P}_{1}(\phi)$ and $\mathcal{P}_{2}(\phi)$ are the BH 
propagators. The leading twist expressions of the DVCS, BH and interference 
Fourier coefficients on a proton target can be found in reference 
\cite{PhysRevD.82.074010}.  The $+$($-$) sign in the interference term stands 
for the negatively (positively) charged lepton beam. In the case of an 
unpolarized proton target, the coefficients of the $\sin(\phi)$ in the BH 
amplitude are zeros.



