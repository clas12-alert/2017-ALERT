\setlength\parskip{\baselineskip}%
-------------------------------------------------------------------------------
\chapter{The Kinematics of Spectator-Tagged DVCS}\label{chap:appendixKine}

This appendix defines and discuses the kinematics of spectator-tagged DVCS. We 
will begin by defining the basic kinematic variables and the plane-wave impulse 
approximation (PWIA). This is followed by an analysis of the fully exclusive 
kinematics where all final-state particles are detected and a discussion of how 
to leverage this extra information for studying FSIs. 

%===============================================================================
\section{Incoherent DVCS Kinematic Variables}

\subsection{Experimentally Measured Variables}

The four-momenta in the tagged incoherent DVCS reaction are defined in 
Figure~\ref{fig:appendDVCSkinematics}.
The momenta are explicitly
\begin{align}
  k_1&=(k_1 ,\bm{k}_1 \simeq k_1^0) &  \label{eq:momenta1}
   k_2&=(k_2 ,\bm{k}_2 \simeq k_2^0) && \text{for $e$ and $e^{\prime}$,}\\
   q_1&=(\nu_1,\bm{q}_1) &
   q_2&=(\nu_2,\bm{q}_2) && \text{for $\gamma^{*}$ and $\gamma$,}\\
   p_1&=(E_1,\bm{p}_1) &
   p_2&=(E_2,\bm{p}_2)&& \text{for initial and struck nucleon,}\\
   p_{A}&=(M_A,\bm{0}) & p_{A-1}&=(E_{A-1},\bm{p}_{A-1})  && \text{for target 
   and spectator nucleus,}\label{eq:momenta2}
\end{align}
and the virtual and real photon momenta are
\begin{align}
   |\bm{q}_1| &= \sqrt{Q^2+\nu_1^2} &\quad& \text{and} & |\bm{q}_2| &= \nu_2\,\text{.} &
\end{align}
The virtual photon energy and four-momentum squared are
\begin{align}
  \nu_1 &= k_1^0 - k_2^0 &\quad& \text{and} & Q^2 &= -q_1^2 = -(k_1-k_2)^2 \simeq 4 k_1^0k_2^0 \sin^2(\theta_{k_1k_2}). &
\end{align}
\begin{figure}
   \centering
   \includegraphics[width=0.60\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure5.pdf}
\caption{\label{fig:appendDVCSkinematics}Tagged incoherent DVCS with labeled momenta.}
\end{figure}

For the remainder of this chapter we will be considering the process where all 
particles are detected, i.e., $^4$He$(e,e^{\prime}\,\gamma\, p\,+^3$H$)$. The 
incident and scattered electron momenta are experimentally well-determined, thus, 
the virtual photon four-momentum is well defined. The real photon energy and 
direction is measured in the electromagnetic calorimeter and the struck nucleon 
is also detected in the forward CLAS12 detector. The spectator system is 
identified in ALERT, which also measures its momentum. And finally the initial 
nucleus is at rest with mass $M_A$.
Therefore all the momenta in equations \ref{eq:momenta1}--\ref{eq:momenta2} 
(and Figure~\ref{fig:appendDVCSkinematics}) are determined with the exception 
of the initial struck nucleon, $p_1$.  We will return to determining this in 
section~\ref{sec:PWIA}.

\subsection{Momentum Transfer}

The Mandelstam variable $t$ is the square of the momentum transfer and can 
be calculated on the photon side of the diagram or the hadron side of the diagram 
as illustrated in Figure~\ref{fig:Handbag2}.
We define the former as
\begin{align}
   t_q &= (q_1-q_2)^2 \\
     &= -Q^2 -2\nu_2(\nu_1-q_1\cos\theta_{q_1q_2}) \label{eq:tqDef}
\end{align}
and the latter as
\begin{align}
   t_p &= (p_1-p_2)^2 \\
       &= 2M^2 -2(E_1E_2 - \bm{p}_1\cdot\bm{p}_2)\label{eq:tpDef}.
\end{align}
\begin{figure}[!htb]
   \centering
   \includegraphics[width=0.40\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure22.pdf}
   \caption{\label{fig:Handbag2}The DVCS handbag diagram showing the two ways 
   the momentum transfer can be calculated. }
\end{figure}

\subsubsection{Nucleon at Rest}

For DVCS on a fixed proton target, the momentum transfer can be calculated
from the virtual photon momentum, $q_2$, and the \emph{direction} of the 
real photon,
$\hat{\bm{q}}_2=(\cos\phi_{q_2}\sin\theta_{q_2},\sin\phi_{q_2}\sin\theta_{q_2},\cos\theta_{q_2})$.
The momentum transfer squared in this case is calculated as
\begin{equation}\label{eq:tgammagamma}
   \boxed{ t_{\gamma\gamma} = \frac{-Q^2 -2\nu_1( \nu_1 - q_1 \cos\theta_{q_1q_2})}{1+(\nu_1-q_1 \cos\theta_{q_1q_2})/M}}
\end{equation}
where the angle between the virtual and real photons is $\theta_{q_1q_2}$.  

\begin{derivation}[$t_{\gamma\gamma}$ for nucleon at rest.]
  Equation~(\ref{eq:tgammagamma}) is obtained from equation~(\ref{eq:tPhotons}) and 
the real photon's energy determined using the initial nucleon at rest. 
Using the struck nucleon's invariant mass we get
\begin{align*}
   p_2^2 &= M^2\\
         &= M^2 -Q^2 + 2\left(M_1(\nu_1-\nu_2)-\nu_1\nu_2 + \bm{q}_1\cdot\bm{q}_2\right).
\end{align*}
This equation becomes
\begin{align}
   \frac{Q^2}{2} &= \nu_1M_1-\nu_2M_1-\nu_1\nu_2 + \bm{q}_1\cdot\bm{q}_2
\end{align}
which can be solved for $\nu_2$ to yield 
\begin{equation}\label{eq:nu2AtRest}
  \boxed{\nu_2 = \frac{Q^2/2-\nu_1M}{|\bm{q}_1|\cos\theta_{q_1q_2} - M -\nu_1}\,.}
\end{equation}
Putting (\ref{eq:nu2AtRest}) into (\ref{eq:tqDef}) yields the result of (\ref{eq:tgammagamma}).
\end{derivation}

\subsubsection{Bound Nucleon with Fermi Motion}\label{sec:appendxBoundNfermi}

Equation~(\ref{eq:tgammagamma}) is a special case of the more general situation 
where the initial nucleon is not at rest in the lab frame. This is the case 
for a nucleon with non-zero Fermi motion or an electron-proton collider lab 
frame.  Unlike the nucleon at rest case we cannot eliminate \emph{both} $p_1$ 
and $p_2$, instead, we have only the option of eliminating one. This is not a 
problem for an electron-proton collider, where $p_1$ is constant, since we can 
just boost to the frame with $p_1=(M,\bm{0})$ and the analysis can be carried 
out consistently. However, a bound nucleon in a nucleus makes for a lousy 
collider because every scattering event would require a unique analysis frame.

It should be emphasized that the two possible choices above lead to a unique 
opportunity for studying tagged DVCS where the final state is fully detected.  
We will return to this in section \ref{sec:appendixFSIs} only after defining 
the PWIA in section \ref{sec:PWIA}.


\subsection{\texorpdfstring{$t_{\text{min}}$}{t-min} and \texorpdfstring{$t_{\text{max}}$}{t-max}}

The minimum and maximum momentum transfer are easily understood in the virtual 
photon-nucleon center-of-momentum (CM) frame which is shown in 
Figure~\ref{fig:NgammaCM}.
We begin by deriving the real photon's energy in this frame which will be 
useful for deriving further relations between frames.
\begin{figure}
  \centering
  \includegraphics[width=0.60\textwidth]{../tikz/DVCS/DVCS_diagrams-figure6.pdf}
\caption{\label{fig:NgammaCM}Nucleon-photon center-of-momentum system.}
\end{figure}

\begin{derivation}[Real photon energy in the CM frame.]
The center-of-mass energy squared calculated from the final state momenta
is
\begin{align}
   s &= M^2 + 2(E_2\nu_2+|\bm{p}_2||\bm{q}_2|) \\
          &= M^2 + 2\nu_2(\sqrt{\nu_2^2+M^2}+\nu_2) \label{eq:sCMnu2}
\end{align}
where we have used the CM relation $\bm{p}_2=-\bm{q}_2$ and the
fact that the final state nucleon and photon are both on-shell.
Solving (\ref{eq:sCMnu2}) for $\nu_2$ yields
\begin{equation}
   \boxed{ \nu_2^{CM} = \frac{s-M^2}{2\sqrt{s}}}
\end{equation}
where we label the result explicitly as a CM value.
\end{derivation}

The minimum momentum transfer corresponds to the scenario in the CM system
where the virtual photon loses just enough momentum as to become on-shell. That 
is, it transfers only enough momentum to become a real photon and continues to 
propagate in the same direction.  This corresponds to the case where 
$\theta_{\gamma\gamma}=0$.
Using this value in (\ref{eq:tqDef}) gives
\begin{equation}\label{eq:tminCM}
   \boxed{t_{\text{min}} = -Q^2 - \left(\frac{s-M^2}{\sqrt{s}}\right)(\nu_1^{CM}- q_1^{CM})}
\end{equation}
where we have explicitly labeled the frame dependent quantities. Quite similarly, the
maximum momentum transfer corresponds to the case where the particles scatter 
in the opposite direction of their initial momentum. This corresponds to 
$\theta_{\gamma\gamma}=\pi$, yielding the maximum momentum transfer
\begin{equation}\label{eq:tmaxCM}
   \boxed{t_{\text{max}} = -Q^2 - \left(\frac{s-M^2}{\sqrt{s}}\right)(\nu_1^{CM}+q_1^{CM})}.
\end{equation}

As a check, in the case of a real initial photon ($Q^2=0$),
\begin{align}
  t_{\text{min}} &\rightarrow 0  & \text{and} t_{\text{max}}&\rightarrow 
  \left(\frac{M^2-s}{\sqrt{s}}\right)2\nu_1^{CM}.
\end{align}
In the high energy limit were $M^2 \ll s$ or in the massless case  where 
$M$ terms are neglected we find 
\begin{align}
   s &\rightarrow (2 \nu_1^{CM})^2 & \text{and} && 
   t_{\text{max}}&\rightarrow -s
\end{align}
where we find the maximum momentum transfer is simply all the available momentum.

We now need relations for the CM energies between the photon-nucleon CM frame, lab frame, and the frame 
where the initial nucleon is at rest. The CM energy squared in each of these frames is
\begin{align}
   s &= -Q^2 +M^2 +2\left(\nu_1 E_1 - |\bm{q}_1||\bm{p}_1| \cos\theta_{p_1q_1}\right) && \text{any frame} \label{eq:sLabFrame}\\
   s &= -Q^2 +M^2 +2\left(\nu_1^{R} M\right) && \text{$p_1$ rest frame}\label{eq:sRestFrame}\\
   s &= -Q^2 +M^2 +2\left(\nu_1^{CM}\sqrt{|\bm{q}_1^{CM}|^2+M^2} - |\bm{q}_1^{CM}|^2\right) && \text{CM frame}\label{eq:sCMFrame}
\end{align}
where the rest frame and CM frame variables are labeled with $R$ and $CM$, respectively,
while the lab frame variables are not labeled.

Using (\ref{eq:sLabFrame}) and (\ref{eq:sRestFrame}) we find the relation between the 
nucleon rest frame and the lab
\begin{align}
   \nu_1^{R} &= \frac{1}{M}\left(\nu_1 E_1 - |\bm{p}_1||\bm{q}_1|\cos\theta_{p_1q_1} \right) && \text{Lab to nucleon rest frame} \label{eq:nu1R}
\end{align}
and similarly for (\ref{eq:sCMFrame}) and (\ref{eq:sRestFrame})
\begin{align}
\nu_1^{R} &= \frac{1}{M}\left(\nu_1^{CM} 
\sqrt{|\bm{q}_1^{CM}|^2+M^2}+|\bm{q}_1^{CM}|^2 \right) && \text{CM to nucleon 
rest frame.} \label{eq:nu1R2}
\end{align}
Equation \ref{eq:nu1R2} can be turned around and solved for $\nu_1^{CM}$ since 
we need it to calculate the kinematic limits above.
This gives
\begin{equation}
   \boxed{\nu_1^{CM} = 
   \sqrt{\frac{\left(M\nu_1^{R}-Q^2\right)^2}{M^2+2M\nu_1^{R}-Q^2}}}
\end{equation}
which, along with (\ref{eq:nu1R}), can be quite useful for evaluating $t_{min}$ 
and $t_{max}$.

\section{Plane Wave Impulse Approximation}\label{sec:PWIA}

In the following sections we discuss the plane-wave impulse approximation and 
how it \emph{provides a framework for comparison}, even for 
kinematics where it is not expected to apply. We conclude with a detailed 
discussion of the kinematic issues raised around about Fermi motion in 
section~\ref{sec:appendxBoundNfermi}.

\subsection{PWIA Definition}

The plane-wave impulse approximation is a simple model for calculating   an 
incoherent scattering from a bound nucleon.  The PWIA assumes 
\cite{Whelan:1339352}~i) the virtual photon is absorbed by a single nucleon, 
and ii) this nucleon is also the nucleon detected, and iii) this nucleon leaves 
the nucleus without interacting with the A-1 spectator system.  This implies 
the recoiling spectator system has a momentum opposite that of the initial 
struck nucleon,
\begin{equation}\label{eq:p1offshell}
   \bm{p}_1 = -\bm{p}_{A-1}.
\end{equation}
Furthermore, this approximation also implies that the spectator system is 
on-shell, {\it i.e.},
\begin{equation}
   E_{A-1}=\sqrt{|\bm{p}_{A-1}|^2 - M_{A-1}^2}.
\end{equation}
Noting  the initial nucleon can be off-shell, we introduce the following 
definition of the initial nucleon's invariant mass
\begin{equation}
   p_1^2 = \bar{M}^2 \ne  M_N^2.
\end{equation}
The ``off-shellness'' of the struck nucleon is typically characterized by 
$0.7 \lesssim \bar{M}/M_N < 1$.

\subsection{FSI and Off-shellness}\label{sec:appendixFSIs}

From its definition, the PWIA implies all the ``off-shellness'' goes with 
initial nucleon. Where this not the case, the spectator system would be left  
off-shell, and thus, necessitate some final state interaction to put it 
on-shell prior to detection. So here we should emphasize that the PWIA is not 
used throughout this proposal because the authors think it is a correct or even 
a good approximation, but rather, \emph{because it provides a basis for 
comparison}.

\subsection{Measuring Off-shellness in the PWIA}

The off-shell mass of the nucleon can be determined two different ways with the 
PWIA and a fully detected final state. Starting first with the direct approach 
using the spectator
\begin{align*}
  \bar{M}^2_{(0)} &= (p_A - p_{A-1})^2 \\
                  &= M_A^2 + M_{A-1}^2 - 2 M_A E_{A-1}.\label{eq:Mbar0}\numberthis
\end{align*}
The momenta used in this calculation are highlighted in 
Figure~\ref{fig:offshelldiagram}'s left diagram. The second way to calculate 
the off-shell mass is to use the invariant $p_1^2$ with all the other momenta 
not used in the previous calculation. This yields
\begin{align}
   \begin{split}
     \bar{M}^2_{(1)}(q_1,q_2,p_2) &= M^2  - Q^2 + 2 E_2 (\nu_1 + \nu_2) \\
             &\quad- 2 \left(\nu_1 \nu_2 +  \nu_2 |\bm{p}_2|\cos\theta_{p_2q_2} - \nu_2 |\bm{q}_1| \cos\theta_{q_1q_2} + 
   |\bm{p}_2| |\bm{q}_1|\cos\theta_{p_2q_1}\right)\label{eq:Mbar1}.
   \end{split}
\end{align}
The last way we calculate $\bar{M}$ is to start with the struck nucleon 
invariant mass to eliminate $p_2$ from the expression.  This results in a 
slight more complicated expression:
\begin{align}
  \bar{M}^2_{(2)}(q_1,q_2,p_1) &= \frac{1}{2(\nu_1-\nu_2)}\sqrt{ 
  (a_{\bar{M}}+Q^2+2\bm{q}_1\cdot\bm{p}_1)(b_{\bar{M}}+Q^2+2\bm{q}_1\cdot\bm{p}_1) 
} \label{eq:Mbar2} \\
   \intertext{where}
   a_{\bar{M}} &= 2\nu_1(\nu_2+|\bm{p}_1|)- 
   2\nu_2|\bm{p}_1|(\cos\theta_{p_1q_2}+1+\frac{|\bm{q}_1|}{|\bm{p}_1|}\cos\theta_{q_1q_2})\label{eq:offshellnessA}\\
   b_{\bar{M}} &= 2\nu_1(\nu_2-|\bm{p}_1|)- 
   2\nu_2|\bm{p}_1|(\cos\theta_{p_1q_2}-1+\frac{|\bm{q}_1|}{|\bm{p}_1|}\cos\theta_{q_1q_2}).
\end{align}
The initial nucleon momentum, $\bm{p}_1$, is calculated from the target and 
spectator nuclei using the PWIA.
It is worth noting that $\bar{M}_{(1)}$ does not depend on $p_1$ and  
$\bar{M}_{(2)}$  does not depend on $p_2$. The dependent momenta for each 
calculation are shown in Figure~\ref{fig:offshelldiagram}.
\begin{figure}
  \centering
   \includegraphics[width=0.31\textwidth]{../tikz/DVCS/DVCS_diagrams-figure4.pdf}\quad
   \includegraphics[width=0.31\textwidth]{../tikz/DVCS/DVCS_diagrams-figure3.pdf}\quad
   \includegraphics[width=0.31\textwidth]{../tikz/DVCS/DVCS_diagrams-figure2.pdf}
\caption{\label{fig:offshelldiagram}Highlighted in red are the momenta used to 
calculate the off-shell mass. The diagrams from left to right correspond to  
$\bar{M}_{(0)}$ (\ref{eq:Mbar0}), $\bar{M}_{(1)}$ (\ref{eq:Mbar1}), and 
$\bar{M}_{(2)}$ (\ref{eq:Mbar2}).  }
\end{figure}

\subsection{Photon Energy as FSI Indicator}\label{sec:nu2FSIindicator}

\paragraph{Calculating $\bm{\nu_2}$.}
%Exclusive DVCS \ref{fig:Handbag2}
Using the over-determined kinematics we can calculate the real photon energy two 
different ways.
\begin{derivation}[$\nu_2(p_2,\bm{\hat{q}}_2, \bar{M}, q_1)$]
  \begin{align*}
    p_2^2 &= M^2\\
          &= -Q^2 +\bar{M}^2 + 2\left(E_1(\nu_1-\nu_2)-\nu_1\nu_2- (\bm{q}_1 - \bm{q}_2)\cdot\bm{p}_1 + \bm{q}_1\cdot\bm{q}_2\right).\numberthis
  \end{align*}
  The last equation becomes
  \begin{align}
    \frac{M^2 - \bar{M}^2 + Q^2}{2} &= \nu_1E_1-\nu_2E_1-\nu_1\nu_2-\bm{q}_1\cdot\bm{p}_1 + \bm{q}_2\cdot\bm{p}_1 + \bm{q}_1\cdot\bm{q}_2
  \end{align}
  which can be solved for $\nu_2$ to yield \begin{equation}\label{eq:nu2A}
    \boxed{
      \nu_2^{(1)} = 
      \frac{(M^2-\bar{M}^2+Q^2)/2-\nu_1E_1+|\bm{q}_1|\,|\bm{p}_1|\cos\theta_{p_1q_1}}
    {|\bm{q}_1|\cos\theta_{q_1q_2} + |\bm{p}_1|\cos\theta_{p_1q_2} - E_1 -\nu_1}}~.
  \end{equation}
  In the case of an on-shell nucleon at rest in the lab 
  ($|\bm{p}_1|\rightarrow0$), (\ref{eq:nu2A}) reduces to (\ref{eq:nu2AtRest}).
\end{derivation}
\begin{derivation}[$\nu_2(p_2,\bm{\hat{q}}_2,\bar{M}, q_1)$]
  Solving for the invariant mass of $p_1$
  \begin{align*}
    p_1^2 &= \bar{M}^2\\
    \begin{split}
      &= -Q^2 +M^2 + 2\Big(\nu_2(q_1\cos\theta_{q_1q_2} -\nu_1 +E_2-|\bm{p}_2| \cos\theta_{p_2q_2})\\
      &\quad\quad -\nu_1E_2 + |\bm{q}_1||\bm{p}_2|\cos\theta_{q_1p_2}\Big)
    \end{split} \numberthis
  \end{align*}
  This becomes
  \begin{equation}\label{eq:nu2B}
    \boxed{
      \nu_2^{(2)} = \frac{(\bar{M}^2 - M^2+Q^2)/2+\nu_1E_2 
      -|\bm{q}_1||\bm{p}_2|\cos\theta_{q_1p_2}}
      {|\bm{q}_1|\cos\theta_{q_1q_2} -|\bm{p}_2| \cos\theta_{p_2q_2}-\nu_1 +E_2}.
    }
  \end{equation}
  %This also reduces to (\ref{eq:nu2AtRest}) in the case of an on-shell nucleon 
  %at rest.
\end{derivation}

\paragraph{Calculating $\bm{t}$.}
We can now put solutions (\ref{eq:nu2A}) and (\ref{eq:nu2B}) into 
(\ref{eq:tqDef}) to obtain the analogues of $t_{\gamma\gamma}$ in 
(\ref{eq:tgammagamma}) for the case of a bound nucleon with Fermi motion as 
discussed in section \ref{sec:appendxBoundNfermi}. The results are  
\begin{equation}\label{eq:tq1}
  \boxed{t_{q}^{(1)}  = -Q^2 -2(\nu_1-|\bm{q}_1|\cos\theta_{q_1q_2})\frac{(M^2 - \bar{M}^2+Q^2)/2-\nu_1E_1+|\bm{q}_1|\,|\bm{p}_1|\cos\theta_{p_1q_1}}
  {|\bm{q}_1|\cos\theta_{q_1q_2} + |\bm{p}_1|\cos\theta_{p_1q_2} - E_1 -\nu_1}}
\end{equation}
and
\begin{equation}\label{eq:tq2}
  \boxed{ t_{q}^{(2)} = -Q^2 -2(\nu_1-|\bm{q}_1|\cos\theta_{q_1q_2})\bigg[\frac{(\bar{M}^2 - M^2+Q^2)/2+\nu_1E_2 -|\bm{q}_1||\bm{p}_2|\cos\theta_{q_1p_2}}
  {|\bm{q}_1|\cos\theta_{q_1q_2} -|\bm{p}_2| \cos\theta_{p_2q_2}-\nu_1 +E_2}\bigg]}.
\end{equation}

%, \ref{eq:Mbar1}, and \ref{eq:Mbar2}. 



%The energy of the real photon can be calculated from (\ref{eq:nu2Full}) and (\ref{eq:nu2B}):
%\begin{align}
%  \nu_2^{(1)} &= \left(\frac{(\bar{M}^2 - M^2+Q^2)/2+\nu_1E_2 -|\bm{q}_1||\bm{p}_2|\cos\theta_{q_1p_2}}
%{|\bm{q}_1|\cos\theta_{q_1q_2} -|\bm{p}_2| \cos\theta_{p_2q_2} +E_2-\nu_1}\right)\label{eq:nu2CalcA}\\
%\nu_2^{(2)} &= \left(\frac{(M^2 - \bar{M}^2+Q^2)/2-\nu_1E_1+|\bm{q}_1|\,|\bm{p}_1|\cos\theta_{p_1q_1}}
%{|\bm{q}_1|\cos\theta_{q_1q_2} + |\bm{p}_1|\cos\theta_{p_1q_2} - E_1 -\nu_1}\right)\label{eq:nu2CalcB}~.
%\end{align}
%Here we have used maximal information on the photon side of the diagram while 
%introducing minimal dependence on $p_1$ or $p_2$. 
%Since we will measure $\nu_2$, $p_2$, and $p_{A-1}$, we can use the first equation
%and solve for $\bar{M}$ yielding (\ref{eq:Mbar1}) which uses the measured $\nu_2$ and $p_2$.
%Using this $\bar{M}$ in the second equation for $\nu_2$, we can compare this to the measured $\nu_2^{\text{exp}}$.

\paragraph{Identifying significant FSIs.}
The equations above for $\nu_2$ and $t$ require the off-shell mass, so we use 
the first PWIA result $\bar{M}_{(0)}$ in (\ref{eq:Mbar0}). To quickly summarize 
the procedure:
\begin{align}
   \big[\text{Eq.~(\ref{eq:Mbar1})}\big]  &\longrightarrow&  
   \bar{M}^{\text{calc}} &= 
   \bar{M}_{(1)}(p_2,\bm{\hat{q}}_2,\nu_2^{\text{exp}})\\
   \big[\text{Eq.~(\ref{eq:nu2B})}\big]            &\longrightarrow& 
   \nu_2^{\text{calc}} &= \nu_2^{(1)}(p_1,\bm{\hat{q}}_2,\bar{M}_{(0)}) \\
   \Big[\nu_2^{\text{calc}} \neq \nu_2^{\text{exp}},  \bar{M}^{\text{calc}} \ne \bar{M}_{(0)} \Big] &\longrightarrow &\text{PWIA} &~\text{modified by FSI.}
\end{align}
In the case where the initial nucleon is on-shell, this reduces to checking 
$\nu_2^{\text{exp}}$ against (\ref{eq:nu2B}). Furthermore, the momentum 
transfer  can be calculated from equation (\ref{eq:tq1}) and compared against 
$t_q$ to verify that we are indeed identifying those events where they differ 
significantly due to FSIs.
This analysis can be turned around, {\it i.e.}, $p_1$ and $p_2$ can be swapped 
in the procedure above:
\begin{align}
  \big[\text{Eq.~(\ref{eq:Mbar2})}\big]   &\longrightarrow& 
  \bar{M}^{\text{calc}} &= 
  \bar{M}_{(2)}(p_1,\bm{\hat{q}}_2,\nu_2^{\text{exp}}) \\
  \big[\text{Eq.~(\ref{eq:nu2A})}\big] &\longrightarrow& \nu_2^{\text{calc}}   
                                       &= 
  \nu_2^{(2)}(p_2,\bm{\hat{q}}_2,\bar{M}_{(0)}) \\
  \Big[\nu_2^{\text{calc}} \neq \nu_2^{\text{exp}},  \bar{M}^{\text{calc}} \ne 
  \bar{M}_{(0)} \Big]   &\longrightarrow  &\text{PWIA} &~\text{modified by FSI.}
\end{align}
Similarly, comparisons of (\ref{eq:tq2}) to $t_q$  can be used to to determine 
the effectiveness of selection cuts. We now will turn our attention to this 
point and try to understand things with a toy model of FSIs.

%Again, in the case where the initial nucleon is on-shell, this reduces to 
%checking $\nu_2^{\text{exp}}$ against (\ref{eq:nu2Full}).% and 
%(\ref{eq:nu2Full})
 
%Of course this method of identifying FSIs is limited by detector resolutions.
%So having the best resolution possible is ideal.
%{Identify how the ALERT resolutions affect the results.}
%
%Of course we would like to stick into (\ref{eq:nu2CalcA}) and (\ref{eq:nu2CalcA}) both
%the FSI and off-shell effects and solve the system of equations for $\bar{M}$ and $k$.
%However, the FSI substitutions for the momentum vectors in (\ref{eq:FSIsubs}) causes 
%the direction of $k$ to contribute as well. 
%
%This can be seen by calculating $p_1^2$ and including both effects:
%\begin{equation}
%   \bar{M}^2= E_1^2-|\bm{p}_{A-1}|^2- |\bm{k}|^2- \bm{k}\cdot\bm{p}_{A-1}
%\end{equation}
%where the last term introduces the nuisance directional dependence. Doing the same 
%for (\ref{eq:nu2CalcA}) adds further dependence on the angles $\theta_{kq_1}$ 
%and $\theta_{kq_2}$. Thus the system of equations  is under-constrained.


%===============================================================================

\section{Toy Model of FSIs}

In this section we discuss a simple toy model of FSIs which was developed in 
order to understand the usefulness of the fully measured final state.
First, we will discuss the toy model and emphasize that more theoretical work 
\emph{or experimental data} is needed before the models can be taken seriously.  
Then we will use the model to test how well the analysis outlined above 
isolates the events with kinematics that are inconsistent with the PWIA due to 
significant FSIs. 

\subsection{Modeling the FSI}

The FSIs were modeled as a single momentum exchange as illustrated in 
Figure~\ref{fig:toyModelFSI}. This is obviously far from realistic since any 
rigorous treatment will require amplitude-level calculations. However, as was 
already emphasized, we aim to separate those events which are no longer 
consistent with the PWIA using the momentum measured in the final state.  This 
leaves those events where the FSI exchange produces little kinematic difference 
from the PWIA but may affect the cross section at the amplitude level. With 
these things in mind, we can proceed with the details of the toy model.
\begin{figure}[!htb]
  \centering
   %{%
   %  \setlength{\fboxsep}{0pt}%
   %  \setlength{\fboxrule}{1pt} \fbox{
   \includegraphics[width=0.56\textwidth]{../tikz/DVCS/DVCS_diagrams-figure0.pdf}
 %}}
\caption{\label{fig:toyModelFSI}A toy model of FSIs.}
\end{figure}

To get a feel for the size of the off-shell nucleon in the PWIA, it is worth 
pointing out that the mass difference between $^4$He and $^3$H is
\begin{align}
   M_{^4\text{He}} - M_{^3\text{H}} &= (3.7284 - 2.80943) ~\text{GeV/c}^2 \\
           &= 0.91897~\text{GeV/c}^2\\
   &= 0.97945 M_p
\end{align}
and the mass difference between $^4$He and $^3$He is
\begin{align}
   M_{^4\text{He}} - M_{^3\text{He}} &= (3.7284 - 2.80941) ~\text{GeV/c}^2 \\
           &= 0.91899~\text{GeV/c}^2\\
   &= 0.97943 M_p~.
\end{align}
These differences give a rough estimate of the expected off-shellness in the 
case there are no FSIs present.
%Q2 = 2.6535245
%x0 = 0.15716207

A straightforward Monte Carlo was generated, and in order simplify the present 
analysis, the virtual photon kinematics were held fixed at
\begin{align*}
  \nu_1 &= 9~\text{GeV,} &  Q^2 &= 2.65~\text{GeV}^2,
\end{align*}
where for a nucleon at rest this would correspond to  $x=0.157$.
The final state was uniformly sampled from the Lorentz invariant phase space, 
that is, there is no physics in the generated events and therefore all the 
results shown are purely a result of kinematics. However, The initial nucleon 
momentum was sampled from an empirical fit to the nucleon momentum 
distributions and the direction isotropic. The FSI momentum exchanged was 
also isotropic with a value uniformly sampled in the range of $0 < k < 
0.2$~GeV/c.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.6\textwidth,clip,trim=20mm 2mm 20mm 8mm]{figs/dvcs_gamma2_Mass_1.pdf}
  \caption{\label{fig:toyModel1}The initial nucleon invariant mass without FSI 
  (blue and reduced by a factor of 5), with FSI turned on (black), and with the 
selection cut (red). See text for more details.}
\end{figure}

\subsection{Toy MC Results}

We now follow the analysis outlined section \ref{sec:nu2FSIindicator}. The 
results for the invariant mass of the initial nucleon calculated with and 
without FSIs are shown in Figure~\ref{fig:toyModel1}. Also shown in red are the  
events that pass a selection cuts:
\begin{align}\label{eq:deltaNu2}
  \Delta\nu_1^{(1)} &> 0.1~\text{GeV,} &\text{and} & &  \Delta\nu_1^{(2)} &> 
  0.1~\text{GeV.}
\end{align}
Furthermore, the results for $\Delta \nu_{(1,2)} = \nu_2^{exp} - \nu_2^{(1,2)}$
are shown in Figure~\ref{fig:toyModel3}.. The dashed histograms have a cut on 
the invariant mass $M_0 > 0.8$~GeV/c$^2$.
\begin{figure}[htb]
  \centering
  %{%
  %  \setlength{\fboxsep}{0pt}%
  %  \setlength{\fboxrule}{1pt} \fbox{
    \includegraphics[width=0.6\textwidth,clip, trim=17mm 4mm 22mm 8mm]{figs/dvcs_gamma2_Delta_nu_1.pdf}
%}}
  \caption{\label{fig:toyModel2}The real photon momentum difference as calculated 
  in equation (\ref{eq:deltaNu2}).  }
\end{figure}

The differences between the experimental momentum transfers are shown in 
Figure~\ref{fig:toyModel3}.
They are defined as
\begin{equation}\label{eq:deltattoy}
\Delta t_{(1,2)} = t_q - t_q^{(1,2)}
\end{equation}
where $t_q^{(1,2)}$ are calculated from (\ref{eq:tq1}) and (\ref{eq:tq2}) 
respectively, and $t_q$ is computed using the directly measured virtual and 
real photons.
\begin{figure}[!htb]
  \centering
  %{%
  %  \setlength{\fboxsep}{0pt}%
  %  \setlength{\fboxrule}{0.1pt} \fbox{
   \includegraphics[width=0.48\textwidth,clip, trim=17mm 4mm 22mm 8mm]{figs/dvcs_gamma2_deltat_1.pdf}
 %}}
   \includegraphics[width=0.48\textwidth,clip, trim=17mm 4mm 22mm 8mm]{figs/dvcs_gamma2_DeltatCut_1.pdf}
\caption{\label{fig:toyModel3}The results of equation (\ref{eq:deltattoy}) 
without selection cuts (solid) and with selection cuts (dashed). The right plot 
shows only the results after selection cuts.}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[clip, trim=17mm 4mm 22mm 8.5mm,width=0.7\textwidth]{figs/dvcs_gamma2_k0_FSI_1.pdf}
  \caption{\label{fig:k0FSI}The generated FSI momentum exchange with different  
  cuts applied to select events with small $k_0^{\text{FSI}}$.}
\end{figure}
Figures~\ref{fig:toyModel1}, \ref{fig:toyModel2}, and \ref{fig:toyModel3} 
clearly show that
events with significant FSIs which result in kinematic differences from the 
PWIA can be isolated. The fully exclusive measurement will allow for a unique 
opportunity to study FSIs in this manner.

In order to see the impact of these cuts we take a look at the distribution of 
events versus spectator angle in a fixed bin of ($x$, $Q^2$, $t$, $P_s$). We 
form the ratio of distributions where the denominator is the 
distribution of all events in that bin and numerator includes the specific cuts to 
select a certain type of FSI (small or large momentum exchange). This ratio is 
defined as
\begin{equation}\label{eq:AppendixFSIRatios}
  R=A \frac{N(x,Q^2,t,P_s|\text{FSI cut})}{N(x,Q^2,t,P_s)},
\end{equation}
where $A$ is an arbitrary normalization chosen so that the ratio of backward, 
low-$P_s$, and small $k_0^{\text{FSI}}$ is about 1. The efficacy of such a cut, 
as outlined above, at removing  events with significant FSIs can be seen in 
Figure~\ref{fig:k0FSI}.  For simplicity we limit ourselves to two bins in $P_s$ 
(high and low). We can compare the result against its (unrealistic) counterpart 
by cutting on the FSI momentum exchange as well, i.e., $k_0^{\text{FSI}} < 
15$~MeV or~$k_0^{\text{FSI}} > 15$~MeV which correspond to the solid and dashed 
histograms in Figure~\ref{fig:FSIRatios}.
\begin{figure}[htb]
  \centering
  \includegraphics[clip, trim=17mm 4mm 22mm 15mm,width=0.61\textwidth]{figs/dvcs_gamma2_ThetaSpec2_1.pdf}
  \caption{\label{fig:FSIRatios} Ratios (\ref{eq:AppendixFSIRatios})  for the 
    small (solid) and large (dashed) FSI momentum exchange.  The plots labeled 
    with $\overline{\text{cuts}}$ indicate the events are selected with the inverse 
    of the cut discussed above, which in this case will select events with 
  significant FSIs.}
\end{figure}

\subsubsection{Noteworthy Features}

\interfootnotelinepenalty=500
Here we note some observations which are not integral to this proposal but are 
rather interesting. As Figure~\ref{fig:FSIRatios} shows, the experimental cuts 
quite effectively act like a cut on the FSI momentum exchanged. Using this to 
our advantage we could take this one step further and isolate the effects for a 
given momentum exchange by systematically varying the effective 
$k_0^{\text{FSI}}$ cut (e.g. by loosening the values) and subtracting the 
difference\footnote{This is somewhat analogous to using a bremsstrahlung photon 
  beam at different energies in photo-production experiments. Taking the 
  cross-section difference at slightly different beam energies allows the 
  contribution from the high-energy tip of the bremsstrahlung spectrum to be 
isolated.}.  However, we must remember that this is just a crude model and 
reality may be quite different.  Furthermore, even in this simple example we do 
not know how to measure the value of $k_0^{\text{FSI}}$.  We only have cuts 
which are roughly proportional to a range of FSI momentum exchange with an 
unknown proportionality constant.

\subsection{Concluding Remarks}

The FSI problem cannot escape model dependence. The strategy outlined above 
uses the over-determined kinematics to get a handle on FSIs. A PWIA analysis 
permits a kinematic separation \emph{at the event level} yielding roughly two 
event types: i) events with FSI causing significant deviation from PWIA 
kinematics, and ii) events with FSI that produce no discernible difference when 
compared to the PWIA result.

The first type of event can be removed if the goal is finding maximally FSI-free 
events, however, these events are invaluable for studying various models of 
FSIs. The over-determined kinematics will give an extra handle to test various 
models. Using models in agreement with type (i) events, the events of type (ii) 
can be systematically corrected (or even justified to be negligible). More 
theoretical input is needed for accurately modeling but this does not affect 
the impact of this proposals' result.

Perhaps the ultimate extension of the experimental setup would be to measure 
the induced polarization of the struck nucleon. This would be a DVCS version of 
the quasi-elastic scattering experiments where $P_{y}$ gives a measure of FSIs.  
This too would have a model dependence but the combination would be quite 
powerful in understanding the FSIs\footnote{However, this method would require 
a new large recoil polarimeter which does not seem feasible at the moment.}.

