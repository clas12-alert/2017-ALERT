\setlength\parskip{\baselineskip}%
\chapter{Formalism and Experimental Observables}
\label{chap:formalism}
%
\section{Deeply Virtual Compton Scattering}
%
The cross section for DVCS on a spin-1/2 target can be parameterized in terms 
of four helicity conserving GPDs: $H^q$, $E^q$, $\tilde{H}^q$, and 
$\tilde{E}^q$. For spin-0 targets, such as $^{4}$He, the cross section is 
parameterized with just one helicity conserving GPD~\cite{Guzey:2003jh}. For 
spin-1 targets like the deuteron, the cross section is parameterized with nine 
GPDs~\cite{Cano:2003ju,Kirchner:2003wt}.

\begin{figure}
   \centering
   \includegraphics[width=0.60\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure5.pdf}
   \caption{\label{fig:dvcsMomenta}Incoherent DVCS process with the momentum 
   definitions labeled.}
\end{figure}

The DVCS cross section is written as
\begin{equation}
\frac{d\sigma}{dx_A\,dy\,dt\,d\phi\,d\varphi} = \frac{\alpha^3 x_A y}{16 \pi^2 
Q^2 \sqrt{1+\epsilon^2}} \left| \frac{\mathcal{T}}{e^3} \right|^2
\end{equation}
where
\begin{equation}
   \epsilon \equiv 2x_A \frac{M_A}{Q},
\end{equation}
$x_A=Q^2/(2p_1\cdot q_1$) is the scaling variable, $y= (p_1\cdot q_1)/(p_1\cdot 
k_1)$ is the photon energy fraction, $\phi$ is the angle between the leptonic 
and hadronic planes, $\varphi$ is the scattered electron's azimuthal angle, $Q^2= 
-q_1^2$, and $q_1=k_1-k_2$. The particle momentum definitions are shown in 
Figure~\ref{fig:dvcsMomenta}. We use the BMJ\footnote{The Belitsky, M\"{u}ller, and
Ji reference frame. See \cite{Braun:2014paa} for a nice discussion of the 
various reference frames.} 
convention~\cite{Braun:2014paa,Belitsky:2001ns,Belitsky:2010jw,Belitsky:2012ch} 
for defining the momentum transfer where the target nucleus is initially at 
rest, $\Delta = p_1-p_2$ and $t=\Delta^2$. The Bjorken variable  is related to 
the scaling variable by
%
\begin{equation}
   x_{\text{B}} = \frac{Q^2}{2 M_N E\,y} \simeq A x_A
\end{equation}
%
where $M_N$ is the nucleon mass and $E$ is the beam energy. Another scaling variable called skewedness is
%
\begin{equation}
\xi = \frac{x_A}{2 - x_A} + \mathcal{O}(1/Q^2)
\end{equation}
where the power suppressed contributions originate with the selection of the 
BMJ frame convention needed to unambiguously define the leading-twist 
approximation used in this proposal~\cite{Braun:2014paa}.

The amplitude is the sum of the DVCS and Bethe-Heitler (BH) amplitudes, and 
when squared has terms
\begin{equation}
   \mathcal{T}^2 = \left|\mathcal{T}_{\text{BH}}\right|^2 + 
   \left|\mathcal{T}_{\text{DVCS}}\right|^2 + \mathcal{I}
\end{equation}
where the first is the BH contribution, the second is the DVCS part, and the last 
term is the interference part,
\begin{equation}
   \mathcal{I} = \mathcal{T}_{\text{DVCS}}\mathcal{T}_{\text{BH}}^{*} + 
   \mathcal{T}_{\text{DVCS}}^{*}\mathcal{T}_{\text{BH}}.
\end{equation}
The corresponding amplitudes are calculated with the diagrams shown in 
Figure~\ref{fig:DVCShandbag}. The details of contracting the DVCS tensor with 
various currents and tensors can be found in~\cite{Kirchner:2003wt}.
\begin{figure}[!hbt]
   \centering
   \includegraphics[width=0.20\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure1.pdf}
   \includegraphics[width=0.20\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure2.pdf}
   \includegraphics[width=0.20\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure3.pdf}
   \includegraphics[width=0.20\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure4.pdf}
   \caption{\label{fig:DVCShandbag}DVCS handbag diagram and BH contributions 
   used for calculating DVCS amplitudes.}
\end{figure}
%
The resulting expressions for the amplitudes are
\begin{align}
   \left|\mathcal{T}_{\text{BH}}\right|^2 &= 
   \frac{e^6(1+\epsilon^2)^{-2}}{x_A^2\,y^2\,t\,
   \mathcal{P}_1(\phi)\mathcal{P}_2(\phi)} \left\{ c_0^{\text{BH}} + 
   \sum_{n=1}^{2}\left[ c_n^{\text{BH}}\cos(n\phi) +s_n^{\text{BH}}\cos(n\phi) 
\right] \right\} \\
\left|\mathcal{T}_{\text{DVCS}}\right|^2 &= \frac{e^6}{y^2\,Q^2}\left\{ 
c_0^{\text{DVCS}} + \sum_{n=1}^{2}\left[ c_n^{\text{DVCS}}\cos(n\phi) 
+s_n^{\text{DVCS}}\cos(n\phi) \right] \right\}\\
   \mathcal{I} &= \frac{e^6(1+\epsilon^2)^{-2}}{x_A\,y^3\,t\,
   \mathcal{P}_1(\phi)\mathcal{P}_2(\phi)}\left\{ c_0^{\mathcal{I}} + 
   \sum_{n=1}^{3}\left[ c_n^{\mathcal{I}}\cos(n\phi) 
+s_n^{\mathcal{I}}\cos(n\phi) \right] \right\}
\end{align}
%
The functions $c_0$, $c_n$, and $s_n$ are called \emph{Fourier coefficients} 
and they depend on the kinematic variables and the operator decomposition of 
the DVCS tensor for a target with a given spin. At leading twist there is a 
straightforward form factor decomposition which relates the vector and 
axial-vector operators with the so-called Compton form factors 
(CFFs)~\cite{Belitsky:2000gz}. The Compton form factors appearing in the DVCS 
amplitudes are integrals of the type
%
\begin{equation}
   \mathcal{F} = \int_{-1}^{1} dx F(\mp x,\xi,t) C^{\pm}(x,\xi)
\end{equation}
where the coefficient functions at leading order take the form
\begin{equation}
   C^{\pm}(x,\xi) = \frac{1}{x-\xi + i\epsilon} \pm \frac{1}{x+\xi - 
   i\epsilon}.
\end{equation}
%
We plan on measuring the beam spin asymmetry as a function of $\phi$
\begin{equation}
   A_{LU}(\phi) = \frac{d\sigma^{\uparrow}(\phi) - 
   d\sigma^{\downarrow}(\phi)}{d\sigma^{\uparrow}(\phi) + 
   d\sigma^{\downarrow}(\phi)}
\end{equation}
%
where the arrows indicate the electron beam helicity. 

%(**********Before directly addressing the challenges associated with such an 
%extraction, it is instructive to examine what has already be(BEEN) 
%observed******** SENTENCE NOT CLEAR*************).
%
\subsection{DVCS Beam Spin Asymmetry}
%
%The beam spin asymmetry for DVCS in proportional to.

Through the Bethe-Heitler dominance of the first sine harmonic of the beam spin 
asymmetry
\begin{equation}
   A_{LU}^{\sin\phi} = \frac{1}{\pi} \int_{\pi}^{\pi} d\phi \sin\phi 
   A_{LU}(\phi)
\end{equation}
is proportional to the following combination of Compton form 
factors~\cite{Guidal:2013rya}
\begin{equation}
   A_{LU}^{\sin\phi} \propto \operatorname{Im}( F_1 \mathcal{H}- \frac{t}{4M^2} 
   F_2 \mathcal{E}+ \frac{x_B}{2}(F_1+F_2)\tilde{\mathcal{H}})
\end{equation}
which is dominated by $\operatorname{Im}(\mathcal{H})$ for the proton, and
dominantly sensitive to $\operatorname{Im}(\mathcal{E})$ and 
$\operatorname{Im}(\tilde{\mathcal{H}})$ for the neutron.

Recent measurement~\cite{eg6_note} of incoherent DVCS by the CLAS collaboration 
conducted during the 6 GeV era (E08-024) have indeed shown significant 
modification of the proton beam spin asymmetry in $^4$He without the 
possibility to decipher between the nuclear effects presented above. These 
results are shown in Figure~\ref{fig:eg6Result}. In these measurements the SRC 
and mean field nucleons are not separated and the FSIs remain unchecked.
\begin{figure}
   \centering
   \includegraphics[width=0.5\textwidth,trim=0mm 10mm 0mm 5mm, 
   clip]{figs/incoh_emc_xB.png}
   \caption{\label{fig:eg6Result}The beam spin asymmetry from 
      eg6~\cite{eg6_note} and HERMES along with models from Liuti and Taneja~\cite{Liuti:2005gi}.}
\end{figure}


\section{Tagged DVCS Reactions}

%(***********TO WHAT********* PLEASE REVIEW ALL PARAGRAPH*******))
The ALERT detector combined with CLAS12 provides a unique opportunity to 
measure incoherent exclusive processes on light nuclei.
As mentioned in the previous chapters, tagging low momentum spectator recoils 
in exclusive knockout reactions provides the experimental leverage needed to 
separate and cleanly study a variety of nuclear effects. 
%Furthermore, the over-determined kinematics can be exploited to explore 
%process with unique or rare final states (*******THIS IS A PARACHUTE 
%STATEMENT!!! WHAT IS UNIQUE OR RARE FS???****).

Neutron DVCS (n-DVCS) is of immediate interest as it is needed to do a flavor 
separation of the GPDs. We propose to measure tagged n-DVCS on $^2$H and $^4$He 
targets starting at $P_{A-1} \simeq$~70~MeV/c  for tagged protons and   
$P_{A-1} \simeq$~120~MeV/c for $^3$He ions. The momentum densities for these 
targets can be seen in Figure~\ref{fig:protonDistInDeuteron} and 
Figure~\ref{fig:boundDeuteronPDist}.
%
\begin{figure}
   \centering
   \includegraphics{figs/deuteron_density_Wiringa_2014.pdf}
   \caption{\label{fig:protonDistInDeuteron}The total proton momentum 
      distribution in the deuteron is shown by the red solid line; the 
      contribution from S-wave and D-wave components are shown
   separately by blue and magenta dashed lines.~\cite{Wiringa:2013ala}}
\end{figure}
%
\subsection{n-DVCS with a \texorpdfstring{$^2$H}{Deuteron} Target}
%
Previous measurements of n-DVCS using a deuteron target required subtracting a 
proton contribution from the total deuteron yields~\cite{Mazouz:2007aa} and 
assumed the validity of the PWIA. The yield for the neutron and coherent 
deuteron can not be separated and the subtraction yields the resulting beam 
spin asymmetry of the combination
%
\begin{equation}\label{eq:PWIAnDVCSextract}
   D(\vec{e},e^{\prime}\,\gamma)X - H(\vec{e},e^{\prime}\,\gamma)X =
   d(\vec{e},e^{\prime}\,\gamma)d + n(\vec{e},e^{\prime}\,\gamma)n + ...
\end{equation}
%
which is fit with the CFFs of the neutron and deuteron as free parameters.  
This procedure has a few downsides: it requires a bin by bin equivalent proton 
measurement which is highly prone to systematic effects, the undetected 
spectator system or struck nucleon leaves the center-of-momentum energy, 
$\sqrt{s}$, undetermined, and FSI remain unchecked.   

We propose to measure the recoiling spectator proton, thus, measuring 
$\sqrt{s}$ for every event. Furthermore, the reconstructed missing momentum can 
be used to check for significant final state interactions (see appendix 
\ref{chap:appendixKine}). Comparing $t$ calculated from the virtual and real 
photon momenta to $t$ calculated using the reconstructed missing momentum of 
the neutron (after selection cuts),
\begin{equation}\label{eq:tPhotons}
   t_q = (q_1-q_2)^2
\end{equation}
can provide a measure of the presence of significant final state interactions.  
%
\subsection{n-DVCS and p-DVCS with a \texorpdfstring{$^4$He}{Helium-4} Target}
%
\begin{figure}
   \centering
   \includegraphics{figs/He4_pair_dist_Wiringa_2014.pdf}
   \caption{\label{fig:boundDeuteronPDist}The proton momentum distribution in 
      $^4$He is shown by the red circles; the tp cluster distribution is shown 
      by the blue triangles and the dd cluster distribution is shown by the 
   magenta squares.~\cite{Wiringa:2013ala}}
\end{figure}
%
A helium target provides the unique opportunity to again measure the neutron 
DVCS beam spin asymmetry, however, now on a bound nucleon with unprecedented 
control over final state interactions. Through the two reactions 
$^4$He$(e,e^{\prime}\gamma\,p\,^3\text{H})$ and 
$^4$He$(e,e^{\prime}\gamma\,^3\text{He})n$ the ratios
\begin{align}\label{eq:BSAratios}
   R_n &= \frac{A_{LU}^{n^{*}}}{A_{LU}^{n}}\\
   R_p &= \frac{A_{LU}^{p^{*}}}{A_{LU}^{p}}
\end{align}
can  provide the leverage needed to definitively make a statement on medium 
modifications.

The proton BSA will be measured by fully detecting the final state; the struck 
proton will be detected in CLAS12 and the recoiling spectator $^3$H will be 
detected in ALERT. The neutron BSA will be measured by tagging a recoil $^3$He 
and without detecting the struck neutron. Exclusivity cuts will ensure the 
n-DVCS event is cleanly selected.
The free proton BSA measurement in Equation\,\ref{eq:BSAratios} will be taken 
from the already approved JLab measurements \cite{E1206119, E1206114}, while 
the neutron BSA will come from the deuteron target measurement discussed above.  
The neutron measurement will have the extra advantage of experimental 
systematics canceling in the ratio because both asymmetries will be measured 
using the same apparatus.
%(***** HERE YOU HAVE TO SPECIFY HOW YOU ARE GOING TO GET EACH OF THE BSA FOR 
%PROTON AND NEUTRON, AND MENTION THAT THE FREE PROTON BSA WILL BE OBTAINED 
%THROUGH APPROVED DVCS EXPERIMENTS IN HALLB AND ADD THE REFERENCE********). 
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure6.pdf}
   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure7.pdf}
   \caption{\label{fig:incoherentHe4FSI}Incoherent DVCS on a nuclear target 
   without (left) and with (right) final state interactions.}
\end{figure}

Finally, we consider the fully exclusive proton DVCS reaction where a recoil 
triton is detected as $\bm{P}_{A-1}=-\bm{p}_1$. The fully detected final state 
kinematics present an opportunity to test the PWIA\footnote{Please see appendix 
\ref{chap:appendixKine} for a more detailed explanation.}. One way is to use 
the two momentum transfers, $t_q$ (Equation~\ref{eq:tPhotons}) and
\begin{align}\label{eq:tHadrons}
   t_p &= (p_1-p_2)^2,
\end{align}
which must be the same, i.e., $\delta{t}=t_q-t_p=0$. If a FSI occurs between 
the spectator and the struck nucleon (Figure\,\ref{fig:deuteronFSIdiagram}), such 
as pion exchange, $\delta{t}$ can be non-zero depending which over-determined 
kinematic variables we choose to use (or not use). The reader is referred to 
appendix \ref{chap:appendixKine} for a thorough discussion of this point.  By 
selecting events where $\delta{t}\simeq0$, within the detector resolutions, we 
can be sure that significant final state interactions have not occurred. These 
are events that may contain FSIs that are kinematically indistinguishable from 
the PWIA, but which have an amplitude level influence on the cross section.  
Alternatively, requiring the missing momentum to be back-to-back with the 
recoil spectator provides a cut which is expected to reduce final state 
interactions.

With the final state interactions well under control in the proton DVCS 
channel, charge symmetry suggests that, for the same kinematics, they will be 
similarly understood in the neutron channel. That is the FSIs are assumed to 
follow charge symmetry. Therefore, the proton DVCS BSA measurement on $^4$He is 
crucial for measuring in a model independent way the validity of the PWIA and 
mapping the FSIs for the mirror neutron measurement.

