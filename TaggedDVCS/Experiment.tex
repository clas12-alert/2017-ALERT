\setlength\parskip{\baselineskip}%
\chapter{Proposed Measurements}
\label{chap:reach}

We propose to measure the beam spin asymmetry for three DVCS channels using two 
different targets and with tagged spectator systems. The three principal 
reactions are:
\begin{itemize}
   \item $^4$He$(\vec{e},e^{\prime}\,\gamma\,^3\text{H}p)$ -- bound p-DVCS
   \item $^4$H$(\vec{e},e^{\prime}\,\gamma\,^3\text{He})n$ -- bound n-DVCS
   \item $^2$H$(\vec{e},e^{\prime}\,\gamma\,p)n$ -- quasi-free n-DVCS
\end{itemize}
where in the first process the final state is fully detected. Before discussing 
the details of the measurements, we present an overview of the procedure  for 
extracting the $\sin\phi$ harmonic of the BSAs and identify the primary 
deliverables of the experiment.
%%%%%%%%%%%%%%%%%%%%%%
\section{Asymmetry Extraction Procedure}
%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
   \centering
   \includegraphics[width=1.0\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure12.pdf}
   \caption{\label{fig:BSAExtraction}A general overview of the BSA extraction 
      procedure for a kinematic bin. For each ($x$, $Q^2$, $t$) bin the BSA is 
      extracted through fitting the asymmetry as function of $\phi$ (see 
      Equation~\ref{eq:ALUfit}) for the various spectator momentum configurations 
      and the $\sin\phi$ harmonic, $\alpha$ is extracted. Note these plots are only 
   showing statistical uncertainties and the values are offset for clarity.}
\end{figure}
%
Figure~\ref{fig:BSAExtraction} shows how, starting with just one kinematic bin in 
$t$, $Q^2$, and $x$ (which is not explicitly shown), the BSA is extracted for 
three regions of spectator recoil angles relative to the virtual photon direction and three ranges of spectator momenta.  
As indicated, the spectator angles correspond to a forward tagged system, a 
system with perpendicular momenta, and a backward tagged spectator.  In the 
latter angular region FSIs are expected to minimal. Furthermore, three ranges 
of momenta are identified, the lowest corresponding to nucleons moving in the 
mean field  and the highest belonging to nucleons in short range correlated 
pairs.
Fitting the BSA asymmetries yields the $\sin\phi$ harmonic which is shown on 
the right of Figure~\ref{fig:BSAExtraction} for the different spectator kinematic 
regions.

The first process above, p-DVCS on $^4$He, provides a model independent way of 
identifying kinematics where final state interactions are minimized (see 
Introduction and Chapter~\ref{chap:physics}). Armed with this information we 
will then measure the n-DVCS beam spin asymmetries on $^4$He and $^2$H knowing 
which kinematics are, or are not, influenced by FSIs.
We will then proceed as shown in Figure~\ref{fig:BSARatios} where the two n-DVCS 
measurements are combined into a ratio of bound neutron to quasi-free neutron.  
These BSA measurements and ratios are the primary deliverables of this 
proposal. They will be measured over a broad range of DVCS kinematics 
accessible to CLAS12 and for the spectator momenta regions noted above using 
the ALERT detector.

%A deviation from unity in the mean field FSI-free region of spectator 
%kinematics  would indicate the quark distributions are modified. 
\begin{figure}
   \centering
   \includegraphics[width=1.0\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure11.pdf}
   \caption{\label{fig:BSARatios}The BSA ratios targeting different nuclear 
      effects with specific spectator kinematics. Note  only statistical 
      uncertainties are shown and the BSA harmonics/ratios are (arbitrarily) 
      offset for clarity.
   }
\end{figure}
%%%%%%%%%%%%%%%%
\section{Kinematic Coverage}
%%%%%%%%%%%%%%%%
The kinematic coverage was studied using a newly developed CLAS12 fast 
Monte-Carlo, \texttt{c12sim}, where the CLAS12 detector resolutions were 
replicated based on the Fortran CLAS12 Fast-MC code. Because  \texttt{c12sim} 
is a Geant4 based simulation, the particle transport through the magnetic fields 
was handled by the Geant4 geometry navigation where all other processes were 
turned off. The resolutions for ALERT were obtained through full Geant4 
simulations with all physics processes turned on.
\begin{figure}[htb]
   \centering
   \includegraphics[width=0.49\textwidth,trim=10mm 4mm 10mm 12mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_0.pdf}
   \includegraphics[width=0.49\textwidth,trim=10mm 4mm 10mm 12mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_1.pdf}
   \caption{\label{fig:mcPdist}The simulated and detected momentum (left) and 
   angular (right) distributions showing overall detector coverage for the 
 experiment.}
\end{figure}
%
\begin{figure}[htb]
   \centering
   \includegraphics[width=0.49\textwidth,trim=10mm 4mm 10mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_5.pdf}
   \includegraphics[width=0.49\textwidth,trim=10mm 4mm 10mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_deltat_2_11.pdf}
   \caption{\label{fig:tResolutions}Left: simulated and reconstructed $t$ 
   calculated from the photons ($t_q$) and hadrons ($t_p$). Right: The 
difference between the two momentum transfers, $\delta t= t_p-t_q$.}
\end{figure}

First, we consider the p-DVCS reaction on $^4$He because of its special ability to determine 
the presence of final state interactions through a fully detected final state.
The spectator system, a recoiling $^3$H in the present case, is detected in ALERT 
while the forward electron, photon, and proton are detected in CLAS12. The 
resulting kinematics for the n-DVCS on $^2$H and $^4$He reactions will be quite 
similar, where the key difference is the struck neutron goes undetected. These 
events are then selected via the neutron missing mass cuts.

The overall coverage in momentum and scattering angle can be seen in 
Figure~\ref{fig:mcPdist} and angular detector coverage of all the particles can 
be seen in Figure~\ref{fig:eThetaVsPhi}. The bin variables $x$, $Q^2$, and $t$ 
are shown in Figure~\ref{fig:Q2Vsxandt}. See \ref{sec:extraKineProjections} for 
more details on the kinematic coverage.
%, and the spectator variables, $\theta_s$ and $P_{A-1}$,
%are shown in Figure~\ref{fig:thetasCoverage}.
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_8.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_81.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_82.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_83.pdf}
   \caption{\label{fig:eThetaVsPhi}The angular coverage shown as $\theta$ Vs.  
   $\phi$ for the electron (upper left), proton (upper right), photon (lower 
 left), and recoil spectator (lower right).}
\end{figure}
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 12mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_70.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 2mm 12mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_72.pdf}
   \caption{\label{fig:Q2Vsxandt}$Q^2$ plotted against $x$ (left) and $t$ 
   (right).}
\end{figure}

As mentioned throughout this proposal and with detail in appendix 
\ref{chap:appendixKine}, the momentum transfer can be reconstructed via using 
the photons, or using the nucleon side of the diagram where we make use use of 
the detected spectator system and the PWIA. Figure\,\ref{fig:tResolutions} shows 
that the resolutions are comparable, thus, allowing for the systematic check of 
FSIs which were not included in the generated events. The spectator angle and 
momentum can be seen in Figure\,\ref{fig:thetasCoverage}, where these results can 
be used along with calculations such as those shown in 
Figure\,\ref{fig:deuteronFSI} to isolate kinematic regions with significant 
FSIs.
%
%A selection cut of $Q^2>1$~GeV$^2$ and $W>2$~GeV is used on the reconstructed 
%variables. Figures \ref{fig:eThetaVsPhi} through \ref{fig:recoilThetaVsPhi} 
%show the reconstructed angles before and after applying this cut. 
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 10mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_28.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 10mm 10mm,
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_75.pdf}
   %\includegraphics[width=0.49\textwidth,trim=0mm 4mm 10mm 10mm,
   %clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_2_280.pdf}
   \caption{\label{fig:thetasCoverage}Left: Spectator recoil angle, $\theta_s$,
     showing the generated and reconstructed values, also shown is the 
     reconstructed with $Q^2>1$ GeV$^2$ and $W>2$ GeV.  Right: Spectator angle vs 
   reconstructed spectator momentum.}
\end{figure}


% -------------------------------------------------
\section{Projections}
%%%%%%%%%%
\subsection{Beam Spin Asymmetry Extraction}
%%%%%%%%%%%%%%%%%%%%%%%%
The measured beam spin asymmetries are binned in 6 variables: $x_B$, $Q^2$, 
$t$, $\phi$, $P_s$, and $\theta_s$. The 6 dimensional data will be reduced to 5 
dimensions by fitting the BSA as a function of $\phi$ to extract harmonic 
content. 
%The projections shown in Figure\,\ref{fig:phiAsymsBinned} integrate over all 
%spectator momentum.  If we also bin in spectator momentum, $P_{1}$, we can 
%begin determine the sensitivity to FSIs. 
Projections for the statistical uncertainties of these asymmetries and their fits 
are shown for a few bins in Figure~\ref{fig:He4ProtonPhiAsyms1} and Figure~\ref{fig:He4ProtonPhiAsyms2}
for p-DVCS on $^4$He. A few of the $\phi$ binned asymmetries for n-DVCS on $^4$He are shown in Figure~\ref{fig:He4NeutronPhiAsyms}
and similarly in Figure~\ref{fig:H2NeutronPhiAsyms} for n-DVCS on $^2$H.
Note that we are using a simple binning scheme shown in Table\,\ref{tab:bins}.
These bins are likely to change as the cross sections are not well known, 
especially when isolating high momentum spectators.
\begin{table}
   \centering
\vspace*{0.2cm}
   \begin{tabu}{lccccc}
\tabucline[2pt]{-}                                                   
         %Bin   &      &      &      &      &     & \\
         %\hline
         %$x$        &              & 0.05 & 0.25 & 0.35 & 0.5 & 0.8\\
         %$Q^2$      & \si{\GeV^2}  & 1    & 1.5  & 2.0  & 3.0 & 10\\
         %$t$        & \si{\GeV^2}  & 0    & 0.75 & 1.5  & 2.5 & 6.0\\
         %$\theta_s$ & \si{\degree} & 0.0  & 50   & 100  & 180 & \\
         %$P_s$      & \si{\GeV/c}  & 0.0  & 0.2  & 0.35 & 0.5 & \\
         %\hline
         %\hline
         Bin  & $x$         & $Q^2$       & $t$          & $\theta_s$   & $P_s$ \\ 
        units&             & \si{\GeV^2} & \si{\GeV^2}  & \si{\degree} & \si{\GeV/c} \\
\tabucline[1pt]{-}                                                   
             &0.05         & 1           & 0           & 0.0          & 0.0         \\
             &0.25         & 1.5         & 0.75        & 50           & 0.2         \\
             &0.35         & 2.0         & 1.5         & 100          & 0.35        \\
             &0.5          & 3.0         & 2.5         & 180          & 0.5         \\
             &0.8          & 10          & 6.0 & & \\
\tabucline[2pt]{-}                                                   
%Number of $\phi$ bins &  & 12  &   &  &  & \\
         %\hline
\end{tabu}
   \caption{\label{tab:bins}The simple binning scheme used for the proposal.
   Listed here are the bin edges forming each bin.}
\end{table}

\begin{figure}
   \centering
   \includegraphics[width=0.79\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure1.pdf}
   \caption{\label{fig:He4ProtonPhiAsyms1}Projections for the statistical 
     uncertainties on $A_{LU}$ for three different bins in spectator angle, all 
     corresponding to the lowest spectator momentum bin. The spectator angles 
     are forward (left), perpendicular (center), and backward (right). Note the 
   low momentum bin corresponds to the mean field nucleons. }
\end{figure}

\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure2.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure3.pdf}
   \caption{\label{fig:He4ProtonPhiAsyms2}Expected statistical uncertainties of 
      $A_{LU}$ for $\theta_{s}$ bins identical to those  in 
      Figure\,\ref{fig:He4ProtonPhiAsyms1}, but these results show the two higher 
   spectator momentum bins. Note the highest momenta (blue) correspond to SRC 
nucleons.  }
\end{figure}

\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure4.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure5.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure6.pdf}
   \caption{\label{fig:He4NeutronPhiAsyms}Projected statistical uncertainties  
     $A_{LU}$ for $\theta_{s}$ bins identical to those  in 
   Figure\,\ref{fig:He4ProtonPhiAsyms1}, for n-DVCS on $^4$He measurement, in 9 
 different bins of spectator momentum and angle.}
\end{figure}

\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure7.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure8.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure9.pdf}
   \caption{\label{fig:H2NeutronPhiAsyms}Projected statistical uncertainties  
     $A_{LU}$ for $\theta_{s}$ bins identical to those  in 
   Figure\,\ref{fig:He4ProtonPhiAsyms1}, for n-DVCS on $^2$H measurement, in 9 
 different bins of spectator momentum and angle. }
\end{figure}

The beam spin asymmetries are the primary observables for this experiment and 
will be fit with the following simplified parameterization
\begin{equation}\label{eq:ALUfit}
   A_{LU}(\phi) = \frac{\alpha \sin\phi}{1+\beta\cos\phi}
\end{equation}
where the free parameters $\alpha$ and $\beta$ are related to CFFs and Fourier 
harmonics. As emphasized in section~\ref{sec:polEMCeffect}, the $\sin\phi$ 
harmonic, $\alpha$, is quite sensitive to nuclear effects. 
Therefore, we will extract $\alpha$ for every bin by fitting the 
asymmetry binned in $\phi$ for each kinematic setting. Out of the many 
kinematic settings, Figure~\ref{fig:alphaALU} (left) shows the result of fitting the 
$\phi$ asymmetry for one bin in $x$, $Q^2$, and $t$.
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 0mm 12mm, 
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_asyms_t_2_1_0_2.pdf}
   \includegraphics[width=0.49\textwidth,trim=0mm 4mm 0mm 12mm, 
   clip]{fastmc2/tagged_dvcs_He4_proton_2/He4_proton_asyms_alpha_ratio_2_1_0_2.pdf}
   \caption{\label{fig:alphaALU}Left: Projected uncertainties after fitting the 
     beam spin asymmetry with equation\,(\ref{eq:ALUfit}) to extract a 
   value of $\alpha$. Each bin in $x$, $Q^2$, and $t$ has 3 bins in $P_{A-1}$ and $\theta_s$, which 
 are offset vertically for clarity. Right: The ratio of $\alpha$s for a bound 
 neutron  and a quasi-free nucleon. }
\end{figure}
%\begin{figure}
%   \centering
%   %\includegraphics[width=0.49\textwidth,trim=0mm 4mm 0mm 0mm,
%   %clip]{fastmc/tagged_dvcs_He4_proton_3_0_20500_asyms_alpha_ratio.pdf}
%   %\includegraphics[width=0.49\textwidth,trim=0mm 4mm 0mm 0mm,
%   %clip]{fastmc/tagged_dvcs_He4_proton_3_3_20500_asyms_alpha_ratio.pdf}
%   \caption{\label{fig:alphaALURatio}Similar results as Figure\, 
%   \ref{fig:alphaALU} for the ratio, $R_{\alpha}$, in 
%Equation\,\ref{eq:Ralpha}.}
%\end{figure}

For the n-DVCS measurements, the missing mass cut will select DVCS events. The 
primary assumption is what we will have already observed through the p-DVCS channel
and isolated kinematics where FSIs are minimized. Typically, this corresponds 
to backward low momentum spectators. We will match the kinematics where the FSI 
are observed to be negligible for the proton and look for nuclear effects in 
neutron.  We define the following ratio for the extracted $\alpha$ values from 
DVCS on a quasi-free neutron in $^2$H and from DVCS on a bound neutron in 
$^4$He:
\begin{equation}\label{eq:Ralpha}
   R_{\alpha}^{N} = \frac{\alpha^{(^4\text{He})}_{N^{*}}}{\alpha^{(^2\text{H})}_N}
\end{equation}
where the $N^{*}$ indicates the bound nucleon. We will identify nuclear effects 
by observing  deviations from unity in this ratio and extracting its trend as a 
function of $x$, and for various spectator kinematics limits where we expect mean 
field nucleons or SRC nucleons to dominate.
The projected statistical uncertainty is shown in Figure~\ref{fig:alphaALU} 
(right) and in Fig~\ref{fig:RalphaAllQ2}. See appendix 
\ref{sec:extraRatioProjections} for more BSA ratio projections.

\begin{figure}
   \centering
   \includegraphics[width=0.7\textwidth,trim=0mm 0mm 0mm 0mm, 
   clip]{../tikz/extra_diagrams/extra_diagrams-figure14.pdf}
   \caption{\label{fig:RalphaAllQ2}A subset of the $R_{\alpha}$ ratios  for all $Q^2$ with backward 
      tagged spectators. The highest spectator momenta  bins are offset vertically  above the 
 lowest spectator momenta and the colors indicate the different $t$ bins which are shifted horizontally for clarity.}
\end{figure}

\subsection{Systematic Uncertainties}

We estimate the main sources of systematic uncertainties from those ultimately obtained 
for the CLAS-eg6 experiment's incoherent DVCS measurement~\cite{eg6_note}. 
They are listed in Table~\ref{tab:systematics} along with our estimates for the beam spin
asymmetry systematics. For the BSA the beam polarization will 
dominate our systematic uncertainties followed by the DVCS event selection cuts. 
With the significant improvement of ALERT for detecting the spectator recoils this 
uncertainty is expected to improve by more than a factor of two.

The so-called ``acceptance ratio'' corrects for the $\pi^0$ background 
and is defined for each bin as
\begin{equation}\label{eq:acceptanceRatio}
   R_{\pi^0} = \frac{N_{\pi(\gamma)}}{N_{\pi(\gamma\gamma)}}
\end{equation}
where $N_{\pi(\gamma)}$ and $N_{\pi(\gamma\gamma)}$ are the rates for exclusive 
electro-production of $\pi^0$s where one decay photon is detected and 
where both decay photons are detected, respectively.
The ratio calculated in Equation.~\ref{eq:Ralpha} has the benefit that the acceptance cancels 
in the ratio  under the approximation $R_{\pi^0}(^4\text{He}) \simeq R_{\pi^0}(^2\text{H})$.

External radiative effects on the electron side can be easily understood and studied
using the over-determined kinematics. The exclusivity of the process allows tight cuts 
that remove any initial state radiation. Furthermore, much of the radiative effects will
cancel in the ratio.

\begin{table}
   \centering
   \begin{tabu}{lccc}
\tabucline[2pt]{-}                                                   
      \bf Source & \bf \quad CLAS-eg6 \quad&\quad \bf CLAS12-ALERT \quad & \bf Systematic Type\\
\tabucline[1pt]{-}                                                   
      Beam polarization    & 3.5\%    &    3.5\% & normalization \\
      DVCS event selection & 3.7\%    &    1.0\% & bin-to-bin\\
      Acceptance ratio     & 2.0\%    & $<$1.0\% & bin-to-bin\\
      Radiative Corrections& 2.0\%    & $<$1.0\% & bin-to-bin\\
      Others               & 0.1\%    &    0.1\% & \\
\tabucline[1pt]{-}                                                   
      \textbf{Total}       & 5.5\%    &     4.0\% \\
\tabucline[2pt]{-}                                                   
   \end{tabu}
   \caption{\label{tab:systematics}Estimates of the expected systematic 
   uncertainties compared to CLAS-eg6.}
\end{table}








