\setlength\parskip{\baselineskip}%
\chapter*{Introduction\markboth{\bf Introduction}{Introduction}}
\label{chap:intro}
\addcontentsline{toc}{chapter}{Introduction}

Deeply virtual Compton scattering is widely used to extract information about 
the generalized parton distributions of the nucleon. Its usefulness 
comes from the fact that the final state photon does not interact strongly (at 
leading order), requiring no additional non-perturbative formation mechanism.  
That is, the process in which the active quark radiates a final state photon is 
well understood, therefore, it is very useful for extracting information about 
the unknown non-perturbative vertex shown in Figure~\ref{fig:Handbagintro}.

The extracted GPDs offer a three dimensional picture of how quarks and gluons 
are distributed in the nucleon. DVCS measurements on the 
proton~\cite{Chekanov:2003ya,Aktas:2005ty,Camacho:2006qlk,Girod:2007aa} and 
neutron~\cite{Mazouz:2007aa} have already begun to provide insight into this 
slowly developing picture of the nucleon, however, without a free neutron 
target a flavor separation will always require using a quasi-free neutron 
target bound in light nuclei such as deuterium or $^3$He. Such an extraction 
requires control of numerous nuclear effects: Fermi motion, off-shellness of 
the nucleons, mean field modified nucleons, short-range correlations (SRC), and 
final-state interactions. 

Most observables involving nuclear targets ({\it e.g.}, inclusive deep-inelastic 
scattering (DIS), tagged DIS, inclusive quasi-elastic, semi-inclusive nucleon knockout, and polarization 
transfer in quasi-elastic scattering) are sensitive to many of these nuclear 
effects.  Some experiments have been conducted in such a way as to mitigate or 
provide some systematic control over the size of these 
effects~\cite{Rvachev:2004yr,Paolone:2010qc,Malace:2010ft}.  However, as 
discussed in the next chapter, the very nature of each experiment often 
precludes control of one or more nuclear effect mentioned above.  Therefore, it 
is difficult to unambiguously draw conclusions from these measurements as to 
whether a nucleon is modified in a nuclear environment.

Much like the DVCS observables' ability to cleanly access information about the 
GPDs, tagged incoherent DVCS analogously provides a method for cleanly 
extracting nuclear effects from the observables. In a fully exclusive reaction, 
the over-determined kinematics yield two measurements of the same momentum 
transfer (see Figure~\ref{fig:Handbagintro}). Within the plane wave Born 
approximation (PWBA), the momentum transfer between the virtual and real photon 
is completely insensitive to FSIs. On the other side of the diagram, the 
momentum transfer calculated between the initial and final nucleon is quite 
sensitive to FSIs under the assumption that the plane wave impulse approximation 
(PWIA) holds\footnote{See appendix~\ref{chap:appendixKine} for a detailed 
discussion of kinematics and the PWIA.}. That is to say, any relative deviation 
between the two momentum transfers can be attributed to the breakdown of the 
PWIA. In this way we can identify the kinematics where FSIs are significant and 
where they are minimal.  Unlike, fully exclusive quasi elastic knockout 
reactions, tagged DVCS has a unique opportunity to simultaneously probe nuclear 
effects at the parton level.

\begin{figure}[!hb]
   \centering
   \includegraphics[width=0.40\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure22.pdf}
   \caption{\label{fig:Handbagintro}The DVCS handbag diagram showing the two 
   ways the momentum transfer can be calculated. The hatched vertex represents   
   the non-perturbative GPD.
 }
\end{figure}


% maybe this should be emphasized?
Therefore, we propose to measure tagged DVCS beam spin asymmetries on two 
nuclear targets ($^2$H and $^4$He) to unambiguously determine if the nucleon is 
modified in a nuclear environment. We will measure three beam spin asymmetries 
through tagged incoherent DVCS using two gas targets. The experiment requires 
the measurement of the three main processes:
\begin{enumerate}
   \item $^4$He$\,+\,\gamma^{*} \longrightarrow \gamma\,+\,p\,+\,^3$H
   \item $^4$He$\,+\,\gamma^{*} \longrightarrow \gamma\,+\,(n)\,+\,^3$He
   \item $^2$H$\,+\,\gamma^{*}  \longrightarrow \gamma\,+\,(n)\,+\,p$
\end{enumerate}
The first process provides a measurement of bound proton DVCS, but more 
importantly, by also detecting the final state proton, provides the 
over-determined kinematics needed to systematically probe the size of FSIs. This 
measurement is of critical importance to unwinding the nuclear effects when 
analyzing the last two processes, bound and quasi-free neutron DVCS, where the 
active nucleon (a neutron) goes undetected. A self-contained analysis of the 
nuclear effects on a neutron will be compared to a similar analysis of the 
proton. In addition to the proposed measured bound proton results, the latter 
will make use of previously measured results and already approved free proton 
DVCS~\cite{E1206119,E1206114} experiments.
In this way we will extract for both the proton and neutron an ``off-forward 
EMC effect'', {\it i.e.}, the ratio of a bound nucleon's off-forward structure 
function in $^4$He to a free nucleon's off-forward structure function.

A large acceptance detector system capable of running at high luminosity along 
with the ability to detect or ``tag'' the low energy recoil spectator system is 
needed to perform such measurements.
The ideal choice for this experiment is clearly CLAS12 augmented with \emph{a 
low energy recoil tracker} (ALERT) that cleanly identifies the recoiling 
spectator system down to the lowest possible momentum.  The ALERT detector 
consists of a small drift chamber that is insensitive to minimum ionizing 
particles, and a surrounding scintillator hodoscope that principally provides 
TOF information.

The outline of this proposal is as follows.  Chapter one will provide a 
detailed motivation for the experiment.  Chapter two will present the formalism 
and the observables we aim to measure.  Chapter three presents  a discussion of 
detector requirements, specifically, the detectors of CLAS12 and the need for a 
new low energy recoil recoil detector.  Chapter four will discuss the 
experimental outputs, kinematic coverage, projected results, and required 
beam time for the proposed experiment.
The first time reader is encouraged to first read the 
Appendix~\ref{chap:appendixKine} which covers in detail the kinematics, PWIA, 
and FSIs. 


%The initial momentum of the struck nucleon is inferred from the spectator 
%while the struck system can go undetected. In the case of DVCS on the neutron 
%with a nuclear target, tagging the recoil system, as shown in 
%Figure~\ref{fig:taggedDVCS}, is crucial for quantifying effects due to FSI, 
%off-shell nucleons, and short range correlations. By pushing the detection of 
%the spectator system to the lowest momentum possible, we will be able to tune 
%the kinematics to the regime where each effect dominates. Thereby allowing a 
%systematic evaluation as a function of spectator momentum the onset of each of 
%these effects.
