\setlength\parskip{\baselineskip}%
\chapter{Physics Motivations}
\label{chap:physics}

Before diving into the details of tagged incoherent DVCS, we will first explain 
why an understanding of the nature and origins of nuclear effects is important 
for determining the nucleon structure at the parton level.  We will begin this 
chapter with a discussion of nuclear effects and the  challenges they present 
to experiment. This is followed by a quick overview of GPDs and their 
importance to understand the partonic structure of nuclear matter.  The 
kinematics of incoherent DVCS are discussed, highlighting the critical 
importance of the spectator tagging method. We will emphasize the unique 
opportunity tagged DVCS has to finally settle the more than three decades old 
question: \emph{is the partonic structure of the nucleon modified in presence 
of a nuclear medium?}
%
\section{Nuclear Effects} \label{sec:NuclearEffects}
%
\subsection{The EMC Legacy}\label{sec:EMCLegacy}
%
Measurements of the longitudinal parton distribution functions (PDFs) with 
polarized beams and targets have provided a detailed one dimensional mapping of 
the quark distributions in the nucleon. QCD has been successful in describing 
the evolution of these distributions across scales differing by many orders of 
magnitude. The European Muon Collaboration not only observed the 
so-called ``EMC effect'', but they also created the poorly named ``spin 
crisis''. The EMC effect originates from their observation that the naive 
expectation of the quark distributions in nuclei, {\it i.e.}, they are the sum 
of the quark distributions for $A$ free nucleons, was not 
observed~\cite{Aubert:1983xm}.  However, consensus has yet to be reached in how 
to explain this effect. The spin crisis began when it was discovered that the 
spin of the quarks only carry a small fraction of the nucleon's total spin.  It 
was soon understood that, through the non-perturbative dynamics of QCD, the 
remaining angular momentum will likely come in the form of quark orbital 
angular momentum (OAM) and gluon angular momentum.  The EMC experiments gave us 
a ``crisis'' that was quickly understood, and an empirical ``effect'' whose 
origins remain ambiguous more than 3 decades later.\footnote{In hindsight, 
   perhaps the ``EMC effect'' should have been called the ``EMC Crisis'', and 
the ``spin crisis'' called the ``EMC spin effect''.}
%
\subsection{Measuring Medium Modified Nucleons}\label{sec:MediumModified}
%
The EMC effect  showed the possibility of medium modifications to the nucleon 
may be significant in deep inelastic scattering\footnote{Perhaps the earliest 
known medium modification of the nucleon is the free neutron lifetime compared 
to the significantly longer lifetime when bound in a nucleus.}.  However, the 
degree to which these modifications can be cleanly studied in inclusive or 
semi-inclusive processes is made difficult by the possible presence of final 
state interactions.  Furthermore, when considering the Fermi motion of a 
bound nucleon, there is a probability of finding a nucleon moving with 
large relative momenta which corresponds to a configuration where the two 
nucleons are separated by a small distance. By selecting these dense 
configurations through spectator tagging in hard processes, it is not 
unreasonable to expect sizable modifications relative to the mean field 
nucleons~\cite{Weinstein:2010rt}.
%(*****ADD REFERENCES TO THE CORRELATION BETWEEN EMC AND SRC ,Weinstein et 
%al.***********)
Therefore, knowing precisely the initial momentum of the struck nucleon is key 
for understanding the short and long range nuclear effects.  Isolating the 
configuration space effects from the FSIs presents a significant obstacle to 
drawing a definitive conclusion about medium modifications.

Similar medium modifications are expected to manifest themselves in the elastic 
form factors of bound nucleons. Observation of saturation of the Coulomb sum 
rule (CSR), which is accessible through measurements of the longitudinal 
response function in quasi-elastic scattering off nuclei, has been debated for 
some time~\cite{Meziani:1984is,Morgenstern:2001jt}. An observed quenching of 
the sum rule would indicate that nucleons are modified in such a way that the 
net charge response of the parent nucleus is much more complicated than a 
simple sum of nucleons.  Recent, QCD inspired theoretical work predicts a 
dramatic quenching of the sum rule~\cite{Cloet:2015tha}. Furthermore, 
observations of short range correlated nucleon pairs in knockout reactions have 
challenged us to confront our ignorance of the short-range part of the N-N 
potential.  

While a consensus has yet to be reached in explaining the origins of and the 
connections between the EMC effect, short range correlations, and quenching of 
the Coulomb sum rule, medium modifications of the nucleon is expected to play 
an important role in these phenomena.
%
\subsection{Why \emph{Tagged} DVCS?}
%
DVCS is poised to provide some much needed contact between the EMC effect and 
short-range correlations. The two phenomena are observed through notably  
different processes but the connection between the inelastic and elastic 
observables is due to the properties of GPDs (see~\ref{sec:GPDs}). In the 
forward limit the GPDs reduce to the longitudinal parton distributions whose 
modification may explain the EMC effect, and in the off-forward case they 
reduce to the form factors, thus, describing elastic scattering off of the 
nucleon.  Therefore, measurements of nucleon GPDs in nuclei will bridge the gap 
between these two processes and will shed light on the connections between the 
EMC effect and short range correlations.  The sensitivity of the GPDs to medium 
modifications is a significant motivation, however, \emph{tagged} incoherent 
DVCS provides an unprecedented handle on quantifying and systematically 
controlling the various nuclear effects.

First, let us consider the inclusive DIS measurements where only the scattered 
electron is detected and the exchanged virtual photon interacts at the parton 
level. The nucleon containing the struck quark may potentially be in a 
short-range correlated N-N pair, therefore, tagging the spectator system in the 
PWIA provides the experimental handle needed to compare the contributions to 
the EMC effect from SRC nucleons versus the nucleons in the mean field. This 
measurement is part of the ``Tagged EMC'' proposal found in the current 
proposal's run group. Here, FSIs are the principle challenge for this method 
which become amplified at larger initial nucleon momentum. The re-interaction 
of the spectator system (A-1) with hadronizing fragments (X) can alter the 
detected momentum of the spectator system.  Therefore model calculations have 
to be used to explore kinematics where FSIs can 
minimized~\cite{CiofidegliAtti:2002as}.

Similarly, for inclusive quasi-elastic scattering we would like to measure the 
nucleon elastic form factor modifications associated with the SRC and the 
mean-field nucleons. Therefore, by detecting the knockout nucleon or a 
spectator recoil, the initial nucleon's momentum can be determined (within the 
PWIA).  If both are detected, the over-determined kinematics allow for a second 
calculation of the momentum transfer.  However, for large nuclei the 
possibility of detecting the full (A-1) recoil system  becomes nearly 
impossible. Furthermore, FSIs in the form of meson exchange currents (MEC) can 
become rather troublesome even for measurements of induced polarization in 
quasi-elastic knock-out reactions. Therefore, again, we find an explicit model 
dependence spoiling the interpretation of medium modifications.  

Finally, incoherent DVCS has a unique combination characteristics found in DIS 
and quasi-elastic scattering that make it the ideal process for exploring 
nuclear effects. Like both processes, tagging the recoil spectator system  
serves to identify and separate the mean field (low momentum) nucleons from the 
SRC (high momentum) nucleons. Similar to DIS, DVCS has a parton level 
interpretation, and like elastic scattering the process is exclusive. The 
latter property allows for systematic control of the FSIs through the redundant 
measurement of the momentum transfer, $t$. Therefore, tagged incoherent DVCS 
provides a \emph{model independent} method for studying and accounting for 
final state interactions while providing an observable that is uniquely 
sensitive to the medium modifications. 

%Measuring the recoil and full final state allow for an excellent handle on 
%measuring the strength of FSI and provide a

%On the other hand, the off-forward features found in elastic scattering, 
%namely the exclusivity,   allow for a to be observed while simultaneously 
%measuring  the degree to which FSI are contributing to the observed .
%
\section{Generalized Parton Distributions}
%
\subsection{Spin Sum Rule}
%
By studying the ``off-forward parton distributions'', Ji derived a sum 
rule~\cite{Ji:1996ek} which is a gauge invariant decomposition of the nucleon 
spin. It relates integrals of the GPDs to the quark total angular momentum and 
is written as
\begin{equation}\label{eq:JisSumRule}
   J_q = \frac{1}{2}\int dx\,x\left[ H_q(x,\xi,t=0;Q^2) + 
   E_q(x,\xi,t=0;Q^2)\right]
\end{equation}
where the $H_q$ and $E_q$ are the leading-twist chiral-even quark GPDs. An 
identical  expression for the total gluon orbital angular momentum is obtained 
using the two gluon GPDs, $H_g$ and $E_g$. The total nucleon spin is simply 
written as
\begin{equation}
   \frac{1}{2} = \sum_q J_q + J_g ,
\end{equation}
where the sum is over light quarks and anti-quarks. A topic of heavy discussion 
over the past five years or so has been about the decomposition of the nucleon 
spin. Ji showed that the gluon angular momentum cannot be broken into spin and 
orbital in a gauge invariant way, however, the quark can. In fact, the 
polarized PDFs provide the quark spin contribution in the forward limit
\begin{equation}\label{eq:quarkspin}
   \Delta \Sigma_q (Q^2) = \int dx\, x\,\tilde{H}_q(x,\xi=0,t=0;Q^2)
\end{equation}
where $\Delta\Sigma_q$ is the integral of the polarized PDF $\Delta q$. Therefore, we arrive at an expression for the quark OAM
\begin{equation}
L_q = J_q - \frac{1}{2}\Delta\Sigma_q
\end{equation}
which, through equations~\ref{eq:JisSumRule} and~\ref{eq:quarkspin}, is a 
function of the quark GPDs $E$, $H$, and $\tilde{H}$.

The GPDs of the up and down quarks can be extracted from measurements on the 
neutron and proton using  isospin symmetry
\begin{align}
   F_u &= \frac{3}{5} \left( 4 F^p - F^n\right)\\
   F_d &= \frac{3}{5}\left( 4 F^n - F^p \right)
\end{align}
where $F \in [H,E,\tilde{H},\tilde{E}]$. The flavor separation is straight 
forward if equal data on the proton and neutron GPDs are available.  However, 
clean neutron data are not available due to a non-existent free-neutron target, 
and when neutron measurements are made using nuclear targets they suffer from a 
variety of nuclear effects previously discussed. Part of the proposed 
experiment would provide a precise neutron measurement from deuteron with a 
technique similar to the one used by the BoNuS experiment\footnote{BoNuS 
stands for ``Barely off-shell Nucleon Structure''.} to extract the neutron 
structure function at high x~\cite{bonus6,bonus12}.

\begin{figure}
   \centering
   \includegraphics[width=0.60\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure0.pdf}
   \caption{\label{fig:taggedDVCS}Tagged DVCS diagram showing the detection of 
   the forward DVCS final state particles in CLAS12 and the detection of the 
recoiling spectator system (A-1) in ALERT. The hatched circle represents the 
hard DVCS process.}
\end{figure}

%This proposal consists of three sets of measurements which collectively aim to 
%elucidate the various roles that medium modifications, final state 
%interactions, and short range correlations play in light nuclei, all while, 
%providing much needed measurements for completing the picture of the nucleon.  
%The first set of measurements are aimed at measuring DVCS on the quasi-free 
%neutron by detecting the recoiling proton when scattering off deuterium 
%target.  The second set of measurements will focus on measuring DVCS off 
%$^4$He for bound neutrons (protons) by detecting recoil $^3$He ($^3$H) and 
%compare it to the quasi-free neutron measurements. For the proton case, one 
%can use the CLAS12 DVCS proton data to produce the ratio of bound proton over 
%free proton BSA.

%The final set of measurements, catch-all of sorts, includes coherent DVCS on a 
%(free) deuteron, and, using a $^4$He target, the quasi-coherent deuteron DVCS 
%breakup with a recoiling spectator deuteron and adjacent exotic DVCS process 
%such as the three-body break-up (3BBU) in neutron DVCS where the recoil 
%spectator system consists of a deuteron and proton. 
%
% I think DVCS on bound proton should be listed in there as well. This is the
% target for which we have the best data in the free configuration. I am a little
% doubtful about a deuterium-deuterium break-up of helium. I would set it like 
% this 1 free neutron 2 bound nucleons 3 other break-up: deut-deut & 3BBU (Raphael)
%
%Since the data will simultaneously record tagged semi-inclusive deep inelastic 
%scattering (SIDIS) events and we expect to also to observe other final state 
%hadrons (pions, kaons, etc.) in CLAS12, we plan on similarly exploring nuclear 
%effects on the transverse momentum distributions (TMDs).
%
\subsection{Polarized EMC Effect}\label{sec:polEMCeffect}
%
Using polarized nuclear targets, the polarized EMC effect can be measured and 
is predicted to be larger than the ``unpolarized'' EMC 
effect~\cite{Cloet:2005rt}.  The polarized EMC effect is measured through the 
ratio of spin structure functions $g_1$ of a bound nucleon to that of a free 
nucleon. DIS measurements typically require both a longitudinally polarized 
target and longitudinally polarized beam to measure $g_1$. The $\sin\phi$ 
harmonic of the neutron DVCS beam spin asymmetry is
\begin{equation}\label{eq:ALUneutronEMC}
   A_{LU,n}^{\sin\phi} \propto \operatorname{Im}( F_1^n \mathcal{H}^n- 
   \frac{t}{4M^2} F_2^n \mathcal{E}^n+ 
   \frac{x_B}{2}(F_1+F_2)^n\tilde{\mathcal{H}}^n)~.
\end{equation}
The first term is suppressed by $F_1^n$ and if for the moment we neglect the 
$\mathcal{E}$ term, the ratio of this asymmetry for a bound neutron to a free 
neutron is
\begin{equation}\label{eq:PolEMCRatio}
   R_{AL,n}^{\sin\phi} = \frac{A_{LU,n^{*}}^{\sin\phi}}{A_{LU,n}^{\sin\phi}} 
   \simeq 
   \frac{G_M^{n^*}(t)}{G_M^n(t)}\frac{\operatorname{Im}(\tilde{\mathcal{H}}^{n^*}(\xi,\xi,t))}{\operatorname{Im}(\tilde{\mathcal{H}}^{n}(\xi,\xi,t))}
\end{equation}
which in the forward limit becomes
\begin{equation}\label{eq:FWDPolEMCRatio}
   R_{AL,n}^{\sin\phi} \longrightarrow 
   \frac{\mu_{n^*}}{\mu_{n}}\frac{g_1^{n^*}(x)}{g_1^{n}(x)}~,
\end{equation}
where $\mu_{n^*}/\mu_{n}$ is the ratio of the bound neutron magnetic moment to the 
free neutron magnetic moment, and $g_1^{n^*}/g_1^n$ is similarly the ratio of 
the bound to free neutron spin structure functions.

Equations~\ref{eq:PolEMCRatio} and~\ref{eq:FWDPolEMCRatio} are rather 
interesting for a few reasons. First, they can be used to draw conclusions 
about the behavior of the \emph{polarized} quark distributions in unpolarized nuclei 
without using a polarized target. But we must note the unjustified neglect of the 
$\mathcal{E}^{n}$ term which complicates subsequent analysis.
We point out this term in the ratio of Equation~\ref{eq:PolEMCRatio} because it 
highlights the observable's sensitivity to medium modifications. Specifically
noting that a modification to the nucleon's static properties, such as, 
anomalous magnetic moment or polarization-dependent transverse-charge 
distribution (see~\cite{Burkardt:2002hr}), would also manifest themselves 
through the magnetic form factor whose ratio also appears in this observable.

%The sensitivity of the this ratio to medium modification effects is further 
%enhanced if Equation\,\ref{eq:FWDPolEMCRatio} deviates from unity. That is, 
%the x dependence of the ratio could only explained by modification of the 
%nucleon spin structure function $g_1^n$ at the parton level.  (**** YOU HAVE 
%TO RECOGNIZE THE APPROXIMATIONS YOU MADE AND REDUCE THE STATEMENT OF 
%CONCLUSION.  REPLACE IT BY FEELING, GUIDANCE OR YOU WILL HAVE TO STUDY THE 
%VALIDITY OF YOUR APPROXIMATIONS).
Also, a measurement of the BSA in Equation~\ref{eq:ALUneutronEMC} will provide 
important model constraints on the GPD $E^n$ and measurements of the ratio with 
$^4$He would further constrain nuclear GPD models. This is particularly 
motivating in the context of Equation~\ref{eq:JisSumRule}, where $E^n$ is 
clearly an important quantity for understanding the quark orbital angular 
momentum.

%****************WHAT THESE EQS ARE DOING HERE?*************************
%\begin{align}
%\operatorname{Im}(\mathcal{H}(\xi,\xi,t)) &= H(\xi,\xi,t)-H(-\xi,\xi,t), & 
%   \mathcal{H}(x,0,0) &= q(x)+\bar{q}(x)\\ 
%   \operatorname{Im}(\tilde{\mathcal{H}}(\xi,\xi,t)) &= 
%   \tilde{H}(\xi,\xi,t)+\tilde{H}(-\xi,\xi,t) &
%   \tilde{\mathcal{H}}(x,0,0) &= \Delta q(x)+\Delta\bar{q}(x)\\
%   \operatorname{Im}(\mathcal{E}(\xi,\xi,t)) &= E(\xi,\xi,t)-E(-\xi,\xi,t) & 
%\end{align}
%****************WHAT THESE EQS ARE DOING HERE?*************************


%In summary, we propose to measure the neutron DVCS beam spin asymmetry for a 
%(quasi-free) and bound neutron with excellent leverage on various nuclear 
%effects through recoil spectator tagging.
%
\subsection{Models of Nuclear Effects}
%
To understand the potential sources of observable nuclear effects, we take the 
ratio of beam spin asymmetries for a bound nucleon to that on the free nucleon 
target.  Here we discuss just two models that make very different predictions 
for similar ratios based on the presumed sources of the nuclear effects.

First, predictions for the ratio of beam spin asymmetry at 6 GeV are shown in 
Figure~\ref{fig:ALUratio}, which shows the bound proton beam spin asymmetry, 
$A_{LU}^{p^{*}}$, to the free proton $A_{LU}^{p}$~\cite{Guzey:2008fe}.  These 
calculations use the medium modified GPDs as calculated from the quark-Meson 
coupling model. However, they do not include FSIs and predict their 
contribution is at most a few percent. In another calculation, Liuti et 
al.~\cite{Liuti:2005qj,Liuti:2006dx} use a realistic spectral function and 
consider off-shell effects. This is a more traditional approach to explaining 
differences in a bound nucleon. They make predictions for the ratio
\begin{equation}\label{eq:RA}
   R_A(x,\xi=0,t) = \frac{H_A(x,\xi=0,t) F_N(t)}{H_N(x,\xi=0,t) F_A(t)}
\end{equation}
which is shown in Figure~\ref{fig:RAratio}. For more discussions of modeling 
nuclear effects see~\cite{Dupre:2015jha,Atti:2015eda}.
\begin{figure}
   \centering
   \includegraphics{figs/DVCS_ALU_ratio_Guzey_2009.pdf}
   \caption{\label{fig:ALUratio}Beam spin asymmetry ratio of a bound proton to 
   a free proton.  Reproduced from~\cite{Guzey:2008fe}.}
\end{figure}
\begin{figure}
   \centering
   \includegraphics{figs/DVCS_RA_Liuti_2005.pdf}
   \caption{\label{fig:RAratio}Predictions for the ratio given in equation 
   \ref{eq:RA}. Reproduced from~\cite{Liuti:2005qj}.}
\end{figure}

It is clear that the role of off-shellness, and final state interactions in 
nuclei needs to be better understood if we are to conclude that the nucleon 
structure is modified by the nuclear medium. With spectator tagging, we will be 
able to test these models over a broad range of spectator momentum and angles.  
This tagging technique can be used as a knob to tune the effect of final state 
interactions and either maximized or minimized it.%
%\subsection{Final state interactions}
%
\begin{figure}
   \centering
   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure13.pdf}
   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure12.pdf}
   \caption{\label{fig:deuteronFSIdiagram}PWIA diagram for incoherent DVCS on 
   the deuteron (left) and with the inclusion of final state interactions 
(right).}
\end{figure}

To understand the regions where FSIs are expected to be significant, we first 
look at the deuteron. Consider the quasi-elastic scattering on a quasi-free 
nucleon as shown in Figure\,\ref{fig:deuteronFSIdiagram}. Measurements of the  
cross section as a function of missing momentum are shown in 
Figure\,\ref{fig:quasiElasticDeuteronFSI} along with model calculations in PWIA 
with different final state interactions. From model calculations it was found 
that the PWIA was insufficient for describing the data at missing momenta above 
300 MeV/c.
\begin{figure}
   \centering
   \includegraphics{figs/elastic_deuteron_FSI.pdf}
   \caption{\label{fig:quasiElasticDeuteronFSI}Reduced quasi-elastic scattering 
   cross section  on deuteron. Reproduced from ~\cite{Jeschonnek:2008zg}.  The 
   reduced cross section includes a term multiplying the cross section by $M_d 
 f_{\text{rec}}/(\sigma_{\text{Mott}} m_p m_n p_p)$, where the $f_{\text{rec}}$ 
 is the recoil factor, and $\sigma_{\text{Mott}}$ is the Mott cross section.  
 See reference~\cite{Jeschonnek:2008zg} for more details.}
\end{figure}
Similarly, the size of the FSI strength as a function of spectator momentum 
(left) and angle relative to the momentum transfer, $\theta_s$, (right) is 
shown in 
Figure~\ref{fig:deuteronFSI}~\cite{CiofidegliAtti:2003pb,CiofidegliAtti:2002as}.  
At low recoil momentum and backwards spectator angle, the FSIs are negligible, 
where at high momenta perpendicular to the momentum transfer, the FSIs are 
maximized.

\begin{figure}
   \centering
   \includegraphics{figs/FSI_quasielastic_Atti_2003.pdf}
   \caption{\label{fig:deuteronFSI} Ratio of cross sections for the FSI model 
   from~\cite{CiofidegliAtti:2003pb} to PWIA calculation as a function of
   the spectator momentum (left) and spectator angle (right).}
\end{figure}

%\section{Off-Forward EMC Effect}

%\begin{equation}
%e + ^2\text{H} \longrightarrow e^{\prime} + \gamma + p (+ n)
%\end{equation}
%   $^2$H$(e,e^{\prime}\gamma~p)n$,        neutron DVCS ,

%\begin{equation}
%   e + ^4\text{He} \longrightarrow e^{\prime} + \gamma + ^3\text{He} (+ n)
%\end{equation}
%Tagging a recoil helium-3, the reaction $^4$He$(e,e^{\prime}\gamma~^3$He$)n$ 
%would be sensitive to the effects of a more tightly bound neutron. In this 
%reaction the neutron can be detected forward or selected via missing mass.
%
%\begin{equation}
%   e + ^4\text{He} \longrightarrow e^{\prime} + \gamma + ^3\text{He} (+ n)
%\end{equation}
%Tagging a recoil helium-3, the reaction $^4$He$(e,e^{\prime}\gamma~^3$He$)n$ 
%would be sensitive to the effects of a more tightly bound neutron. In this 
%reaction the neutron can be detected forward or selected via missing mass.
%
%\subsection{Coherent DVCS on the Deuteron}
%%
%With a deuteron target, as previously mentioned, has been measured and the 
%Compton form factors extracted simultaneously with the neutron's due to the in 
%ability to separate the events. With tagging of a recoil deuteron, in either 
%CLAS12 or ALERT, these events are cleanly separated. This complementary data 
%can be used with previous measurements to test the validity of the neutron  
%extraction in the PWIA given by equation \ref{eq:PWIAnDVCSextract}.

%\subsubsection{Coherent deuteron DVCS with a $^2$H target}

%The coherent DVCS process on deuterium presents a theoretical challenge, due 
%to being a spin-1 target, it contains nine GPDs of which a BSA is only 
%sensitive to just three.

%\begin{figure}
%   \centering
%   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure21.pdf}
%   \includegraphics[width=0.49\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure16.pdf}
%   \caption{\label{fig:coherentDVCSdeuteron}}
%\end{figure}
%
%\begin{figure}
%   \centering
%   \includegraphics[width=0.60\textwidth]{../tikz/dvcs_feynman/dvcs_feynman-figure11.pdf}
%   \caption{\label{fig:}}
%\end{figure}
%
%\begin{equation}
%   e + ^2\text{H} \longrightarrow e^{\prime} + ^2\text{H} + \gamma 
%\end{equation}
%
%   $^2$H$(e,e^{\prime}\gamma~^2$H$)$,     deuteron DVCS ,
%
%\subsubsection{Incoherent DVCS on a bound Deuteron}
%
%
%If only a scattered deuteron is detected from a helium target, the missing mass 
%of two nucleons can be used to ensure exclusivity of the reaction 
%$^4$He$(e,e^{\prime}\gamma~^2$H$)$NN.

%A recoil proton is detected with a forward deuteron
%$^4$He$(e,e^{\prime}\gamma~^2$H$~p)n$.
%
%Although the rates are expected to be low, the reaction 
%$^4$He$(e,e^{\prime}\gamma~^2$H$^2$H) would be a very interesting subset.

%\subsection{DVCS on the proton and recoil breakup}
%   $^4$He$(e,e^{\prime}\gamma~^3$He$)n$,  neutron DVCS in  helium-4
%   $^4$He$(e,e^{\prime}\gamma~^3$H$)p$,   proton DVCS in  helium-4
%   $^4$He$(e,e^{\prime}\gamma~^2$H$p)n$,  proton DVCS in  helium-4
%   $^4$He$(e,e^{\prime}\gamma~pp)nn$,     proton DVCS in  helium-4
%
\section{Motivation Summary}
%
In summary, we propose to perform the following key measurements using CLAS12 and ALERT for the low energy spectator recoil tagging:
\begin{itemize}
\item \emph{Bound proton DVCS} with a $^4$He target where the final state is 
   fully detected by tagging a spectator $^3$H and the struck proton is 
   detected in CLAS12.  The PWIA will be tested by the redundant measurement of 
   the momentum transfer as explained above and in great detail in appendix 
   \ref{chap:appendixKine}. Thus, kinematics with significant FSIs are 
   identified in a completely \emph{model independent} way. 
   %allowing for the neutron DVCS measurement to proceed    for systematic 
   %study of FSIs for this reaction and $^4$He$(e,e^{\prime}\,\gamma~^3$He$)n$ 
   %(*******THIS IS CONFUSING, THE OVERDETERMINED KINEMATICS IS FROM THE PROTON 
   %CHANNEL, YOU CAN USE ISOSPIN ARGUMENTS FOR NEUTRON AS WELL BUT YOU NEED TO 
   %SPELL THEM OUT HERE*******).
\item \emph{Bound neutron DVCS} with a $^4$He target where the neutron goes 
  undetected and the spectator $^3$He is detected in ALERT. Using the same 
  kinematics identified in the previous measurement, and using iso-spin 
  (charge) symmetry, we can conclude that the struck neutron feels the same  
  final state interactions as the struck proton.
\item \emph{Quasi-free neutron DVCS} with a $^2$H target where the recoil 
  proton is tagged and the struck neutron goes undetected.
\end{itemize}




