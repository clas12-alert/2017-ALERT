\setlength\parskip{\baselineskip}%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Formalism and Experimental Observables}
\label{chap:expobs}

The observables which are sensitive to $^4$He's quark and gluon GPDs are 
noticeably  different, and therefore, so are the techniques for extracting the 
quark and gluon information. In this chapter we first discuss how the GPDs 
relate to the proposed measurements. In \ref{sec:DVCSFormalism} the methods by 
which we extract the quark GPD $H_A$ from the DVCS beam spin asymmetry are 
presented and the current experimental status is discussed.  In 
section~\ref{sec:DVMPFormalism} we show how the gluon GPD $H_g$ is extracted 
from the angular distribution of the $\phi$ meson decay. 

%Then we will conclude with a discussion of the quark and gluon distributions 
%in impact-parameter space.

\section{Generalized Partons Distributions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% need separate the GPD discussion from the DVCS discussion
%
GPDs are universal non-perturbative objects, entering the description of hard 
exclusive electroproduction processes. They are defined for each quark flavor 
$f$ and gluon as matrix elements of light cone operators~\cite{Belitsky:2005qn} 
describing the transition between the initial and final states of a hadron.  
The GPDs depend on two longitudinal momentum fraction variables $(x,\xi)$ and 
on the momentum transfer $t$ to the target. At twist-2 order, $\xi$ 
can be calculated as $x_B/(2 - x_B)$, where $x_{B}$ ($= Q^2/2M_p\nu$) is the Bjorken variable , and -2$\xi$ is the longitudinal fraction of the momentum transfer $\Delta$, with $\Delta^2 =(p-p')^2 = t$. $x$ is the average longitudinal momentum fraction of the parton involved in the process. 

In the limit $\xi \rightarrow 0$, the 2-dimensional Fourier transform yields 
impact parameter GPDs
\begin{equation}
  f(x,\bm{b_{\perp}}) =  \int \frac{d^2 \bm{\Delta}_{\perp}}{4\pi^2} F(x, \xi=0, 
  \Delta)\mathrm{e}^{-i \bm{b}_{\perp}\cdot\bm{\Delta}_{\perp}}
\end{equation}
where $F\in {H,E,\tilde{H},\tilde{E}}$. With azimuthal symmetry in the transverse plane this 
reduces to the Hankel transform
\begin{align}
  f(x,b) &=  \int_{0}^{\infty} J_{0}(b \Delta_{\perp}) 
  F(x,0,\Delta_{\perp}^2) \frac{d\Delta_{\perp}}{\pi} \\ &= \int_{0}^{\infty} 
  J_{0}(b\sqrt{t}) F(x,0,t) \sqrt{t} \frac{dt}{2\pi}\label{eq:HankelTransform}
\end{align}
where $J_0$ is a Bessel function of the first kind.
In this limit the impact parameter GPDs can be interpreted as the probability 
distribution of finding a parton with longitudinal momentum fraction $x$ at a 
transverse position $b$ with respect to the nucleus' center of 
momentum~\cite{Burkardt:2002hr}.

GPDs can also be considered as the off-forward kinematic generalizations of the 
standard Parton Distributions Functions (PDFs) from inclusive DIS.  PDFs can be 
recovered from GPDs in the limit of zero momentum transfer between the initial 
and final protons (the forward limit), with no target spin flip. Similar to 
DIS, higher twist terms describe quark-gluon-quark correlations which are 
suppressed by powers of $1/Q$, we
elaborate in the 4$^{th}$ proposal of the run group on possible methods to 
study these effects. 

The spin zero of the $^4$He target allows for a simple parametrization of its 
partonic structure characterized at leading twist by one chirally-even GPD 
$H_A$. In the forward limit ($t \to 0$), this GPD reduces to the usual parton 
densities of $^4$He measured in DIS. The polynomiality property of GPDs leads 
to interesting consequences: the first Mellin moment provides an explicit link 
with the electromagnetic form factor $F_A$ of the nucleus
\begin{equation}
\sum_f e_{f} \int_{-1}^{1} dx \, H_A^f(x,\xi,t) = F_A(t) \, ,
\end{equation}
and the second moment yields the relationship
\begin{equation}
\int_{-1}^{1} dx \, x \, H_A^f(x,\xi,t) = M_2^{f/A}(t) + \frac{4}{5} \xi^2 d_A^f(t) \,
\label{dterm}
\end{equation}
which constrains the $\xi$-dependence of the GPDs. At $t \to 0$, the first term 
of the right-hand side of Eq.~(\ref{dterm}) is the momentum fraction of the 
target carried by a given quark. The second term of Eq.~(\ref{dterm}) is the 
so-called $D$-term which was shown to encode information about the spatial
distribution of forces experienced by quarks and gluons inside 
hadrons~\cite{Polyakov:2002yz}.

\section{Coherent DVCS}\label{sec:DVCSFormalism}

\subsection{Accessing the Quark GPD}

The handbag diagram in Figure~\ref{fig:handbag} displays a hard part which is 
calculable in perturbative QCD, and a soft, non-perturbative, part which 
contains the fundamental partonic structure of the nucleus. However, because 
of the loop in the handbag diagram, the $x$ variable is not directly accessible
in the DVCS process, so we access the Compton Form Factor
(CFF), noted $\mathcal{H}_{A}$, and expressed in terms of the GPD as 
\begin{align}
  \begin{split}
    \Re e(&\mathcal{H}_{A}) = \mathcal{P} 
  \int_{0}^{1}dx[H_A(x,\xi,t)-H_A(-x,\xi,t)] \, C^{+}(x,\xi), \end{split} \\
  \Im m(&\mathcal{H}_{A}) = H_A(\xi,\xi,t)-H_A(-\xi,\xi,t),
\end{align}
with $\mathcal{P}$ as the Cauchy principal value integral, and $C^{+}(x,\xi)$ a 
coefficient function ($=  \frac{1}{x-\xi} + \frac{1}{x+\xi}$) 
\cite{Guidal:2013rya}.


%
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.65\linewidth]{fig_2017/coherent_handbag.pdf}
    \caption{Lowest order (QCD) amplitude for the DVCS and DVMP processes, the 
      so-called handbag diagram. $q$, $q'$ represent the the four-momentum of the 
      virtual and real photons or mesons, and $p$, $p'$ are the initial and final 
    four-momentum of the target nucleus.}
    \label{fig:handbag}
  \end{center}
\end{figure}
%

Experimentally, the DVCS reaction is indistinguishable from the Bethe-Heitler 
(BH) process, which is the reaction where the final photon is emitted either 
from the incoming or the outgoing leptons. The BH process is not sensitive to 
GPDs and does not carry information about the partonic structure of the 
hadronic target. The BH cross section is calculable from the well-known 
electromagnetic FFs. The DVCS amplitude is enhanced through the interference 
with the BH process. Figure \ref{fig:He-4_FFs} shows the world measurements of 
the $^4$He $F_{A}(t)$ along with theoretical calculations. Following the $^4$He 
$F_{A}(t)$ parametrization by R.  Frosch and his collaborators 
\cite{PhysRev.160.874} (valid at the small values of $-t$ which are of interest 
in this work), Figure \ref{fig:BH_cross_section_4He} shows the calculated BH as 
a function of the azimuthal angle between the leptonic and the hadronic planes 
($\phi$), using 11~GeV electron beam on a $^4$He target.
%%%%%%%%%%%
\begin{figure}[htb]
  \begin{minipage}[c]{.46\linewidth}
    \hspace{-0.2in}\includegraphics[height=6.0cm]{fig_NuclGPD/He-4_FF.png}
    \caption{$^4$He charge form factor measurements at Stanford, SLAC, Orsay, Mainz 
      and JLab Hall A compared with theoretical calculations. The figure is from 
    \cite{PhysRevLett.112.132503}. }
    \label{fig:He-4_FFs}
  \end{minipage} \hfill
  \begin{minipage}[c]{.46\linewidth}
    \hspace{-0.3in}\includegraphics[height=7.1cm]{fig_NuclGPD/BH_xsection_phi.pdf}
    \caption{The calculated BH cross section as a function of $\phi$ on a $^4He$ 
      target at three values of $x_{B}$ and fixed values of $Q^{2}$ and $t$.  
      ($t$~=~-~0.1~GeV$^2$/c$^2$ corresponds to $Q^2$~$\approx$~2.57~fm$^{-2}$ on 
    figure \ref{fig:He-4_FFs}).}
    \vspace{+0.3in}
    \label{fig:BH_cross_section_4He}
  \end{minipage}
\end{figure}
%%%%%%%%%%%%%%%%%

The differential cross section of leptoproduction of photons for a 
longitudinally-polarized electron beam ($\lambda = \pm 1$) and an unpolarized 
$^4$He target can written as:
\small
\begin{equation}
\frac{d^{5}\sigma^{\lambda}}{dx_{A} dQ^{2} dt d\phi_{e} d\phi} = 
\frac{\alpha^{3}}{16 \pi^{2}} \frac{x_{A} \, y}{Q^{4} \sqrt{1 + \epsilon ^{2}}} 
\frac{
|\mathcal{T}_{BH}|^{2} + |{\mathcal{T}}_{DVCS}^{\lambda}|^{2} + {\mathcal{I}}_{BH*DVCS}^{\lambda}}{e^{6}}
\label{eq:sigdiff}
\end{equation}
\normalsize
where $y = \frac{p \cdot q}{p \cdot k}$, $\epsilon  =  \frac{2 x_{A} 
M_{A}}{Q}$, $x_A  =  \frac{Q^2}{2 p \cdot q}$, ${\mathcal{T}}_{DVCS}$ is the 
pure DVCS scattering amplitude, $ {\mathcal{T}}_{BH}$ is the pure BH amplitude 
and ${\mathcal{I}}^{\lambda}_{BH*DVCS}$ represents the interference amplitude.  
Similarly to a nucleon target one can write out the azimuthal angle, $\phi$, 
dependence for the nuclear BH, DVCS and interference terms in the cross 
section: each modulation in $\phi$ is multiplied by a structure function 
containing the GPDs of interest. The different amplitudes are written as 
\cite{Belitsky:2008bz},
\small
\begin{equation}
 |\mathcal{T}_{BH}|^{2} =  \frac{e^{6} (1 + \epsilon^{2})^{-2}}{x^{2}_{A} y^{2} 
 t \mathcal{P}_{1}(\phi) \mathcal{P}_{2}(\phi)} \left[ c_{0}^{BH} + c_{1}^{BH} 
 \cos(\phi) + c_{2}^{BH} \cos(2\phi)\right] \label{TTBH}
\end{equation}
%
\begin{equation}
 |\mathcal{T}_{DVCS}|^{2} =  \frac{e^{6}}{y^{2} Q^{2}} \left[ c_{0}^{DVCS} + 
 \sum_{n=1}^{2} \Bigg( c_{n}^{DVCS} \cos(n \phi) + \lambda s_{n}^{DVCS} \sin(n 
 \phi)\Bigg) \right] \label{TTDVCS}
\end{equation}
%
\begin{equation}
 \mathcal{I}_{BH*DVCS} =  \frac{\pm e^{6}}{x_A y^{3} t \, \mathcal{P}_{1}(\phi) 
 \mathcal{P}_{2}(\phi)} \left[ c_{0}^{I} + \sum_{n=0}^{3} \Bigg( c_{n}^{I} 
 \cos(n \phi) + \lambda s_{n}^{I} \sin(n \phi) \Bigg) \right]
  \label{TTinter}, 
 \end{equation}
 %
The explicit expressions of the coefficients can be found in Appendix \ref{app:Helium_cross_section}.
%
It is convenient to use the beam-spin asymmetry as DVCS observable because most of the experimental normalization and acceptance issues cancel out in an asymmetry ratio. The beam-spin asymmetry is measured using a longitudinally polarized lepton beam (L) on an unpolarized target (U) and defined as:
%
\begin{equation}
A_{LU} = \frac{d^{5}\sigma^{+} - d^{5}\sigma^{-} }
                {d^{5}\sigma^{+} + d^{5}\sigma^{-}}.
\label{BSA_equation}
\end{equation}
% 
where $d^{5}\sigma^{+}$($d^{5}\sigma^{-}$) is the DVCS differential cross section for a positive (negative) beam helicity.
%
 At leading twist, the beam-spin asymmetry ($A_{LU}$) with the two opposite helicities of a  longitudinally-polarized electron beam (L) on a spin-zero target (U) can be written as:        
% 
 \begin{eqnarray}
 \label{eq:coh_BSA}
A_{LU}& =& \frac{x_A(1+\epsilon^2)^2}{y} \, s_1^{INT} \sin(\phi) \, 
\bigg/ \, \bigg[ \, \sum_{n=0}^{n=2}c_n^{BH}\cos{(n\phi)} +  \\
& & \frac{x_A^2 t {(1+\epsilon^2)}^2}{Q^2} P_1(\phi) P_2(\phi) \, c_0^{DVCS} + 
\frac{x_A (1+\epsilon^2)^2}{y} \sum_{n=0}^{n=1} c_n^{INT} \cos{(n\phi)} \bigg].  \nonumber 
\end{eqnarray}
%
where $\mathcal{P}_1(\phi)$ and $\mathcal {P}_2(\phi)$ are the Bethe-Heitler 
propagators. The factors: $c_{0,1,2}^{BH}$, $c_0^{DVCS}$, $c_{0,1}^{INT}$ and 
$s_1^{INT}$ are the Fourier coefficients of the BH, the DVCS and the 
interference amplitudes for a spin-zero target 
\cite{Kirchner:2003wt,Belitsky:2008bz}.
%
The beam-spin asymmetry ($A_{LU}$) can be rearranged as
%
\begin{equation}
A_{LU}(\phi) = \frac{\alpha_{0}(\phi) \, \Im m(\mathcal{H}_{A})}
{\alpha_{1}(\phi) + \alpha_{2}(\phi) \, \Re e(\mathcal{H}_{A}) + \alpha_{3}(\phi) \, 
\big( 
\Re e(\mathcal{H}_{A})^{2} + \Im m(\mathcal{H}_{A})^{2} \big)}
\label{eq:A_LU-coh}
\end{equation}
%
where $\Im m(\mathcal{H}_{A})$ and $\Re e(\mathcal{H}_{A})$ are the imaginary and real parts of the CFF $\mathcal{H}_{A}$ associated to the GPD $H_A$. The $\alpha_{i}$'s are $\phi$-dependent kinematical factors that depend on the nuclear form factor $F_A$ and the independent variables $Q^2$, $x_{B}$ and $t$. These factors are simplified as:
%
\small
\begin{eqnarray}
   \alpha_0 (\phi) & = &\frac{x_{A}(1+\epsilon^2)^2}{y} S_{++}(1) \sin(\phi)\\
    \alpha_1 (\phi) & = & c_0^{BH}+c_1^{BH} \cos({\phi})+c_2^{BH} \cos(2\phi)\\ 
   \alpha_2 (\phi) & = & \frac{x_{A}(1+\epsilon^2)^2}{y}  \left( C_{++}(0) +  
C_{++}(1) \cos(\phi) \right)\\
\alpha_3 (\phi) &=& \frac{x^{2}_{A}t(1+\epsilon^2)^2}{y} {\mathcal P}_1(\phi) 
{\mathcal P}_2(\phi) \cdot 2 \frac{2-2y+y^2 + \frac{\epsilon^2}{2}y^2}{1 + 
\epsilon^2}
\end{eqnarray}
\normalsize
%
Where $S_{++}(1)$, $C_{++}(0)$, and $C_{++}(1)$ are the Fourier harmonics in 
the leptonic tensor. Their explicit expressions can be found in Appendix 
\ref{app:Helium_cross_section}. Using the $\alpha_{i}$ factors, one can obtain 
in a model-independent way $\Im m(\mathcal{H}_{A})$ and $\Re 
e(\mathcal{H}_{A})$ from fitting the experimental $A_{LU}$ as a function of 
$\phi$ for given values of $Q^2$, $x_B$ and $t$.
%

From a practical point of view, to access the quarks' density distributions 
of the target, we need to extract $H_{A}$ from the CFF $\mathcal{H}_{A}$, that 
appear directly in the cross section expressions. With the assumption that the 
anti-quark contribution is small in our kinematical region, $H_A(\xi,\xi,t) = 
\Im m(\mathcal{H}_{A})$ is a good approximation. Reference 
\cite{Guidal:2013rya} suggests that it is a 10$\%$ to 20$\%$ correction that 
can be applied to the data. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Experimental Status}
\label{sec:expover}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The study of coherent nuclear DVCS is still in its infancy due to the 
challenging detection of the low energy recoil nucleus. The deuterium was 
investigated at HERMES~\cite{Airapetian:2009cga} and JLab Hall 
A~\cite{:2007vj}, and the HERMES experiment was the only one to study heavier 
nuclei ($^4$He, N, Ne, Kr, and Xe)~\cite{Airapetian:2009cga}. In the latter, 
the DVCS process was measured by identifying the scattered lepton and the real 
photon in the forward spectrometer. Sizable asymmetries 
(Figure~\ref{fig:her-alu-h-d-he-n}) have been reported in the missing mass region 
-1.5$< M_X <$ 1.7~GeV mass, while they generally vanish at higher 
masses~\cite{Airapetian:2009cga}.
%
\begin{figure}[htb]
  \begin{center}
    \includegraphics[width=0.65\linewidth]{fig_2017/HERMES_POINTS.pdf}
    \caption{The $t$-dependence of the BSA on H, He, N, Ne, Kr, and Xe expressed in 
      terms of the coefficient $A_{LU}^{\sin(\phi)}$ of the $\sin(\phi)$ 
      contribution to $A_{LU}$~\cite{Airapetian:2009cga}; we note that in the 
      context of the HERMES fitting procedure $A_{LU}^{\sin(\phi)} \equiv A_{LU}$ 
    i.e. the denominator of Eq.~(\ref{eq:A_LU-coh}) was neglected.}
    \label{fig:her-alu-h-d-he-n}
  \end{center}
\end{figure}
%
%\begin{figure}[h]
%\begin{center}
%\includegraphics[width=0.85\linewidth]{fig_NuclGPD/Frac_Hyd_180.pdf}
%\caption{The $A$-dependence of the ratio of the BSA on a nucleus to the BSA on 
%the proton for the coherent enriched (upper panel) and incoherent enriched 
%(lower panel) data samples~\cite{Ellinghaus:2002zw}.}
%\label{fig:frac-hyd-180}
%\end{center}
%\end{figure}
%
These asymmetries are further separated into coherent and incoherent 
asymmetries taking advantage of the different $t$-dependence of the 
electromagnetic form factors: in the $^{4}$He case, for example, the coherent 
channel was assumed to dominate below -$t$~=~0.05~GeV$^2$. The selection of the 
different regions in $t$ (below and above) is then used to define coherent 
enriched and incoherent enriched data samples. The $A$-dependence of the ratio 
of the nuclear BSA to the proton BSA, over all the measured nuclei, is reported 
to be 0.91$\pm$0.19. Within the precision of the measurements, no obvious 
$A$-dependence of the BSA is observed: the coherent enriched ratio is 
compatible with unity, which is contradicting the predictions of 
different models~\cite{Liuti:2005gi,Guzey:2003jh,Belitsky:2000vk}. The 
incoherent enriched ratio, 0.93$\pm$0.23, is also compatible with unity as one 
would expect from an impulse approximation approach~\cite{Guzey:2003jh}.
%

The CLAS collaboration has performed a new measurement (E08-024) of 
coherent exclusive DVCS on $^4$He, where all the products of the reaction have 
been detected including the low energy recoil $^4$He nucleus. This measurement was 
possible due to the high luminosity available at JLab, the large acceptance of 
CLAS spectrometer enhanced with the inner calorimeter (IC) and the addition of 
the newly built GEM based radial time projection chamber (RTPC). The IC was 
used to extend the photon detection to smaller angles and the RTPC was used to 
detect the recoil $^4$He nucleus. The data analysis and the corresponding 
internal review by the CLAS collaboration are completed \cite{eg6_note}, and a 
first publication draft is being finalized. The results indicate that the 
collaboration has been successful in measuring the exclusive DVCS both for the 
coherent and incoherent channels. Figure~\ref{fig:bsa_coh_q2_bins} shows the 
BSA $A_{LU}$ as a function of the azimuthal angle $\phi$ for different bins in 
$Q^2$ (top panel), $x_B$ (middle panel) and $-t$ (lower panel). These 
asymmetries are sizable indicating a strong nuclear DVCS signal.
%

Figure~\ref{fig:coh_Q2_xB_t_ALU} shows the $\sin\phi$ contribution to the 
coherent BSA $A_{LU}$, which also correspond to the coefficient 
$\frac{\alpha_0}{\alpha_1}$ in equation~\ref{eq:A_LU-coh} as a function of 
$Q^2$, $x_B$ and $-t$. It is clear the kinematic coverage and the statistics 
are limited, which made multidimensional binning impossible. Within the 
statistical uncertainties, CLAS data are in reasonable agreement with the 
model by Liuti et al.~\cite{Liuti:2005qj} for both the $x_B$ and $t$ 
dependencies, although a better comparison should be made with similar binning 
in $x_B$, $Q^2$ and $t$.  The Liuti at al. model includes dynamical 
off-shellness of the nucleons taking into account medium modifications beyond 
the conventional Fermi motion and binding effects, which are included in their 
spectral function. The model also appears to be consistently giving slightly 
smaller asymmetries than the data, which might indicate that some of the 
nuclear effects are still missing in this calculation. The CLAS measurements 
also agree with the HERMES data, considering HERMES large uncertainties.
%

As shown in equation \ref{eq:A_LU-coh}, one can extract both real and imaginary 
parts of the $^4$He CFF $\mathcal{H}_A$ from fitting the beam-spin asymmetry 
signals. This extraction is fully model-independent at leading twist and, in 
contrast with the proton's GPD extraction, does not necessitate any assumption on 
additional GPDs.  Figure \ref{fig:HA_CFF} presents the first ever experimental 
extraction of $\mathcal{H}_A$ from exclusive measurements as a function of 
$Q^{2}$, $x_B$, and $-t$. More theoretical effort is needed to develop 
predictions for $\mathcal{H}_A$. One can see a difference between the precision 
of the extracted real and imaginary parts, indicating the fact that the 
beam-spin asymmetry is mostly sensitive to the imaginary part of the  CFF 
$\mathcal{H}_A$.
%

These challenging CLAS measurements were a first step toward a promising 
program dedicated to nuclear QCD studies. With the 12 GeV upgrade and CLAS12 
augmented with the ALERT detector, exclusive nuclear DVCS and DVMP measurements 
in addition to tagged EMC and tagged DVCS experiments will allow our 
understanding of nuclear structure and nuclear effects to reach a new frontier.  
%
\begin{figure}[H]
\centering
\includegraphics[scale=0.8]{fig_NuclGPD/coherent-ALU_phi.pdf}
\caption{[PRELIMINARY] The measured coherent $^4$He DVCS $A_{LU}$, from EG6 
   experiment, as a function of $\phi$ and Q$^2$ (top panel), $x_{B}$ (middle 
   panel), and $-t$~(bottom panel) bins \cite{eg6_note}. The error bars 
   represent the statistical uncertainties. The gray bands represent the 
   systematic uncertainties. The red curves are the results of the fits with 
   the form of equation \ref{eq:A_LU-coh}.}
\label{fig:bsa_coh_q2_bins}
\end{figure}
%
\begin{figure}[H]
  \centering\includegraphics[scale=0.8]{fig_NuclGPD/Coherent_ALU_phi_90_V.pdf} 
  \caption{[PRELIMINARY] From EG6 experiment, the $Q^{2}$-dependence (top panel), 
    the $x_{B}$ and the $t$-dependencies~(bottom panel) of the fitted coherent 
    $^4$He DVCS $A_{LU}$ asymmetry at $\phi$= 90$^{\circ}$ (black squares) 
    \cite{eg6_note}.  The curves are theoretical predictions from 
    \cite{Liuti:2005qj} for two values of $-t$. The green circles are the HERMES 
    $-A_{LU}$ (a positron beam was used) inclusive 
  measurements\cite{Airapetian:2009cga}.} \label{fig:coh_Q2_xB_t_ALU}
\end{figure}
%
\begin{figure}[H]
  \centering \includegraphics[scale=0.8]{fig_NuclGPD/Coherent_CFF.pdf}
  \caption{[PRELIMINARY] Model-independent extraction of the imaginary (top 
    panel) and real (bottom panel) parts of the $^4$He CFF $\mathcal{H}_A$, from 
    EG6 experiment \cite{eg6_note}, as functions of $Q^{2}$ (right panel), $x_B$ 
    (middle panel), and $t$ (left panel). The full red curves are calculations 
    based on an on-shell model from \cite{Guzey:2008th}.  The black-dashed 
    curves are calculations from a convolution model based on the VGG model for the 
    nucleons' GPDs \cite{Guidal:2004nd}. The blue long-dashed curve on the 
  top-right plot is from an off-shell model based on \cite{PhysRevC.88.065206}. }
  \label{fig:HA_CFF}
\end{figure}
%

\section{Coherent \texorpdfstring{$\phi$}{phi} Production}\label{sec:DVMPFormalism}

\subsection{Accessing the Gluon GPD}\label{sec:sigLFormalism}

The gluon GPDs can be accessed in coherent $\phi$ production through a 
measurement of the longitudinal part of the differential cross section.
The gluon GPDs for the nucleon are related to the longitudinal differential 
cross-section for coherent vector meson production \cite{Girod:2012PR, 
Aktas:2005tz, Goloskokov:2007nt, Diehl:2005gn}:
\begin{equation}\label{eq:sigLproton}
  \frac{d\sigma_L}{dt} (\mathrm{proton}) = 
  \frac{\alpha_{em}}{Q^2}\frac{x_B^2}{1 - x_B}[(1 - \xi^2)|\langle H_g \rangle 
  |^2 + \mathrm{terms\,\,in} \langle E_g \rangle],
\end{equation}
where $\alpha_{em}$ is a QED coupling constant, $\xi$ is the skewness, and
the nucleon GPDs $H_g$ and $E_g$ are relatively unconstrained. Note that
the bracket notation $\langle H_g \rangle$ indicates an analog of the CFF
for the DVMP, see \cite{Favart:2015umi} for complete expressions. However for a 
spin-0 nucleus, such as $^4$He, with only one leading-twist gluon GPD, the 
extraction of the gluon GPD greatly simplifies
\begin{equation}\label{eq:sigLHe4}
  \frac{d\sigma_L}{dt} (^4\mathrm{He}) \propto |\langle H_g \rangle |^2.
\end{equation}
where $H_g$ is the only unknown on the right hand side.

The technique used to determine $\sigma_L$, which we quickly outline, is found 
in \cite{Schilling:1973ag,Schilling:1969um}. First, the angular distribution of 
the kaons decay is measured. This angular distribution is used to extract the 
spin-density matrix element. The angular distribution in the helicity frame of 
the vector meson is
\begin{eqnarray}\label{eqn:phi_r0400}
  W(\cos\theta_H) = \frac{3}{4}\left[(1 - r_{00}^{04}) + (3r_{00}^{04} -1) \cos^2 \theta_H\right]
\end{eqnarray}
where $r_{00}^{04}$ is a spin-density matrix element, and $\theta_H$ is the 
decay angle in the rest frame of the $\phi$ where the z-direction is aligned 
with the $\phi$ momentum in the center of momentum system.
Equation (\ref{eqn:phi_r0400}) is a result of s-channel helicity conservation 
and $r_{00}^{04}$ is extract by fitting its $\cos^2 \theta_H$ angular 
dependence. Next, the spin-density matrix element is used to determine the 
ratio $R = \sigma_L/\sigma_T$, which is the ratio of longitudinal to transverse 
cross-sections,
\begin{eqnarray}
  R  = \frac{r_{00}^{04}}{\epsilon(1-r_{00}^{04})},
\end{eqnarray}
where $\epsilon$ is the virtual photon polarization.

With $R$ determined from decay distribution of the vector meson, the measured 
differential cross-section is then used to extract the longitudinal part as
\begin{eqnarray}
  \frac{d\sigma_L}{dt} = \frac{1}{(\epsilon + 1/R)\Gamma(Q^2, x_B, 
  E)}\frac{d^3\sigma}{dQ^2 dx_B dt},
\end{eqnarray}
where $\Gamma$ is the virtual photon-flux.  Now with $d\sigma_L/dt$ extracted, 
we can use it in equation (\ref{eq:sigLHe4}) to study the gluon distribution in 
$^4He$.

% For $\phi$ production off the spin zero $^4$He target, we can expect:
%where $A$ is the nucleon number of $^4$He, and $F_C$ the charge form factor of 
%$^4$He is parametrized using the world data through its first minimum in $t$ 
%following: $F_{C,^4He} = (1 - (2.5t)^6)e^{11.7t}$.  The calculation of the 
%$\phi$ production cross-section off the proton follows the exact formalism as 
%put forth by the accepted CLAS12 proposal PR12-12-007 \cite{Girod:2012PR}.  A 
%brief recap of that calculation is presented below:  The 3$^{rd}$ order 
%differential cross-section for the unpolarized case is defined as:
%\begin{eqnarray}
% \frac{d^3\sigma}{dx_B\,dQ^2\,dt} = 
% \Gamma(x_B,Q^2,E)\left(\frac{d\sigma_T}{dt}(W,Q^2,t) + 
% \epsilon\frac{d\sigma_L}{dt}(W,Q^2,t)\right)
%\end{eqnarray}


\subsection{Experimental Status}

Like the case with coherent DVCS, the experimental status of coherent $\phi$ 
production on nuclear targets is lacking. We are proposing the first 
measurement of exclusive electroproduction of the $\phi$ on $^4$He.  However, 
exclusive electroproduction on the nucleon does provide a very useful starting 
point. We will use the existing data on the proton, which is shown in 
Figures~\ref{fig:PR1207_verify} and~\ref{fig:PR1207_verify2}, to build up a 
reasonable model (see \ref{sec:phiEG}) which can be used to estimate production 
rates.
\begin{figure}[htb]
  \centering
  %{%
  %\setlength{\fboxsep}{0pt}%
  %\setlength{\fboxrule}{1pt} \fbox{
  \includegraphics[width=0.5\textwidth,clip,trim=7mm 0mm 7mm 
  5mm]{fig_NuclGPD/PR1207_RQ2.png}
%}}
  \caption{Figure come directly from PR12-12-007 \cite{Girod:2012PR}. The 
    parametrization of $R$ used to calculate $\phi$ production off a proton 
    target plotted vs Q$^2$ against world data. For more information on the 
    world data. See references: CLAS \cite{Lukashin:2001sh, Santoro:2008ai}, 
    Cornell \cite{Dixon:1978vy, Cassel:1981sx}, HERMES \cite{Borissov:2000zz}, 
    NMC \cite{Arneodo:1994id}, ZEUS \cite{Chekanov:2005cqa}, and H1 
  \cite{Aaron:2009xp}.} \label{fig:PR1207_verify}
\end{figure}
\begin{figure}[htb]
  \centering
  \includegraphics[width=0.48\textwidth,clip,trim=7mm 0mm 7mm 
  2mm]{fig_NuclGPD/PR1207_sigTQ2.png}
  %{%
  %\setlength{\fboxsep}{0pt}%
  %\setlength{\fboxrule}{1pt} \fbox{
  \includegraphics[width=0.48\textwidth,clip,trim=5mm 0mm 7mm 
  5mm]{fig_NuclGPD/PR1207_sigTW.png}
  %}}
  \caption{Figures come directly from PR12-12-007 \cite{Girod:2012PR}. The 
    parametrization in W and Q$^2$ used for cross-section calculation for $\phi$ 
    production off a proton target plotted against world data. For more 
    information on the world data, see references: CLAS \cite{Lukashin:2001sh, 
    Santoro:2008ai}, Cornell \cite{Dixon:1978vy, Cassel:1981sx}, HERMES 
    \cite{Borissov:2000zz}, NMC \cite{Arneodo:1994id}, ZEUS 
  \cite{Chekanov:2005cqa}, and H1 \cite{Aaron:2009xp}.  Reproduced from 
\cite{Girod:2012PR} \label{fig:PR1207_verify2}}
\end{figure}

%\section{Distributions in the Transverse Plane}
%
%Put a discussion of impact parameter GPDs

