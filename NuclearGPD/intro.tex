\chapter*{Introduction\markboth{\bf Introduction}{}}
\label{chap:intro}
\addcontentsline{toc}{chapter}{Introduction}

\epigraph{The general evidence on nuclei strongly supports the view that 
    the $\bm{\alpha}$ particle is of primary importance as a unit of 
    the structure of nuclei in general and particularly of the heavier 
    elements.  It seems very possible that the greater part of the mass of 
    heavy nuclei is due to $\bm{\alpha}$ particles which have an independent 
existence in the nuclear structure}{--- \textup{Rutherford, Chadwick, and 
Ellis~(1930)}\\Radiations from Radioactive Substances\footnote{This was the 
first textbook on nuclear physics and notably published two years before the 
discovery of the neutron.}}

%
Inclusive deep inelastic scattering (DIS) experiments have been instrumental in 
advancing our understanding of the QCD structure of nuclei and the effect of 
nuclear matter on the structure of hadrons. A great example is the observation 
by the European Muon Collaboration (EMC) of a deviation of the deep inelastic 
structure function of a nucleus from the sum of the structure functions of the 
free nucleons, the so-called EMC effect~\cite{Aubert:1983xm}. It became clear 
that even in a DIS process characterized by high locality of the probe-target 
interaction region, a different picture emerges from the nucleus other than a 
collection of quasi-free nucleons. On the theory side, despite decades of 
theoretical efforts~\cite{Miller-PRC2002,Thomas-Annal-2004,Liuti:2005qj,
Rezaeian-Pirner-2006,Zhen-min-He-1998} 
with increased sophistication, a unifying physical picture of the origin of the 
EMC effect is still a matter of intense debate. To reach the next level of our 
understanding of nuclear QCD and unravel the partonic structure of nuclei, 
experiments need to go beyond the inclusive measurements and focus on exclusive 
and semi-inclusive reactions. \\

Hard exclusive experiments such as Deep Virtual Compton Scattering (DVCS) and 
Deep Virtual Meson Production (DVMP) provide an important new probe that will 
allow us to discern among the different interpretations of nuclear effects on 
the structure of embedded nucleons in the nuclear medium. By introducing a new 
framework to describe both the intrinsic motion of partons and their transverse 
spatial structure in nuclei~\cite{Liuti:2005qj,Rezaeian-Pirner-2006,
Zhen-min-He-1998,Accardi:2005jd,Accardi:2005hk,Kirchner:2003wt}.  
valuable information can be obtained from the measurement of the nuclear 
Generalized Parton Distributions (GPDs) representing the soft matrix elements 
for these processes. The GPDs correspond to the coherence between quantum 
states of different (or same) helicity, longitudinal momentum, and transverse 
position. In an impact parameter space, they can be interpreted as a 
distribution in the transverse plane of partons carrying a certain longitudinal 
momentum~\cite{Burkardt-2000,Diehl-2002,Belitsky-2002}. A crucial feature of 
GPDs is the access to the transverse position of partons which, combined with 
their longitudinal momentum, leads to the total angular momentum of 
partons~\cite{Burkardt-2005}. This information is not accessible to inclusive 
DIS which measures probability amplitudes in the longitudinal plane. \\

A high luminosity facility such as Jefferson Lab offers a unique opportunity to 
map out the three-dimensional quark and gluon structure of nucleons and nuclei.  
While most of submitted proposals to JLab Program Advisory Committee (PAC) have 
focused on the studies of the 3D nucleon structure considered as one of the 
main motivations for the JLab 12 GeV upgrade, we propose here to extend the 
measurements to light nuclei. While this proposal focuses on $^4$He nucleus, we 
also plan to measure few deuteron GPDs\footnote{See the 4$^{th}$ proposal of 
the ALERT run group which summarizes additional measurements we plan to perform 
with no additional beam time.}. Pioneering measurements of exclusive coherent 
DVCS off $^4$He have been successfully conducted during the JLab 6 GeV era 
(E08-024) using the CLAS detector enhanced with the radial time projection 
chamber (RTPC) for the detection of low energy recoils and the inner 
calorimeter for the detection of forward high energy photons. However, the 
experiment covered only limited kinematic range and the results were dominated 
by statistical uncertainties ~\cite{eg6_note}. \\

We propose a new measurement of hard exclusive DVCS and deeply virtual $\phi$ 
production off $^4$He nuclei. The focus of this proposal is on the coherent 
DVCS (DVMP) channel where the scattered electron, the produced photon (the 
$\phi$ meson) and the recoil $^4$He are all detected in the final state. We 
propose to use CLAS12 because of its large acceptance. In addition to the 
coherent DVCS and DVMP off $^4$He, the CLAS12-ALERT setup will allow us to mine 
the data collected in this experiment for other final states as well, such as 
the $\pi^0$, $\rho$ and $\omega$ mesons and other reaction channels described 
in the accompanying proposals of the ALERT run group$^1$.  The novelty of the 
proposed measurements is the use of a new low energy recoil tracker (ALERT) in 
addition to CLAS12.  The ALERT detector is composed of two types of fast 
detectors: a stereo drift chamber for track reconstruction and an array of 
scintillators for particle identification. ALERT will be included in the 
trigger for efficient background rejection, while keeping the material budget 
as low as possible to detect low energy particles.  This was not possible with 
the previous GEM based RTPC due to the long drift time.

