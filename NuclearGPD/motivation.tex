\chapter{Physics Motivations}
% \epigraphsize{\small}% Default
%\setlength\epigraphwidth{8cm}
%\setlength\epigraphrule{0pt}


A wealth of information on the QCD structure of hadrons lies in the 
correlations between the momentum and spatial degrees of freedom of the 
constituent partons. Such correlations are accessible via GPDs which, more 
specifically, describe the longitudinal momentum distribution of a parton 
located at any given position in the plane transverse to the longitudinal 
momentum of the fast moving nucleon. Various GPDs extracted from measurements 
of hard exclusive reactions with various probe helicities and target spin 
configurations are necessary to identify this subset of the hadronic 
phase-space distribution, known as the Wigner distribution. The processes 
which are most directly related to GPDs are DVCS and DVMP corresponding to the 
exclusive electroproduction of a real photon or a meson in the final state 
respectively, see Figure~\ref{fig:handbag_phi}.\\

The number of GPDs needed to parametrize the partonic structure of a nucleus 
depends on the different configurations between the spin of the nucleus and the 
helicity direction of the struck quark. For example, for a target of spin $s$, 
the number of chiral-even GPDs is equal to ($2s+1$)$^2$ for each quark flavor.  
DVCS off spin 0 nuclear targets, such as $^4$He, is simpler to study since only 
one chiral-even GPD, $H_{A}$, is present at leading twist.\\

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.99\linewidth]{fig_2017/DVCS_DVMP_handbag_1.pdf}
\caption{Left figure: DVCS process in the handbag approximation. Right figure: 
DVMP diagram at the lowest order, dominated by two-gluon exchange.}
\label{fig:handbag_phi}
\end{center}
\end{figure}

The $^4$He nucleus is a well studied few-body system in standard nuclear 
physics. It is characterized by a strong binding energy and relatively high 
nuclear core density similar to some more complex nuclei. Inclusive scattering 
off $^4$He shows a large EMC effect. By measuring quark and gluon GPDs in nuclei, one also 
accesses transverse spatial degrees of freedom, by which one can infer space 
dependent nuclear modifications directly from data.  %In Figure 
%\ref{fig:quarks_nucleus}, the Fourier transform of the nucleon GPDs over the 
%transverse component of the momentum transfer $\Delta$ gives the transverse 
%separation ($b'$) between quarks in the nucleon, while the Fourier transform of 
%the nuclear GPD gives the transverse separation ($b$) between quarks in the 
%nucleus. Knowing these two separations, one can access the transverse 
%separation ($\beta = b - b'$) between the center of momenta of nucleons in a 
%nucleus \cite{Liuti:2005qj}.  This attractive possibility would enable us to 
%obtain quantitative information on {\it{e. g.}} the confinement size of bound 
%nucleons, as well as the transverse overlap areas of hadronic configurations 
%inside the nucleus. 

\section{DVCS Measurement}
\label{chap:physics}
%

The $^4$He nucleus provides a textbook case for DVCS 
measurements since it has only one chiral-even GPD. Therefore, by measuring 
coherent exclusive DVCS, one can, in a model independent way at leading twist, 
access the single Compton form factor and subsequently extract the transverse 
spatial distribution of quarks in the fast moving nucleus. It is also
an ideal target to isolate higher twist effects as is proposed in the
4$^{th}$ proposal of the ALERT run group.\\

%%%%%%%%%%%%
%\begin{figure}[tbp]
%\centering
%\includegraphics[width=0.40\linewidth]{fig_NuclGPD/quarks_nucleus2.pdf}
%\vspace{-0.7in}
%\caption{The spatial coordinates of quarks in a nucleus. See main text for 
%definition of the variables.}
%\label{fig:quarks_nucleus}
%\end{figure}
%%%%%%%%%%%%%

The DVCS process off nuclear targets differs from single proton scattering in 
that it can occur via either the coherent or incoherent channels. In this 
proposal, we will consider the coherent channel where the target nucleus 
remains intact and recoils as a whole while emitting a real photon ($eA 
\rightarrow e' A' \gamma$). This process allows one to measure the nuclear 
GPDs, which contain information on the parton correlations and the nuclear 
forces in the target \cite{Liuti:2005qj,Polyakov:2002yz}.
%
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.60\linewidth]{fig_NuclGPD/4he_dvcs_prop.pdf}
\caption{Theoretical expectations for Off-forward EMC effect in $^4$He.  
   Predictions at $t=0.1$ GeV$^2$ from both ``conventional'' binding models and 
   within a diquark picture for nuclear modifications are shown. For 
comparison, the effect at $t=0$ is given by experimental data and theory (blue 
curves) on the ratio of inelastic structure functions \cite{Gomez:1993ri} 
(adapted from Ref.~\cite{Liuti:2005qj}).}
\label{Mot_fig0}
\end{center}
\end{figure}
%
We propose to measure coherent DVCS Beam Spin Asymmetries (BSA) in order to 
extract in a model independent way both the real and imaginary parts of the 
$^4$He nuclear Compton form factor $H_A$. This will lead the way toward the 
determination of the nucleus 3D picture in terms of its basic degrees of 
freedom, namely valence quarks in this case. In addition, the comparison 
between the coherent nuclear BSA and the free proton ones will allow us to 
study a variety of nuclear medium effects, such as the modification of quark 
confinement size in the nuclear medium. In fact, configuration size 
modifications have been advocated as responsible for the behavior of the EMC 
ratio in the intermediate $x_B$ region 
\cite{Close1983,Nachtmann1984,Jaffe1984,Close1988}. The generalized EMC effect 
{\it i.e.} the modification of the nuclear GPDs with respect to the free 
nucleon ones, normalized to their respective form factors was studied in 
Refs.~\cite{Liuti:2005gi,Guzey:2003jh,Guzey:2005ba,Scopetta:2004kj}.  
Measurements in the intermediate $x_B$ range between 0.1 and 0.6, and for an 
appropriate $t$-range are crucial for both establishing the role of partonic 
configuration sizes in nuclei, and for discerning among the several competing 
explanations of the EMC effect. As shown in Ref.~\cite{Liuti:2005gi}, the role 
of partonic transverse degrees of freedom, both in momentum and coordinate 
space, could be important in the generalized EMC effect, thus predicting an 
enhancement of signals of nuclear effects with respect to the forward case 
(Figure~\ref{Mot_fig0}).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Deep Virtual $\phi$ Production Measurement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
As we mentioned in the previous section, over the last two decades, there has 
been an increasing interest in multidimensional imaging of the structure of the 
nucleon. Through measurements of exclusive processes off 
the nucleon, information on Compton form factors and subsequently GPDs has 
been obtained successfully. Through these GPDs, the transverse parton 
density of the nucleon in the infinite momentum frame is obtained by a Fourier 
transform of the momentum transfer dependence leading to a transverse spatial 
parton density description of the nucleon.\\

%	
In contrast with DVCS, which is sensitive to the charge distribution, exclusive 
$\phi$ production provides an access to the gluon GPDs. The leading order
diagrams of these processes, shown in Figure~\ref{fig:handbag_phi},
illustrate nicely this feature. In the DVCS case, the scattering is 
facilitated through quark exchange, while in the exclusive $\phi$ production, 
the mostly strange $\phi$, interacts with the mostly up-down nucleus 
via a two-gluon exchange. \\

%
A recently approved proposal using the CLAS12 detector, E12-12-007
\cite{Girod:2012PR}, aims to extract the transverse gluon distribution of the 
proton and its gluonic size.  In analogy, this proposal uses a very similar 
framework to the one discussed in E12-12-007 but focuses on the gluon GPD for 
a tightly bound spin zero nucleus, namely $^4$He, thus extending the 
investigation of quark GPDs in a nucleus to the case of gluons.  
An experiment to extract gluon distributions on heavy nuclei through coherent 
$\phi$ electroproduction at the EIC has already been proposed in the most 
recent EIC white paper \cite{Accardi:2012qut}. JLab 12 GeV can start 
such an investigation at large $x$ initiating a full three-dimensional 
partonic structure investigation of a nucleus for the first time.\\

%       
Gluons are the salient partners of the quarks in a nucleon as well as in a 
nucleus. We know they are responsible for the confinement of quarks and for 
their own confinement, and represent a large fraction of the energy or the mass 
of the nucleon.  However, gluons are charge neutral and cannot be probed 
directly using the electromagnetic probe. For example we know the charge 
distribution of $^4$He and how to interpret it through the charge of nucleons.  
For instance, the diffraction minima in the measured charge distribution of 
$^4$He tell us that nucleons are the appropriate degrees of freedom to 
consider when describing the electromagnetic properties of a nucleus. In fact 
we have yet to see unambiguously the elusive signature of quarks in elastic 
scattering off a nucleus even though we know they must be there as the building 
blocks of nucleons. Similarly, we do not know where the gluons are distributed 
in the nucleon and how they participate in the long and short-range 
nucleon-nucleon correlations that are responsible for the structure of the 
nucleus. One can ask whether in a nucleus the gluons are localized in the 
confined volume defined by the nucleons or spread beyond the size of the 
nucleons. Considering only the gluonic matter in a nucleus, a natural question 
arises, is a nucleus the sum of localized gluon density corresponding to that 
of free nucleons, or else? It would be of paramount importance to test our 
naive understanding of the charge neutral gluonic matter when we discuss the 
size of the nucleon and that of the nucleus. Among the interesting questions 
one might ask is whether there is evidence that the gluon transverse spatial 
distribution is homogeneous, or does it appear to be affected by the location of 
the bound nucleons? In the same spirit of the discussion of quarks which was 
carried in the previous section, the discussion of the gluons is at least as 
relevant to our understanding of nuclei from basic principles.\\

%
Measuring the gluon distributions in the nucleon is an important step and will 
be carried  by the approved experiment E12-12-007 \cite{Girod:2012PR}.  
Understanding how these distributions are modified to provide the binding and 
structure in a nucleus is as fascinating of a question and an integral part of 
our quest of using QCD to explore nuclear matter. The future Electron Ion 
Collider will have the tools to address these questions using heavier mesons 
like $J/\Psi$ and $\Upsilon$. At JLab 12 GeV we can use the lighter vector 
mesons, namely the $\phi$, to initiate this physics program and provide a 
glimpse into the salient features of nuclear matter.
%

