ALERT Run Group Proposals
=========================

Todo
----

* What is the **Official beam current limitation** in Hall B?

Nathan followed up with Stepan and there is a limit on the "Beam Blocker" which 
is an attenuator in front of the Faraday cup. Not sure if it is electrically 
coupled to the Faraday cup or just an attenuator. The beam blocker is simply a 
slab of copper(?) that absorbs a significant part of the beam.  The Faraday cup 
has a power limit of 150 W above which it becomes damaged (right?).
This power (150 W) corresponds to a current limit of roughly 14 nA **without 
the beam blocker**.

The current beam block has a power limit of 5 kW which corresponds to ~ 454 nA.
We will therefore need an 11kW "Beam Blocker". I assume the 5 kW reference has 
a significant safety margin engineered into it so we could probably use it and 
not worry.

We should try to find any documentation for the Hall B beam dump/Faraday 
cup/Beam blocker. 

If the Hall radiaiation limits are not exceeded (assuming a factor of 2 
increase is not a problem), do we see any other limitations to the beam 
current? 



Compiling
---------


Directories
-----------

```
.
├── Detector
│   └── fig-chap2
├── ExtraTopics
│   └── figs
├── NuclearGPD
│   ├── fig
│   └── fig_NuclGPD
├── TaggedDVCS
│   ├── abstract
│   ├── fastmc
│   ├── fastmc2
│   │   ├── tagged_dvcs_He4_neutron_2
│   │   └── tagged_dvcs_He4_proton_2
│   └── figs
├── TaggedEMC
│   ├── fig-chap1
│   ├── fig-chap3
│   └── fig-intro
└── tikz
    ├── dvcs_feynman
    └── extra_diagrams
```

