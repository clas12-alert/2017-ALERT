\chapter*{Introduction\markboth{\bf Introduction}{}}
\label{chap:intro}
\addcontentsline{toc}{chapter}{Introduction}
%
Inclusive electron scattering is a simple and yet a powerful tool to probe the 
structure of the nucleus; in the Deep Inelastic Scattering (DIS) regime it 
allows to access the partonic structure of hadrons. Using the nucleus as a target 
permits to study how nucleons and their parton distributions are modified when 
embedded in the nuclear environment. The modification of quark distributions 
in bound nucleons was first observed through the modification of the 
per-nucleon cross section in nuclei, known as the "EMC effect"~\cite{Aubert:1983xm}.
For moderate Bjorken~$x$, 0.35 $\leqslant$ $x_B$ $\leqslant$ 0.7, the 
per-nucleon DIS structure function for nuclei with A $\geqslant$ 3 was found 
to be suppressed compared to that of deuterium, the historic measurement being
confirmed and refined in the past 30 years~\cite{Ashman:1988bf, Arneodo:1988aa, 
Arneodo:1989sy, Gomez:1993ri, Allasia:1990nt, Seely2009}. \\

Since its discovery, the EMC effect has been a subject of extensive theoretical 
investigations aimed at understanding its underlying physics. While progress 
has been made in interpreting the main features of the effect, no single model 
has been able to explain convincingly the effect for both its $x_B$ and $A$ dependencies~%
\cite{Geesaman1995, Norton2003, Malace:2014uea}. A unifying understanding of 
the physical picture is still under intense debate. Most models of the EMC 
effect can be classified into two main categories:
%
\begin{itemize}
 \item ``Conventional'' nuclear models \cite{Ericson1983,Dunne1985,
Akulinichev1985,Jung1988} in which the effect could be understood by a reduced 
effective nucleon mass due to the nuclear binding, causing a shift of $x_B$ to 
higher values ($x_B$-rescaling or binding models). In these models the mass 
shift is sometimes accompanied by an increased density of virtual pions 
associated with the nuclear force (pion cloud models). 
%
 \item Models involving the change of the quark confinement size in the 
nuclear medium \cite{Close1983,Nachtmann1984,Jaffe1984,Close1988} can be 
viewed, in the language of QCD, as Q$^2$ rescaling models. In some cases, a 
simple increase of the nucleon radius is assumed (nucleon swelling), while in 
others, quark deconfinement is invoked and the nucleon degrees of freedom are 
replaced by multi-quark clusters. 
%
 \item Some more elaborate models fall in between or give very different 
predictions. We note here in particular the Point Like Configurations (PLC) 
suppression model as it gives direct predictions as a function of the nucleon
off-shellness. It was argued  
in~\cite{Frankfurt:1985cv} that PLCs are suppressed in 
bound nucleons and that large $x_B$ configurations in nucleons have smaller than 
average size leading to the EMC effect at large $x_B$. The EMC effect in this 
model is predicted to be proportional to the off-shellness of the struck nucleon 
and hence dominated by the contribution of the short-range correlations.
\end{itemize}

\begin{figure}[tbp]
  \begin{center}
    \includegraphics[angle=0, width=0.6\textwidth]{./fig-intro/seely_ratio}
    \caption{The slope of the isoscalar EMC ratio ({\it i.e.} the EMC ratio
    corrected for isospin asymmetry) for $0.35<x_B<0.7$ as a 
    function of nuclear density~\cite{Seely2009}.}
    \label{fig:ratio_hallc}
  \end{center}
\end{figure}

Recent experiment at Jefferson Lab measured the EMC effect for a series of 
light nuclei~\cite{Seely2009} changing significantly our understanding of
the EMC effect. The $^3$He EMC ratio was found to be 
roughly one third of the effect observed in $^4$He, violating the 
$A$-dependent fit to the SLAC data. Similarly, the density-dependent description
of the EMC effect has been contradicted by the large EMC effect 
found in $^9$Be (Figure~%
\ref{fig:ratio_hallc}). This suggests that the EMC effect may be sensitive to 
the local density experienced by a given nucleon or details of the nuclear 
structure, which has been first
suggested in~\cite{Frankfurt:1981mk}. Other models have also 
predicted a local EMC effect and describe the modification of the nucleons 
depending on their shells~\cite{Kumano1990,ciofiliuti1991,CiofidegliAtti1999}. 
The possibility of the EMC effect depending on the local environment of the 
nucleon also motivates the investigation of possible connections between the 
EMC effect and other density-dependent effects such as nucleon short-range 
correlations~\cite{Higinbotham:2010ye, Weinstein:2010rt}. \\

\begin{figure}[tb]
  \begin{center}
    \includegraphics[angle=0, width=0.5\textwidth]{./fig-intro/EMC-SRC}
    \caption{The EMC slopes versus the SRC scale factors 
    from~\cite{Geesaman:2015fha}. The uncertainties 
    include both statistical and systematic errors added in quadrature.}
    \label{fig:src-emc}
  \end{center}
\end{figure}

Short-range correlations (SRC) occur between nucleons located at less than the 
average inter-nucleon distance with high relative momentum~%
\cite{Frankfurt:1993sp, Egiyan:2003vg, Egiyan:2005hs}. Those pairs of nucleons 
carry 80\% of all nucleons kinetic energy inside the nucleus although they only 
represent about 20\% of the total number of nucleons~\cite{Hen:2014nza}. The 
plateau obtained in the inclusive 
cross section ratios of two nuclei (for example iron and deuterium) in the 
region $x_B > 1.5$ for $Q^2 > 1.5$ GeV$^2$ indicates that for nucleon momentum 
larger than Fermi momentum ($p\geqslant p_{N} \simeq$ 275 MeV/c), the nucleon 
momentum distributions in different nuclei have similar shapes but differ in 
magnitude. The ratio of the cross sections in the plateau region also called 
the "SRC scale factor $a_{2N}(A/d)$" was found to be linearly correlated with 
the slope of the EMC effect~\cite{Weinstein:2010rt}. This striking correlation 
shown in Figure~\ref{fig:src-emc} could indicate that high momentum bound 
nucleons are important players in the EMC effect. \\

To investigate the EMC effect further, we need to find ways of looking into the nuclei
with a sensitivity to the local density experienced by the nucleon we probe.  
This is possible by measuring the recoil fragment of the nucleus in addition 
to the scattered electron. Similarly to the SRC, the 
momentum of the recoil gives indications on the inter nucleon distance at 
the time of the interaction. Moreover, the momentum of this recoil can be linked to
the off-shellness of the interacting nucleon~\cite{CiofidegliAtti:2007vx} opening
new avenues to understand how the EMC effect arise in the nucleus. The two main challenges 
to perform such a measurement are, first, to be
able to detect the low energy nuclear fragments and, second, to understand the
Final State Interaction (FSI) effects on the observables. \\

The recent development of two small radial time projection chambers (RTPC) by
the CLAS collaboration for the measurement of the structure function of the 
neutron, by tagging the spectator proton from a deuterium target~\cite{Baillie:2011za},
and the measurement of coherent deep virtual Compton scattering off $^4$He~\cite{eg6_note}
has raised a lot of interest to use the same detector for this proposed tagged EMC 
measurement. However, it was found that the particle identification 
capabilities of the RTPC are not good enough to properly distinguish the 
different nuclear isotopes measured (in particular $^3$H from $^3$He). 
Therefore, we propose to use a different detector (a Low Energy Recoil 
Tracker - ALERT) based on a low gain drift chamber and a scintillator array for time
of flight measurements. Such a detector appears to be perfectly suited for our 
measurement as it offers low energy and large angle capabilities to measure slow recoils
similar to the RTPC. Moreover, such a detector will be much faster to collect the deposited charges
making it possible to include it in the trigger. This will allow to ignore 
the overwhelming majority of the events ($\sim90$\%) where the nuclear recoil
does not make it into the detection area and reduce the pressure on the DAQ. \\

Another important challenge for tagged measurements is to control the impact of
FSI on the observables. The large acceptance of both CLAS12 and ALERT is very
important in this regard as it allows to measure at the same time the regions
of the phase space expected to have negligible FSI and the regions where we expect a larger FSI effect.
The models used to correct for FSI~\cite{CiofidegliAtti2003,ciofi2004,
Alvioli:2006jd,Palli2009} can therefore be tested in
a wide kinematic range in the exact same conditions as the main measurement   
in order to ensure that the effect is understood properly. Then with the 
application of cuts to select the region where the effect is small, we can 
reduce the impact of FSI to a minimum. This
procedure allows to make sure FSI effects are small and under control to
minimize systematic uncertainty on our observables.

