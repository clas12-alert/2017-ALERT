\chapter{Proposed Measurements}
\label{chap:reach}
In light of the physics motivation presented and the capabilities 
of the new ALERT detector, we propose to measure the tagged 
deep-inelastic scattering off $^2$H and $^4$He and for a range of 
the recoiling spectator momenta 
$P_{A-1}$ from 70 to 400 MeV/$c$. We choose the helium target
for several reasons, first it is a light gas that can easily be used in
a very light gaseous target allowing to detect very low momentum spectators. 
Also, calculation for FSI are theoretically very challenging, keeping the
number of nucleon low is therefore of great help; moreover, as spectators
get heavier, their detection threshold increases, which explains why we want to use  
low $A$ target. The reactions we are going to study are:
\begin{itemize}
\item $^2$H$(e, e' p)X$ --- bound neutron;
%\item $^3$He$(e, e' d)X$ --- bound proton;
\item $^4$He$(e, e'~^3\mathrm{H})X$ --- bound proton;
\item $^4$He$(e, e'~^3\mathrm{He})X$ --- bound neutron;
\end{itemize}
%%%%%%%%%%%%%%%%%%% 
\subsection{Monte-Carlo Simulation}
%%%%%%%%%%%%%%%%%%%
To estimate the rates of our experiment and provide meaningful estimates of our
statistical error bars, we developed a Monte-Carlo simulation based on PYTHIA 
to which we added nuclear Fermi motion effects. The interaction on the nucleon is generated 
in a basic impulse approximation, neglecting the off-shellness of the target 
nucleon. We simulate the Fermi motion of the nucleons in the target nuclei 
according to the distribution provided by AV18+UIX potentials~%
\cite{Wiringa1995,Pudliner1997,Wiringa}. This leads to a target nucleon with 
momentum $\vec p_n$ and a nuclear spectator generated with a kinematic 
opposite to the interacting nucleon, -$\vec p_n$. The PYTHIA Monte-Carlo 
provides simulation for the DIS interaction and the fragmentation of the 
partons, we do not include nuclear effects such as FSI here. This should
not be an issue for our estimate as our key measurements are focused on
the parts of the phase space where the FSI are small.\\

In the simulation, we select DIS by requesting $Q^2 > 1.5$~GeV$^2$ and 
$W > 2$~GeV. These are the same for all figures, indication on the figures 
are for the theoretical predictions. In our experimental configuration and with 
the cut described above, we expect $\langle Q^2 \rangle \sim 3$~GeV$^2$.\\

The generated final-state particles undergo acceptance tests. Electrons, 
which will be detected by the forward detector, are treated by a GEANT4 
Monte-Carlo simulation of CLAS12. The recoiling nuclei (including protons) 
acceptance is based on the GEANT4 simulation described in section~\ref{sec:sim} 
and represented in Figure~\ref{fig:acceptance}. On top of these estimates, 
we apply an overall 75\% efficiency to this detection settings to account for 
the fiducial cuts and detector inefficiencies.
%%%%%%%%%%%%%%%%%
\subsection{Beam Time Request}
%%%%%%%%%%%%%%%%%
We estimate, based on past measurements with CLAS~\cite{Dupre:2011afa}, that 
the ratios we want to measure will be affected by systematic errors of $\sim3$ 
percents. The dominant factor being associated to acceptance corrections. Our beam time request, 
allows to have the statistical error bars of our key measurement 
(Figure~\ref{fig:ratio_a_proj} right) comparable to the systematic ones. Assuming the luminosity
of $3.10^{34}$~cm$^{-2}$s$^{-1}$, the beam time request for proposed measurements is of   
20 days for each target.
%%%%%%%%%%%
\section{Projections}
%%%%%%%%%%
\begin{figure}[tbp]
  \begin{center}
    \includegraphics[angle=0, width=0.65\textwidth]{./fig-chap3/MomVsX-H3}
    \caption{Expected event count as a function of $x_B$ and the recoil 
             momentum of the $^3$H from a $^4$He target.}
    \label{fig:xb_vs_p}
  \end{center}
\end{figure}
%
Based on our simulation, we determine the available kinematic range and 
production rates accessible for each channel. The $x_B$ and recoil momenta 
distributions are illustrated in Figure~\ref{fig:xb_vs_p} for tagged $^3H$ out 
of an $^4$He target. This figure shows the available phase space for a 
measurement of the bound proton structure function. 
%%%%%%%%%%%%%%%%%%%%%
\subsection{Testing the Spectator Model}
%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[tbp]
  \begin{center}
    \includegraphics[angle=0, width=0.56\textwidth]{./fig-chap3/Ratio_He4}
    \caption{This figure is similar to Figure~\ref{fig:ratio_spec}, it shows 
the predictions for the ratio $R(A,A',|\vec P_{A-1}|)$ for CLAS12 
kinematic~\cite{CiofidegliAtti1999,CiofidegliAtti2012} 
compared to our projected statistical 
error bars (blue points).}
    \label{fig:ratio_spec_proj}
  \end{center}
\end{figure}

The projections presented in Figure~\ref{fig:ratio_spec_proj} show the capability 
of this experiment to measure cross section ratios of DIS on a bound nucleon in 
light nuclei and, therefore, our capability to check the validity of the 
spectator model used by the theoretical predictions. Statistical error bars in 
this figure are very modest and even smaller than the points on the logarithmic
scale. It is therefore clear that we have capability to test the spectator model 
in more details with multi-dimensional binning as was done in~\cite{Cosyn:2010ux} 
and for the first time perform similar studies on helium. However, we do not have 
specific model predictions for such measurement to compare with at the moment. \\ 
Other tests have been proposed to insure that outgoing pions are formed far
enough from the nuclei to limit FSI~\cite{Egiian:1994ey,Frankfurt:1994kt} and
understand the color transparency effect associated. This study is most 
sensitive to spectators emitted at 90$^\circ$, where the effect is larger,
and can then be extrapolated to lower angles. In Ref.~\cite{Cosyn:2016oiq}, testing 
that the $x_B$ scaling holds in tagged DIS is proposed as another way to confirm
the soundness of the method.\\

As shown above, the high statistics of the experiment will open the 
possibility to make several tests of the spectator mechanism and its limits.
In particular it will, for the first time, experimentally explore this
question for helium, opening the way for light nuclei tagged experiments.
%%%%%%%%%%%%%%%%%%%
\subsection{EMC effect in deuterium}
%%%%%%%%%%%%%%%%%%%
\begin{figure}[tbp]
  \begin{center}
    \includegraphics[angle=0, width=0.56\textwidth]{./fig-chap3/plotMel2}
    \caption{This figure is similar to Figure~\ref{fig:mel}, it shows 
the predictions from~\cite{Melnitchouk1997} 
of the ratio $F_{2p}^{bound}/F_{2p}$ compared to projected statistical 
error bars for the proposed experiment (blue points). Dashed line is a 
prediction for the PLC suppression model, dotted 
is for the $Q^2$-rescaling model, and dot-dashed for the binding/off-shell model.}
    \label{fig:resultmel}
  \end{center}
\end{figure}

As explained in the first chapter, it is possible to enhance the EMC effect in
the deuteron by selecting the highly off-shell nucleons. 
This prediction can be directly tested with our proposed experiment, as shown in
Figure~\ref{fig:resultmel} where we compare our measurement capabilities to Melnitchouk 
{\it et al.}~\cite{Melnitchouk1997} predictions for rescaling models as well as
the PLC suppression model. Note
than in our case, the proton being tagged, we are actually measuring 
$F_{2n}^{bound}/F_{2n}$, but this measurement can be interpreted similarly to
the proton case in regard to the EMC effect. The main limitation in this 
channel is the very fast decrease of the cross section for high $\alpha$ in 
deuterium.\\ 

We point out that such a measurement is also possible with the helium target, 
for either the proton or the neutron. This would allow to reach much higher
$alpha$ without running a prohibitively long experiment. We do not present 
these projections here because, at the moment, there is no theoretical 
predictions for these channels. The main reason 
is the difficulty to extend the calculations to the helium four-body system.
However, we have indications that these kind of studies are on-going in the 
theory community~\cite{Scopetta2016}.
%%%%%%%%%%%%%%%%%%%%%
\subsection{Testing the Rescaling Models}
%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[tbp]
  \begin{center}
    \includegraphics[angle=0, width=0.46\textwidth]{./fig-chap3/Fig5New_90}
    \includegraphics[angle=0, width=0.455\textwidth]{./fig-chap3/Fig5New_180}
    \caption{This figure is similar to figure \ref{fig:ratio_a}, it shows 
predictions of the ratio $R^A(x,x')$ for $A = 2$ and $A = 4$ as a function of 
the momentum of the recoil nucleus $A-1$ at perpendicular (left) and backward 
(right) angle. The full and dashed curves are predictions for CLAS12 kinematic~%
\cite{CiofidegliAtti1999,CiofidegliAtti2012} of the $x$-rescaling (binding) 
and $Q^2$-rescaling models, respectively, points are projections for $^2$H (red) 
and $^4$He (blue).}
    \label{fig:ratio_a_proj}
  \end{center}
\end{figure}

The main goal of our experiment is to discriminate decisively between models 
of EMC, Figure~\ref{fig:ratio_a_proj} illustrates this capability. We have here a 
high differentiation power between $x$-rescaling and Q$^2$-rescaling models. We 
note the good coverage and small error bars for $\theta_{P_{A-1}} = 90^\circ$ 
($75 < \theta_{P_{A-1}} <105^\circ$). This is due to the better acceptance for 
this angle. The measurement at backward angle ($\theta_{P_{A-1}} > 150^\circ$), 
however, is much more difficult and is the main constraint driving our beam 
time request. Still, in order to obtain our planned precision with a reasonable 
beam time request, the backward angles are selected from 150$^\circ$ and up instead of 
the 160$^\circ$ which is used for the theory predictions.\\

We notice the complementarity of our choice of targets in the phase space covered, this
is due to the fact that larger recoil nuclei are more absorbed by the target
material and have higher detection threshold. At the same time, the Fermi
momentum is larger in helium allowing better statistics at high $p_{A-1}$. Using
helium is then also an opportunity to explore higher spectator momentum with a 
reasonable beam time request.
%%%%%%%%%%%%%%%%
\subsection{Tagged EMC Ratio}
%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
    \includegraphics[angle=0, width=0.7\textwidth]{./fig-chap3/Fig10_new}
    \caption{This figure is similar to figure \ref{fig:r0}, it shows the 
semi-inclusive EMC ratio $R_0(x,Q^2)$ as a function of $x_B$ with recoils 
emitted forward and backward, the dashed and dotted curves are predictions for 
the local EMC effect for CLAS12 kinematic~\cite{CiofidegliAtti1999,CiofidegliAtti2012} 
the blue points are statistical error bar projections for our measurement.}
    \label{fig:fig:r0_proj}
  \end{center}
\end{figure}

The experiment can also confront the striking predictions for backward versus 
forward tagged EMC in binding models, as illustrated in the 
Figure~\ref{fig:fig:r0_proj}. We see that the model prediction will be clearly 
tested, however the reach in $x_B$ for the backward recoils is also
strongly constrain by the beam time available for the experiment. Indeed, the 
strongest effect is expected at $x_B \sim 0.5$ for which we need high statistics.\\

The measurement of the tagged EMC ratio is a very good observable even for other
kinds of model, in the low momentum regime one should be able to reproduce very 
nicely the classic EMC effect and then be able to study its dependence to the 
spectator angle and momentum. In general, models based only on off-shellness 
predict no differences between nuclei at a given spectator kinematic. This prediction
can be tested nicely with the measurement presented here.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The Flavor Dependent Nuclear Effects}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \begin{center}
    \includegraphics[angle=0, width=0.7\textwidth]{./fig-chap3/flavor}
    \caption{Statistical error bar projections for the ratio $F_2^n/F_2^p$ for 
bound nucleons as a function of $x_{Bj}$ using an $^4$He target.}
    \label{fig:flavor_proj}
  \end{center}
\end{figure}

Finally with our experimental setup, we will explore the flavor dependence of nuclear PDFs. 
Figure~\ref{fig:flavor_proj} illustrates our capabilities, for $^4$He the isovector 
model predicts a ratio of 1~\cite{Cloet2009}, but others 
predict that nuclear effects change the d/u ratio and should therefore be 
observed here~\cite{Brodsky:2004qa}. We will be able to explore any variation 
in bound nucleons with 1 to 2\% statistical error bars -- {\it i.e.} 4\% when including
expected systematic error bars -- from $x_B$ of $0.1$ to $0.5$.


