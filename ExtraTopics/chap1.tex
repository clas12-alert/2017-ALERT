\chapter{Summary of Physics Topics}
\label{chap:physics}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Helium GPDs beyond the DVCS at leading order and leading twist}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A wealth of information on the QCD structure of hadrons lies in the 
correlations between the momentum and spatial degrees of freedom of the 
constituent partons. Such correlations are accessible via GPDs which, 
more specifically, describe the longitudinal momentum distribution of a parton 
located at a given position in the transverse plane. The processes which are 
most directly related to GPDs are DVCS and DVMP corresponding to the exclusive 
electroproduction of a real photon or a meson, respectively.\\

GPDs provide essential information for determining the missing component to 
the nucleon {\em longitudinal} spin sum rule in the kinematic setting where 
the proton moves in the $z$ direction, its spin being polarized along the same 
direction. A complete description of nucleon structure requires, however, 
understanding also the {\em transverse} spin (\cite{Ji:2012vj} and references 
therein). An experimental determination of the chiral-odd quark helicity-flip 
GPDs, $H_T(x,\xi,t), E_T(x,\xi,t), \tilde{H}_T(x,\xi,t)$, and 
$\tilde{E}_T(x,\xi,t)$~\cite{Diehl:2001pm} is crucial for this effort.\\

In Ref.\cite{Ahmad:2008hp}, it was suggested that deeply virtual exclusive 
pseudoscalar electroproduction can provide a direct channel to measure 
chiral-odd GPDs so long as the helicity flip  contribution to the quark-pion 
vertex is dominant. This idea was subsequently endorsed and further developed 
in Refs.\cite{Goloskokov:2011rd,Goldstein:2012az,Goldstein:2013gra,
Kroll:2016aop,Boussarie:2016aoq}. Recently, experimental measurements from 
Jefferson Lab have been interpreted in terms of chiral odd GPDs~%
\cite{Bedlinskiy:2014tvi,Kim:2015pkf,Favart:2015umi}. In particular, by 
measuring separately $\pi^o$ and $\eta$ production~\cite{Kim:2015pkf}, it is 
possible to perform a flavor separated analysis of the chiral odd GPDs. As 
pointed out in \cite{Goldstein:2013gra,Goldstein:2014aja}, however, in 
principle all four chiral odd GPDs enter the description of the various terms 
in the nucleon cross section. It is therefore a tantalizing job to disentangle 
their various contributions.\\ 

The number of GPDs needed to parametrize the partonic structure of a nucleus 
depends on the different configurations between the spin of the nucleus and 
the helicity direction of the struck quark. For example, for a target of spin 
$s$, the number of chiral-even GPDs is equal to ($2s+1$)$^2$ for each quark 
flavor. DVCS off spinless nuclear targets, such as $^4$He, $^{12}$C and 
$^{16}$O, is simpler to study since only a chiral-even GPD, $H_{A}$, and a 
chiral-odd GPD, $H_{A}^T$, are present at leading twist and two chiral even 
GPDs, $H_{A}^{(3)}$, and $\tilde{H}_{A}^{(3)}$, arise at twist three. This
limited number of GPDs make it much simpler to interpret data from observables
linked to these extra GPDs using a spin less target.\\

The $^4$He nucleus, characterized by its high density can be considered as the 
smallest of complex nuclei. Moreover, inclusive scattering off $^4$He shows a 
large EMC effect. Therefore, this nucleus represents an ideal target for 
understanding a variety of nuclear effects through GPDs measurements. The twist 
three GPDs, $H_A^{(3)}$ and $\tilde{H}_A^{(3)}$, allow us to access the so far 
unexplored spin correlation of a longitudinally polarized quark in an 
unpolarized target. This, in turn, would give us unique information on the 
quark spin-orbit interaction, $\vec S \cdot \vec L$, in a nuclear environment~%
\cite{Lorce:2011kd}. We recall, again, that for a spin zero target the twist 
three GPDs are much easier to disentangle than in a nucleon, since the 
description of the latter requires a larger number of functions. The chiral-odd 
GPD, $H_A^T$, measured through deeply virtual $\pi^0$ production (background to 
DVCS signal) can be expressed in the low $\xi$ approximation, as a convolution 
over the transversity GPD, $H_T$. The latter becomes the transversity PDF, 
$h_1$, in the forward limit. Measurements of nuclear medium modifications of 
this quantity would be crucial for further exploring the hypothesis originally 
put forward in Ref.~\cite{Cloet:2006bq} of whether the EMC effect is enhanced 
in spin dependent observables.\\

We propose to measure coherent deep exclusive $\pi^0$ BSA off $^4$He to extract 
both the real and imaginary part of chiral-odd nuclear Compton form factors 
$H_A^T$ and investigate the sensitivity to twist-3 contributions. The 
corresponding formalism is described in details in Appendix \ref{app:twist-3}.

%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Coherent exclusive DVCS off deuteron}
%%%%%%%%%%%%%%%%%%%%%%%%%

Deep inelastic scattering processes in the deuteron have been used mainly as 
the source of information on unpolarized and polarized distributions of a 
neutron in the forward limit. With the advent of DVCS it became possible to 
study GPDs of a deuteron as a whole leaving it intact in the final state. Due 
to the fact that it is a spin-1 object, there are entirely new functions 
appearing, which could give us a deeper understanding of this nucleus in terms 
of its fundamental degrees of freedom. The nine GPDs for a spin-1 object have 
been given in Ref.~\cite{Berger:2001zb} and their properties have been 
discussed in detail in Ref.~\cite{Kirchner:2003wt}. Sum rules relate these 
GPDs to the usual deuteron form factors $G_i(i=1,3)$, which are linear 
combinations of charge monopole $G_C$, magnetic dipole $G_M$ , and charge 
quadrupole, $G_Q$. The unique feature of these relations is the fact that 
the $G_3(t)$ form factor is totally dominated by the charge quadrupole $G_Q(t)$~%
\cite{deuteronDVCS} and allows us to access the $H_3$ GPD via beam-spin 
asymmetry measurements. This will be the first measurement of the partonic 
structure function of the deuteron related in the forward limit to charge 
quadrupole, and not only to the charge and the magnetic form factors, which can not be 
decomposed into simple additive sum of the proton and the neutron. Using the 
results of these measurements, we can access the partonic structure of the 
deuteron treated as a single hadron, which is irreducible to the partonic 
structure of its nucleon constituents.\\

We propose here to measure the beam-spin asymmetries of the coherent DVCS 
scattering process on a deuteron target using CLAS12 to detect the scattered 
electron and the high energy photon and the ALERT detector to detect low 
energy recoil deuterons. This data will help determine our sensitivity to the
charge quadrupole in coherent DVCS off deuteron and possibly motivate future
proposals.

%%%%%%%%%%%%%%%%%%%%%%%%
\section{DVCS in three-body break up reactions}
%%%%%%%%%%%%%%%%%%%%%%%%

The three-body break up (3BBU) reactions are of particular interest in nuclear 
physics because they provide information about the substructure and nucleon 
correlations within a nucleus. For example quasi-elastic knock-out of a neutron 
from $^4$He leaves the remaining three nucleon system behind but the degree to 
which it remains a $^3$He depends on the initial 
nuclear configuration and final state interactions. As shown in 
Figure~\ref{fig:boundDeuteronPDist}, when going to larger momenta, finding a recoil 
triton becomes unlikely and 3BBU configurations start to dominate.  
Our detector setup allows to detect multiple recoil fragments and CLAS12 will be
equipped with a central neutron detector (CND) making possible to detect final
states where $^4$He $\rightarrow$ d$+$p$+$n. \\

\begin{figure}
   \centering
   \includegraphics{figs/He4_pair_dist_Wiringa_2014.pdf}
   \caption{\label{fig:boundDeuteronPDist}The proton momentum distribution in 
      $^4$He is shown by the red circles; the $tp$ cluster distribution is shown 
      by the blue triangles and the $dd$ cluster distribution is shown by the 
   magenta squares.~\cite{Wiringa:2013ala}}
\end{figure}

This study can be made similarly for DVCS, asymmetries appears to be rather 
simple observables to extract within the experimental conditions 
of this run group. This partonic level measurement can be used to access not 
only the parton distributions of the bound nucleon, but also the parton 
distributions for specific configurations of the nucleus.
This can be understood by considering the relative motion of correlated pairs inside
of the $^4$He nucleus as shown in Figure~\ref{fig:He42Ddists}. \\

We propose here to measure the beam-spin asymmetries of the DVCS scattering in 
three-body break up reactions off $^4$He target using CLAS12 to detect the 
scattered electron, neutrons and the high energy photon and the ALERT detector to detect 
low energy recoil fragments.

\begin{figure}
   \centering
   \includegraphics[width=0.45\textwidth]{figs/he4_rho_pp_2D.pdf}
   \includegraphics[width=0.45\textwidth]{figs/he4_rho_pn_2D.pdf}
   \caption{\label{fig:He42Ddists}
      The proton-proton (left) and the proton-neutron (right) momentum 
      distributions in $^4$He averaged over the directions of 
      $\bm{q}=(\bm{k}_1-\bm{k}_2)/2$ and $\bm{Q}=\bm{k}_1+\bm{k}_2$ as a
      function of $q$ for several fixed values of $Q$.  Reproduced from 
   Ref.~\cite{Wiringa:2013ala}.}
\end{figure}







